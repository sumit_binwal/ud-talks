//
//  ImageDetailCollectionCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 16/02/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ImageDetailCollectionCell: UICollectionViewCell, UIScrollViewDelegate {
    static var cellIdentifier = "ImageDetailCollectionCell"
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageViewDetail: UIImageView!
    @IBOutlet weak var buttonVideo: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
        scrollView.zoomScale = 1.0

    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewDetail
    }
    
    

}
