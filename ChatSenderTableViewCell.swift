//
//  ChatSenderTableViewCell.swift
//  WaltzinUser
//
//  Created by Ratina on 9/24/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit

/// UITableViewCell class to show sender chat data
class ChatSenderTableViewCell: UITableViewCell {

    /// variable for sender's profile image
    @IBOutlet weak var senderImage: UIImageView!
    
    @IBOutlet var senderImageButton: UIButton!
    /// varaible for sender's Media Image
    @IBOutlet var mediaImageView: UIImageView!
    
    /// varaible for sender's message background
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet var chatStatusImage: UIImageView!
    
    /// variable for message
    @IBOutlet weak var messageLabel: UILabel!
    
    /// variable for time
    @IBOutlet weak var timeLabel: UILabel!
    
    /// UITableViewCell life cycle method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disableSelection()
        //let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
       //self.addGestureRecognizer(longGesture)
        if messageLabel != nil
        {
        messageLabel.font = UIFont.setFontTypeRegular(withSize: 14.6)
        }
        
        if mediaImageView != nil
        {
            mediaImageView.layer.cornerRadius = 10
            mediaImageView.clipsToBounds = true
        }
        
        timeLabel.font = UIFont.setFontTypeRegular(withSize: 7.3)
        
        senderImage.layer.borderColor = UIColor.clear.cgColor
        senderImage.layer.borderWidth = 1.0 * scaleFactorX
        senderImage.layer.cornerRadius = senderImage.frame.size.width / 2 * scaleFactorX
        senderImage.layer.masksToBounds = true
    }
    
    /// UITableViewCell life cycle method
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func senderImageButtonClicked(_ sender: UIButton)
    {
        
    }
    
//    @objc func longTap(_ sender: UIGestureRecognizer){
//        print("Long tap")
//
//
//
//        if sender.state == .ended {
//            print("UIGestureRecognizerStateEnded")
//self.contentView.backgroundColor = UIColor.gray
////            viewFarword.frame = CGRect.init(x: 0, y: 0, width: 375 * scaleFactorX, height: 50 * scaleFactorX)
////            self.view .addSubview(viewFarword)
////            tblChat.allowsMultipleSelection = true
//            //Do Whatever You want on End of Gesture
//        }
//        else if sender.state == .began {
//            print("UIGestureRecognizerStateBegan.")
//            //Do Whatever You want on Began of Gesture
//        }
//    }
}
