//
//  FileReciverCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 08/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class FileReciverCell: UITableViewCell {
static let cellIdentifier = "FileReciverCell"
    @IBOutlet var labelFileType: UILabel!
    @IBOutlet var labelFileSize: UILabel!
    @IBOutlet var imgVwFileType: UIImageView!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var buttonFile: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContainer.layer.cornerRadius = 10

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
