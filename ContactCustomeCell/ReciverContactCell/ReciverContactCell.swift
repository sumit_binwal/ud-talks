//
//  SenderContactCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 07/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ReciverContactCell: UITableViewCell {
static let cellIdentifier = "ReciverContactCell"
    @IBOutlet var imageVwProfile: UIImageView!
    @IBOutlet var viewRightContainer: UIView!
    @IBOutlet var labelLocation: UILabel!
    @IBOutlet var labelUserType: UILabel!
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelDiscription: UILabel!
    @IBOutlet var viewMainContainer: UIView!
    @IBOutlet var buttonReciverCell: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewMainContainer.layer.cornerRadius = 10
viewMainContainer.clipsToBounds = true
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
