//
//  FileSenderCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 08/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class FileSenderCell: UITableViewCell {
static let cellIdentifier = "FileSenderCell"
    @IBOutlet var labelFileType: UILabel!
    @IBOutlet var labelFileSize: UILabel!
    @IBOutlet var imgVwFileType: UIImageView!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet weak var chatStatusImage: UIImageView!

    @IBOutlet var buttonFile: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 10
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
