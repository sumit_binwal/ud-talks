//
//  ChatReceiverTableViewCell.swift
//  WaltzinUser
//
//  Created by Ratina on 9/24/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit

/// UITableViewCell class to show receiver chat data
class ChatReceiverTableViewCell: UITableViewCell {

    /// variable for receiver's profile image
    @IBOutlet weak var receiverImage: UIImageView!
    
    /// varaible for reveiver's message background
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet var receiverImageButton: UIButton!
    /// varaible for sender's Media Image
    @IBOutlet var mediaImageView: UIImageView!

    
    /// variable for message
    @IBOutlet weak var messageLabel: UILabel!
    
    /// variable for time
    @IBOutlet weak var timeLabel: UILabel!
    
    /// UITableViewCell life cycle method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      //  self.disableSelection()
        
        
        if messageLabel != nil
        {
            messageLabel.font = UIFont.setFontTypeRegular(withSize: 14.6)
        }
        
        if mediaImageView != nil
        {
            mediaImageView.layer.cornerRadius = 10
            mediaImageView.clipsToBounds = true
        }
        timeLabel.font = UIFont.setFontTypeRegular(withSize: 7.3)
        
        receiverImage.layer.borderColor = UIColor.clear.cgColor
        receiverImage.layer.borderWidth = 1.0 * scaleFactorX
        receiverImage.layer.cornerRadius = receiverImage.frame.size.width / 2 * scaleFactorX
        receiverImage.layer.masksToBounds = true
    }

    /// UITableViewCell life cycle method
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func receiverImageButtonClicked(_ sender: UIButton)
    {
        
    }
}
