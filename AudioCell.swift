//
//  AudioCell.swift
//  ChatApp
//
//  Created by iOS on 27/07/18.
//  Copyright © 2018 iOS. All rights reserved.
//

import UIKit

class AudioCell: UITableViewCell {

    @IBOutlet weak var viewSender: UIView!
    @IBOutlet weak var viewReceiver: UIView!
    @IBOutlet var imageViewCheck: UIImageView!

    @IBOutlet var labelSendTime: UILabel!
    @IBOutlet var labelRcvTime: UILabel!
    
    @IBOutlet var buttonPlaySend: UIButton!
    @IBOutlet var buttonPlayReceive: UIButton!
    
    @IBOutlet weak var imageViewSender: UIImageView!
    @IBOutlet weak var imageViewReceiver: UIImageView!

    @IBOutlet weak var sliderReceiver: UISlider!
    @IBOutlet weak var sliderSender: UISlider!
    @IBOutlet var acvtivity_IndicatorView: UIActivityIndicatorView!
    @IBOutlet var imageViewStarSend: UIImageView!
    @IBOutlet var imageViewStarRecvd: UIImageView!
    
    @IBOutlet var bubbleImageViewR: UIImageView!
    @IBOutlet var bubbleImageViewS: UIImageView!

    
  //  @IBOutlet weak var downloadProgressView: ACRCircleView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        downloadProgressView.baseColor = UIColor.clear
//        downloadProgressView.strokeWidth = downloadProgressView.bounds.width / 2
//        downloadProgressView.progress = 0
//        downloadProgressView.layer.cornerRadius = downloadProgressView.bounds.width / 2
//        downloadProgressView.layer.masksToBounds = true
        
//        bubbleImageViewS.image = #imageLiteral(resourceName: "bubble_sent")
//            .resizableImage(withCapInsets:
//                UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
//                            resizingMode: .stretch)
//            .withRenderingMode(.alwaysTemplate)
//        
//        bubbleImageViewR.image = #imageLiteral(resourceName: "bubble_received")
//            .resizableImage(withCapInsets:
//                UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
//                            resizingMode: .stretch)
//            .withRenderingMode(.alwaysTemplate)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
