
//
//  AudioSenderCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 04/02/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class AudioReciverCell: UITableViewCell {
    static let cellIdentifier = "AudioReciverCell"
    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var seekbarSlider: UISlider!
    @IBOutlet weak var mainAudioView: UIView!
    @IBOutlet weak var labelTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainAudioView.layer.cornerRadius = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}
