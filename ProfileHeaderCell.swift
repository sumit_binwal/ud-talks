//
//  ProfileHeaderCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 11/02/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ProfileHeaderCell: UITableViewCell {
static var cellIdentifier = "ProfileHeaderCell"
    @IBOutlet var profileHeadername : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
