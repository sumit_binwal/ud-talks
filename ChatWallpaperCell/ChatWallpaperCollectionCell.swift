//
//  ChatWallpaperCollectionCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 15/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ChatWallpaperCollectionCell: UICollectionViewCell
{
 
    static var cellIdentifier = "ChatWallpaperCollectionCell"
    @IBOutlet var wallpaperImgVw: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
