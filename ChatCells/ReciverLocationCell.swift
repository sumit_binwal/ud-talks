//
//  ReciverLocationCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 06/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Mapbox
class ReciverLocationCell: UITableViewCell {
    @IBOutlet var viewMap: UIView!
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var buttonLocationCell: UIButton!
    static let cellIdentifier = "ReciverLocationCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMap.layer.cornerRadius = 10
        viewMap.clipsToBounds = true

     //   self.disableSelection()
//        let url = URL(string: "mapbox://styles/mapbox/streets-v11")
//        let mapView = MGLMapView(frame: viewMap.bounds, styleURL: url)
//        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        mapView.setCenter(CLLocationCoordinate2D(latitude: 59.31, longitude: 18.06), zoomLevel: 9, animated: false)
//        mapView.allowsScrolling = false
//        viewMap.addSubview(mapView)
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
