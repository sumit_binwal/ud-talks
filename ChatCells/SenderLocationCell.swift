//
//  SenderLocationCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 06/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Mapbox
class SenderLocationCell: UITableViewCell {
    @IBOutlet var viewMap: UIView!
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var buttonLocationCell: UIButton!
    @IBOutlet var chatStatusImage: UIImageView!
    static let cellIdentifier = "SenderLocationCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMap.layer.cornerRadius = 10
        viewMap.clipsToBounds = true
//        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
