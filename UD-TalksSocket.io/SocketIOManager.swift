	//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Gabriel Theodoropoulos on 1/31/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject{
    
    static let sharedInstance = SocketIOManager()
    let manager = SocketManager(socketURL: URL(string: "http://35.243.191.92:8000/")!, config: [.log(true), .compress])
    lazy var socket = manager.defaultSocket

    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func establishConnection() {
        socket.on("update", callback: { data,ack in
           // print(data)
        })
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func sendMessage(message: String, withNickname nickname: String) {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
       
       
        let exampleDict: [String: Any] = [
            "reciverid" : "51",
            "message" : message,
            "dtype" : "true",
            "senderid" : "445",
            "datetime" : result,
            "isread" : "false",
            "deliver" : "false"// type: e.g. class Human: NSObject, NSCoding {...}
            // ...
        ]
        
        let jsonObject1:  [Any] =
        [[
            "id" : "2",
            "isChatEnable" : "true",
            "reciverid" : "rid"// type: e.g. class Human: NSObject, NSCoding {...}
            // ...
        ]]
        
        let jsonObject2: [Any] =
            [[
                "senderid" : "1",
                "reciverid" : "0",
                "loadRecieverChat" : "true"
                ]]
        
        
         let jonString1 = JSONStringEncoder().encode(exampleDict)
            // Successfully created JSON string.
            // ...
        socket.emit("sendMessage", jonString1!)
          //  print("SUCCESSFUL_STATUS ", jsonString)
       
        
        //print("JSON_ARRAY ",jsonObject, jsonObject1, jsonObject2)
        // var messageDictionary = "{username:karishma,message:heeloo}"
       //  completionHandler(messageDictionary as! [String : AnyObject])
        
    }
    /*
    func sendMessage(message: String, withNickname nickname: String) {
        socket.emit("new message", message ,"krishama")
        
        
    }*/
    
    struct JSONStringEncoder {
        /**
         Encodes a dictionary into a JSON string.
         - parameter dictionary: Dictionary to use to encode JSON string.
         - returns: A JSON string. `nil`, when encoding failed.
         */
        func encode(_ dictionary: [String: Any]) -> String? {
            guard JSONSerialization.isValidJSONObject(dictionary) else {
                assertionFailure("Invalid json object received.")
                return nil
            }
            
            let jsonObject: NSMutableDictionary = NSMutableDictionary()
            let jsonData: Data
            
            dictionary.forEach { (arg) in
                jsonObject.setValue(arg.value, forKey: arg.key)
            }
            
            do {
                jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            } catch {
                assertionFailure("JSON data creation failed with error: \(error).")
                return nil
            }
            
            guard let jsonString = String.init(data: jsonData, encoding: String.Encoding.utf8) else {
                assertionFailure("JSON string creation failed.")
                return nil
            }
            
            print("JSON string: \(jsonString)")
            return jsonString
        }
    }
    func connectToServerWithNickname(nickname: String, completionHandler: @escaping (_ userList: [[String: AnyObject]]?) -> Void) {
    
        let exampleDict: [String: Any] = [
            "id" : "44",
            "messageCount" : "0",
            "isChatEnable" : "true",
            "isDelivered" : "true",
            "isReaded" :    "true",
            "isOnlineStatus" : "true"   // type: e.g. class Human: NSObject, NSCoding {...}
            // ...
        ]
        let jsonString = JSONStringEncoder().encode(exampleDict)
        socket.emit("userid", jsonString!)
        //socket.emit("add user", nickname)
        socket.on("addedUser") { ( dataArray, ack) -> Void in
            completionHandler(dataArray[0] as? [[String: AnyObject]])
        }
        
        listenForOtherMessages()
    }
    
    
    func exitChatWithNickname(nickname: String, completionHandler: () -> Void) {
        socket.emit("exitUser", nickname)
        completionHandler()
    }
    
  /*  func sendMessage(message: String, withNickname nickname: String) {
        socket.emit("new message", "Hello Karishma ji")
        
    }*/
   
    
    func getSenderChat(completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("mySendMessage") { (dataArray, socketAck) -> Void in
            let messageDictionary = dataArray[0] as! [String: AnyObject] as AnyObject
            let name = messageDictionary["senderid"]
            let message = messageDictionary["message"]
            /* var messageDictionary = [String: AnyObject]()
            messageDictionary["username"] = dataArray[0] as! [String: AnyObject] as AnyObject
            messageDictionary["message"] = dataArray[0] as! [String: AnyObject] as AnyObject
            completionHandler(messageDictionary)
            print(messageDictionary)*/
            completionHandler(messageDictionary as! [String : AnyObject])
            print("ZIASY_TECHNOLOGY",dataArray)
            print("DATA_NEW_MESSAGE",dataArray[0] as! [String: AnyObject] as AnyObject)
            print("NAME_VALUE",name!!)
            print("MESSAGE",message!!)
        }
    }
    
    func getReciverChat(completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("recieveMessage") { (dataArray, socketAck) -> Void in
            let messageDictionary = dataArray[0] as! [String: AnyObject] as AnyObject
            let name = messageDictionary["senderid"]
            let message = messageDictionary["message"]
            /* var messageDictionary = [String: AnyObject]()
             messageDictionary["username"] = dataArray[0] as! [String: AnyObject] as AnyObject
             messageDictionary["message"] = dataArray[0] as! [String: AnyObject] as AnyObject
             completionHandler(messageDictionary)
             print(messageDictionary)*/
            completionHandler(messageDictionary as! [String : AnyObject])
            print("ZIASY_TECHNOLOGY",dataArray)
            print("DATA_NEW_MESSAGE",dataArray[0] as! [String: AnyObject] as AnyObject)
            print("NAME_VALUE",name!!)
            print("MESSAGE",message!!)
        }
    }
    
    func getUserChatList(completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("recieveMessage") { (dataArray, socketAck) -> Void in
            let messageDictionary = dataArray[0] as! [String: AnyObject] as AnyObject
            let name = messageDictionary["senderid"]
            let message = messageDictionary["message"]
            /* var messageDictionary = [String: AnyObject]()
             messageDictionary["username"] = dataArray[0] as! [String: AnyObject] as AnyObject
             messageDictionary["message"] = dataArray[0] as! [String: AnyObject] as AnyObject
             completionHandler(messageDictionary)
             print(messageDictionary)*/
            completionHandler(messageDictionary as! [String : AnyObject])
            print("ZIASY_TECHNOLOGY",dataArray)
            print("DATA_NEW_MESSAGE",dataArray[0] as! [String: AnyObject] as AnyObject)
            print("NAME_VALUE",name!!)
            print("MESSAGE",message!!)
        }
    }
    
    
    private func listenForOtherMessages() {
        socket.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
        
        socket.on("userExitUpdate") { (dataArray, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: dataArray[0] as! String)
        }
        
        socket.on("userTypingUpdate") { (dataArray, socketAck) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: dataArray[0] as? [String: AnyObject])
        }
    }
    
    
    func sendStartTypingMessage(nickname: String) {
        socket.emit("typing")
    }
    
    
    func sendStopTypingMessage(nickname: String) {
        socket.emit("stopTyping")
    }
}
