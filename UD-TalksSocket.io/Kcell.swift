//
//  Kcell.swift
//  UD-Talks Demo
//
//  Created by mac on 25/07/1940 Saka.
//  Copyright © 1940 ZiasyTechnology. All rights reserved.
//

import UIKit
//import MGSwipeTableCell
import SwipeCellKit

class Kcell: SwipeTableViewCell {

    @IBOutlet weak var imgUser: UIImageView!

    @IBOutlet weak var viewBGColor: UIView!
    @IBOutlet weak var lblName: UILabel!

        @IBOutlet weak var vwStatus: UIView!
    @IBOutlet var lblDiscription: UILabel!
    @IBOutlet weak var btnSwipeAction: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.font = UIFont.setFontTypeRegular(withSize: 15)
        lblDiscription.font = UIFont.setFontTypeThin(withSize: 13)
        
        //self.disableSelection()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
