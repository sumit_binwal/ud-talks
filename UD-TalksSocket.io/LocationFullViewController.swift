//
//  LocationFullViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 06/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Mapbox

import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation

class LocationFullViewController: UIViewController {

    @IBOutlet var vwMapContainer: UIView!
    var latitude = String()
    var longitude = String()
    var strPlaceName = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         _ = backBarButton

        
        let url = URL(string: "mapbox://styles/mapbox/streets-v11")
            let mapView = MGLMapView.init(frame: self.vwMapContainer.bounds)
                mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        mapView.setCenter(CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!), zoomLevel: 13, animated: false)
            self.vwMapContainer.addSubview(mapView)
        
        mapView.logoView.isHidden = true
        mapView.attributionButton.isHidden = true
        
        let hello = MGLPointAnnotation()
        hello.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        hello.title = strPlaceName
      //  hello.subtitle = "Welcome to my marker"
        
        // Add marker `hello` to the map.
        mapView.addAnnotation(hello)
        // Do any additional setup after loading the view.
    }
    

    @IBAction func navigationButtonClicked(_ sender: Any)
    {
        if LocationManager.sharedInstance().isLocationAccessAllowed() {
            print("userLattitude = \(userLattitude)")
            print("userLongtitude = \(userLongtitude)")
            
            let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: userLattitude, longitude: userLongtitude), name: "Mapbox")
            let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!), name: strPlaceName)
            
            let options = NavigationRouteOptions(waypoints: [origin, destination])
            
            Directions.shared.calculate(options) { (waypoints, routes, error) in
                guard let route = routes?.first else { return }
                
                let viewController = NavigationViewController(for: route)
                self.present(viewController, animated: true, completion: nil)
            }
        }
        else {
            UIAlertController.showAlertWith(title: String.MyApp.AppName, message: String.alertMessage.autoDetectLocation) {}
        }
        
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
