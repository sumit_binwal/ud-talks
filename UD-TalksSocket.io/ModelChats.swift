//
//  ModelChats.swift
//  Knackel
//
//  Created by Santosh on 28/05/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

struct ModelChatInfo {
    
    //    var username: String! = ""
    //    var picture: String! = ""
    //    var userID: String! = ""
    //    var fullname: String! = ""
    
    var isCanSendOffer = false
    var isSiteSurvey = false
    var isSiteSurveySubmitted = false
    var roomID : String! = ""
    
    mutating func updateModel(usingDictionary dictionary: [String:Any]) {
        
        if let sendOffer = dictionary["canSendOffer"] as? Bool {
            self.isCanSendOffer = sendOffer
        }
        

        if let siteSurvey = dictionary["isSiteSurvey"] as? Bool {
            self.isSiteSurvey = siteSurvey
        }
        
        if let room = dictionary["roomId"] as? String {
            self.roomID = room
        }
    }
}




struct ModelChatLastMessage {
    
    var messageID: String!
    var message: String!
    var messageType: String!
    
    mutating func updateModel(usingDictionary dictionary: [String:Any]) {
        
        if let _id = dictionary["_id"] as? String {
            self.messageID = _id
        }
        
        if let message = dictionary["message"] as? String {
            self.message = message
        }
        
        if let type = dictionary["type"] as? String {
            self.messageType = type
        }
    }
}




struct ModelChatMessage {
    
    var chatID: String!
    var jobID: String!
    var userName: String = ""
    var userPicture: String = ""
    var userID: String!
    var messageID: String!
    var message: String!
    var isMediaUploading = false
    var uploadingImage = UIImage ()
    var messageThumb: String = ""
    var messageType: String!
    var createdAt: String!
    var mediaImage : String!
    var isMedia = false
    
    var isSenderSameAsLoggedinUser = false
    
    var downloadProgressValue: CGFloat = 0.0
    
    mutating func updateModel(usingDictionary dictionary: [String:Any], loggedInUserID: String, roomID:String, jobid:String) {
        
//        {
//            "message": hiiiiiiiiiiiiiii;
//            "sender": {
//                "_id": 5 ba0d1a08bd6be42a570efbe;
//                isArchive: 0;
//                name: Tasker;
//                picture: "14f8fd5f645f5876_1537970394_1537970394.jpg";
//                userId: 5 ba0d1a08bd6be42a570efbe;
//                "user_id": 5 ba0d1a08bd6be42a570efbe;
//            },
//            "created": 2018 - 10 - 10 T09: 53: 07.791 Z,
//            "updated": 2018 - 10 - 10 T09: 53: 07.791 Z,
//            "receiver": 5 b9f5ee23e564911304d8388,
//            "_id": 5 bbdcc03f6174f168f3f854c,
//            "seen": 0,
//            "skip": < null > ,
//            "isMedia": 0
//        }
//
      
        self.chatID = roomID
        self.jobID = jobid
        
        if let mediFlag = dictionary["isMediaUploding"] as? Bool {
            self.isMediaUploading = mediFlag
        }
        
        if let uploadingMedia = dictionary["mediaImage"] as? UIImage {
            self.uploadingImage = uploadingMedia
        }
        
        if let msg = dictionary["message"] as? String {
            self.message = msg
        }
        
        if let media = dictionary["isMedia"] as? Bool {
            self.isMedia = media
        }
        
        if let thumb = dictionary["thumb"] as? String {
            self.messageThumb = thumb
        }
        
        if let created = dictionary["created"] as? String {
            self.createdAt = UtilityClass.getDateStringFromStr(givenDateStr: created, andCurrentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", andResultingDateFormat: "dd MMM yyyy, hh:mm a")
        }
        
        if let senderUser = dictionary["sender"] as? [String:Any] {
            
            let senderID = senderUser["_id"] as! String
            
            self.isSenderSameAsLoggedinUser = senderID == loggedInUserID ? true : false
            
            if ((senderID as? String) != nil) {
                    self.userID = senderID
            }
            
            if let sname = senderUser["name"] as? String {
                self.userName = sname
            }
            
            if let spicture = senderUser["picture"] as? String {
                self.userPicture = String.MyApp.AWS_BASE_URL + String.AWSFolder.UsersDir + spicture
            }
        }
        
        if isMedia {
            self.mediaImage = String.MyApp.AWS_BASE_URL + String.AWSFolder.ChatMedia + message
        }
    }
}




struct ModelChatList {
    
    var chatID: String = ""
    var chatType: String = ""
    var groupMembers = ""
    var unreadCount: Int = 0
    var createdAt: String = ""
    var updatedAt: String = ""
    var lastMessage: ModelChatLastMessage?
    
    mutating func updateModel(usingDictionary dictionary: [String:Any]) {
        
        if let id = dictionary["_id"] as? String {
            self.chatID = id
        }
        
        if let members = dictionary["members"] as? Int {
            self.groupMembers = String(members) + " members"
        }
        
        if let chatType = dictionary["chatType"] as? String {
            self.chatType = chatType
        }
        
        if let unreadCount = dictionary["unreadCount"] as? Int {
            self.unreadCount = unreadCount
        }
        
        if let created = dictionary["created"] as? String {
            self.createdAt = created
        }
        
        if let created = dictionary["updated"] as? String {
            self.updatedAt = created
        }
        
        if let lastM = dictionary["lastMessage"] as? [String:Any] {
            self.lastMessage = ModelChatLastMessage ()
            self.lastMessage?.updateModel(usingDictionary: lastM)
        }
    }
}
