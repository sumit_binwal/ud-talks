//
//  HomeViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 18/12/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Mapbox
import SwipeCellKit
class HomeViewController: UIViewController, SwipeTableViewCellDelegate {

    @IBOutlet var addUserFullView: UIView!
    @IBOutlet weak var recentChatTableView: UITableView!
    @IBOutlet var logoImg: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelDiscription: UILabel!
  //  @IBOutlet var addContactView: UIStackView!
    @IBOutlet var addContactView: UIView!
    
    var arrayRecentChat = [[String:Any]]() //Array of dictionary

    var isContactViewOpen = false
    
    var searchBarView = UISearchBar()
    var isSearchBarOpen = false
    var chatSearchMessages = [[String: Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNaviagtionBar()
        setupView()

//

        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // change 2 to desired number of seconds
            // Your code with delay
                    self.authSocketUserID()
                    self.getRecentMessageListSocket()
        }

        let barImg = UIImage.init(named: "navBar")?.resizableImage(withCapInsets: .init(top: 0, left: 0, bottom: 0, right: 0   ), resizingMode: .stretch)
        
        
        self.navigationController?.navigationBar.setBackgroundImage(barImg, for: .default)

        
        //self.view .addSubview(addContactView)
        //addContactView.frame = CGRect.init(x: 50, y: 66, width: UIScreen.main.bounds.width, height: 130)
        
        
        
        
        
        
//        addContactView.center = CGPoint (x: self.view.frame.midX, y: addContactView.frame.height/2)
//        addContactView.transform = CGAffineTransform (translationX: -addContactView.frame.width, y: addContactView.frame.origin.y)
//        addContactView.alpha = 0
        
        
    }

    func authSocketUserID()
    {
        if SocketIOManager.shared().isSocketConnected()
        {
            let param = ["id":UserDefaults.userID,"messageCount":"0","isChatEnable":"false","isDelivered":"false","isReaded":"false","isOnlineStatus":"true"] as! [String:Any]
            
            SocketIOManager.shared().authUserIDtoSocket(parameters: param)
        }
        

    }
    
    func getRecentMessageListSocket()  {
        //addContactView
        //APPDELEGATE.checkReachability()
        if APPDELEGATE.isInternetAvailable()         {
            if SocketIOManager.shared().isSocketConnected()
            {
                let param1 = ["senderid":UserDefaults.userID] as [String:Any]
                SocketIOManager.shared().chatEmployeDetailSocket(parameters: param1)
            }
        }
        else
            
        {
            arrayRecentChat = DataBaseManager.sharedInstance().getValuesToRecentChatDB()
            recentChatTableView.reloadData()
            print("internet koni hai")
        }

    }
    
    
    func setupView()
    {
        UIImageView.makeRound(imageView: logoImg)
        labelTitle.font = UIFont.setFontTypeLightItalic(withSize: 16)
        labelDiscription.font = UIFont.setFontTypeRegular(withSize: 13)
        
        
        recentChatTableView.delegate = self
        recentChatTableView.dataSource = self
        
        
        getRecentChatHitoryList()
    }

    
    @IBAction func addContactButtonClicked(_ sender: UIButton)
    {
        switch sender.tag {
        case 0:
            let addGroupVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "AddGroupVIewController") as! AddGroupVIewController
            self.navigationController?.pushViewController(addGroupVC, animated: true)
            
            break
        case 1:
            let addContactVC = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: String.ControllerName.chatListVC) as! ChatListViewController
            self.navigationController?.pushViewController(addContactVC, animated: true)
            break
        default:
            break
        }
        self.addUserFullView.removeFromSuperview()
        
    }
    func setUpNaviagtionBar()
    {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        // set navigation bar color
        
        
        // hides back button
        self.navigationItem.hidesBackButton = true
        
        let dislike =  UIBarButtonItem.init(image: #imageLiteral(resourceName: "searchIconHome"), style: .plain, target: self, action: #selector(searchButtonPressed))
        //
        let more =  UIBarButtonItem.init(image: #imageLiteral(resourceName: "moreIconHome"), style: .plain, target: self, action: #selector(moreButtonPressed))
        
        self.navigationItem.rightBarButtonItems = [more, dislike]
        
    }
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let barImg = UIImage.init(named: "defaultNavBar")?.resizableImage(withCapInsets: .init(top: 0, left: 0, bottom: 0, right: 0   ), resizingMode: .stretch)
        
        
        self.navigationController?.navigationBar.setBackgroundImage(barImg, for: .default)

        if isContactViewOpen
        {
            self.addUserFullView.removeFromSuperview()
            self.isContactViewOpen = false

        }
    }
    
    
    func getRecentChatHitoryList() {
        
        
        SocketIOManager.shared().recentChatMessageHandler = {[weak self] result in
            
            guard let infoDict = result else {return}
            print(infoDict)
            
            
                print(infoDict)
                
                // If same group chat is open...
                if let arryContact = infoDict["result"] as? [[String:Any]], !arryContact.isEmpty {
                    // Simply add new message model...

                    if let arrayValue = arryContact as? [[String : AnyObject]]
                    {
                        self!.arrayRecentChat = arrayValue; DataBaseManager.sharedInstance().insertValuesToRecentChatDB(dataArry: arrayValue)
                        self?.recentChatTableView.reloadData()

                    }
                    
                }
                else {
                    //self?.createInAppNotification(usingModel: model)
                }
            
            
            
        }
    }
    
    @objc func searchButtonPressed()
    {
        
        if !isSearchBarOpen
        {
        searchBarView = UISearchBar.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        searchBarView.showsCancelButton = true
        searchBarView.delegate = self
        self.view.addSubview(searchBarView)
        isSearchBarOpen = true
        }
        else
        
        {
            searchBarView.removeFromSuperview()

        isSearchBarOpen = false
        }

    }
    
    
    
    
    
    
    
    
    
    
    @objc func moreButtonPressed()
    {
 closeSearchBar()

        if !isContactViewOpen {
            
            self.view.addSubview(addUserFullView)
            addUserFullView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190*scaleFactorX)
                  self.isContactViewOpen = true
            
        }
        else {
            self.addUserFullView.removeFromSuperview()
                          self.isContactViewOpen = false
        }
    }
    
    func closeSearchBar()
    {
        searchBarView.removeFromSuperview()
        searchBarView.text = ""
        isSearchBarOpen = false
        recentChatTableView.reloadData()
    }
    
    @IBAction func udNewsButtonClicked(_ sender: Any)
    {
        closeSearchBar()
     let udNewsVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "UDNewsViewController") as! UDNewsViewController
        self.navigationController?.pushViewController(udNewsVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate
{
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        var image : UIImage
        image = UIImage.init(named: "emoji_chat")!
        return image
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Contacts Yet..."
        
        // multiple attributes declared at once
        let multipleAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.setFontTypeRegular(withSize: 17.0)]
        
        let myAttrString = NSAttributedString(string: text, attributes: multipleAttributes)
        
        return myAttrString
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Press On Menu Icon, And Select The Way You Prefer To Add \n Freinds Or Businessmen"
        
        // multiple attributes declared at once
        let multipleAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.setFontTypeRegular(withSize: 11.0)]
        
        let myAttrString = NSAttributedString(string: text, attributes: multipleAttributes)
        
        return myAttrString
    }
    
   
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return 20.0 * scaleFactorX
    }
    
    
}


extension HomeViewController : UITableViewDelegate,UITableViewDataSource
{
    // MARK:- Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRow = isSearchBarOpen ? chatSearchMessages.count : arrayRecentChat.count
        
        return numberOfRow
     
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .left else { return nil }
        
        var dictVal = self.isSearchBarOpen ? self.chatSearchMessages[indexPath.row] : self.arrayRecentChat[indexPath.row]

        let viewButton = SwipeAction(style: .default, title: "") { action, indexPath in
            // handle action by updating model with deletion
            //dict

            let profileVC =  UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            profileVC.profileName = dictVal["name"] as! String
            self.navigationController?.pushViewController(profileVC, animated: true)

            
        }
        
        // customize the action appearance
        viewButton.image = UIImage(named: "swipeView")
        
        
        
        let callButton = SwipeAction(style: .default, title: "") { action, indexPath in
            // handle action by updating model with deletion
            
            let strNumber = dictVal["name"] as! String
            if let url = URL(string: "tel://\(strNumber)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }

            
        }
        
        // customize the action appearance
        callButton.image = UIImage(named: "swipeCall")
        
    
        let deleteAction = SwipeAction(style: .default, title: "") { action, indexPath in
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "swipeDelete")
        
        
        let blockButton = SwipeAction(style: .default, title: "") { action, indexPath in
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        blockButton.image = UIImage(named: "swipeBlock")
        
        
        
        let moreButton = SwipeAction(style: .default, title: "") { action, indexPath in
            // handle action by updating model with deletion
            
            UIAlertController.showActionSheetWithTitle(title: "UDTalks", message: "More Items", onViewController: self, withButtonArray: ["Stick On Top","Sort Above","Mark as Unread"], dismissHandler: { (indexButton) in
                
            })
            
        }
        
        // customize the action appearance
        moreButton.image = UIImage(named: "swipeMore")
        
//        deleteAction2.backgroundColor = UIColor.init(red: 57, green: 56, blue: 56, alpha: 1)
//        deleteAction1.backgroundColor = UIColor.init(red: 57, green: 56, blue: 56, alpha: 1)
        

        //return [moreButton,blockButton,deleteAction,callButton,viewButton]
        return [viewButton,callButton,deleteAction,blockButton,moreButton]
    }
    
//    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
//        var options = SwipeOptions()
//        options.expansionStyle = .destructive
//        options.transitionStyle = .border
//        return options
//    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "Cell") as! Kcell
        
        
        cell.delegate = self
        var dict = [String:Any]()
        if isSearchBarOpen
        {
            dict = chatSearchMessages[indexPath.row]
        }
        else
        
        {
            dict = arrayRecentChat[indexPath.row]
        }
        
        cell.lblName?.text = dict["name"] as? String
        
        // cell.imgUser.circleView(_color:.gray, borderWidth: 0.5)
        //cell.imgUser?.clipsToBounds = true
        cell.selectionStyle = .none
        let msgType = dict["dtype"] as! String
        
        if indexPath.row % 2 == 0
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 246, GREEN: 134, BLUE: 2, ALPHA: 1)
        }
        else
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 61, GREEN: 61, BLUE: 61, ALPHA: 1)        }
        
        if msgType == "img"
        {
            cell.lblDiscription?.text = "SHARE IMAGE"

        }
        else if msgType == "location"
        {
            cell.lblDiscription?.text = "SHARE LOCATION"
            
        }
        else if msgType == "pdf"
        {
            cell.lblDiscription?.text = "SHARE FILE"
            
        }
        else if msgType == "audio"
        {
            cell.lblDiscription?.text = " SHARE AUDIO"
            
        }
        else if msgType == "share"
        {
            cell.lblDiscription?.text = "SHARE CONTACT"
            
        }
        else if msgType == "video"
        {
            cell.lblDiscription?.text = "SHARE VIDEO"
            
        }
        else if msgType == "emoji"
        {
            cell.lblDiscription?.text = "SHARE EMOJI"
            
        }
        else
        {
            cell.lblDiscription?.text = dict["message"] as! String

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        closeSearchBar()

        var dict = isSearchBarOpen ? chatSearchMessages[indexPath.row] : arrayRecentChat[indexPath.row]

        let chatVC = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: String.ControllerName.chatVC) as! ChatViewController
        
        let chatType = dict["chattype"] as? String ?? ""
        
        let a = dict["id"] as! String
        let b = dict["name"] as! String
        
        if chatType == "group"
        {
            chatVC.isGroupChat = true
chatVC.dictGroupData = dict

        }
        chatVC.reciverID = a
        chatVC.reciverName = b

//        else
//        {
//            chatVC.reciverID = a
//            chatVC.reciverName = b
//        }
        
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * scaleFactorX
    }
}
//MARK: - <---- Extension UISearchBar Delegate Methods
extension HomeViewController : UISearchBarDelegate
{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.removeFromSuperview()
        searchBar.text = ""
        isSearchBarOpen = false
        recentChatTableView.reloadData()
        
    }
    
   
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        print(searchText)
        
if searchText.count > 1
{
    let arrVal:[[String:AnyObject]] = arrayRecentChat.filter { (dictVal) -> Bool in
        
        //chatMessages.filter{($0["message"] as! String) == searchText} as [[String : AnyObject]]
        let strMessage = dictVal["name"] as! String
        if strMessage.contains(searchText)
        {
            return true
        }
        else
        {
            return false
        }
        } as [[String : AnyObject]]
    
    chatSearchMessages = arrVal
    recentChatTableView.reloadData()

        }
    }
}
