//
//  SettingMenuTVCell.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 02/12/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class SettingMenuTVCell: UITableViewCell {

    @IBOutlet weak var imgSettingMenu: UIImageView!
    
    @IBOutlet weak var lblSettingMenu: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
