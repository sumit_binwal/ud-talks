//
//  CustomLocationPicker.swift
//  LocationPickerExample
//
//  Created by Jerome Tan on 3/30/16.
//  Copyright © 2016 Jerome Tan. All rights reserved.
//

import UIKit
import LocationPickerViewController

protocol LatLongPickDelegate {
    // Classes that adopt this protocol MUST define
    // this method -- and hopefully do something in
    // that definition.
    func didFinishPickLocation(_ latitude:String, longitute:String, name:String)
}
class CustomLocationPicker: LocationPicker {


    var viewController: ViewController!
    var latlongDelegate:LatLongPickDelegate?
    override func viewDidLoad() {
        super.addBarButtons()
        super.viewDidLoad()
        
    }
    
    
    
    @objc override func locationDidSelect(locationItem: LocationItem) {
        print("Select overrided method: " + locationItem.name)
    }
    
    
    
    @objc override func locationDidPick(locationItem: LocationItem) {
        
        let latDouble = locationItem.coordinate?.latitude
        let longDouble = locationItem.coordinate?.longitude
      
        if latDouble != nil
        {
            let letString:String = "\(latDouble!)"
            let longString:String = "\(longDouble!)"
            latlongDelegate?.didFinishPickLocation(letString, longitute: longString, name:locationItem.name)
            dismiss(animated: true, completion: nil)
        }
        
    }
}
