//
//  UDNewsViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 11/02/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class UDNewsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "UDTalks News"
        _ = backBarButton

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UDNewsViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 417

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "udNewsCell") as! UITableViewCell
            
            return cell
    }
}
