//
//  WebServiceHandler.swift
//  UD-Talks Demo
//
//  Created by mac on 25/07/1940 Saka.
//  Copyright © 1940 ZiasyTechnology. All rights reserved.
//

import UIKit

class WebServiceHandler: NSObject {
    
    class func postWebService(url:String, param:[String:Any], withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
     
        let headers = [
            "content-type": "application/json",
            "authorization": "E6jhLtJRA4QowlKx",
            "accept": "application/json",
            ]
        
        Alamofire.request("http://128.199.222.145:9090/plugins/restapi/v1/users", method: .post, parameters: param,encoding: JSONEncoding.default, headers: headers).responseString{
            response in
            switch response.result{
            case .success:
                print("success----",response.response?.statusCode as Any)
                break
            case .failure( _):
                print("error-----",response.response?.statusCode as Any)
                //completionHandler(false, "error" as Any)
            }
        }
        /*Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response.response?.statusCode)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }*/
        
    }
    
    class func getWebService(url:String, param:[String:Any]?, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        /* let defaults = UserDefaults.standard
         
         let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
         
         let headers = withHeader ? ["Authorization":token,
                                     "Accept":"application/json",
                                     "Content-Type":"application/json"] : nil*/
        
        let headers = withHeader ? ["Authorization":"E6jhLtJRA4QowlKx",
                                    "Accept":       "application/json",
                                    "Content-Type": "application/json"] : nil
        
        Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
    class func delelteWebService(url:String, param:[String:Any]?, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        
        
        let defaults = UserDefaults.standard
        let token = "Bearer " + (defaults.value(forKey: "token") as! String)
        let headers = ["Authorization":token,"Accept":"application/json"]
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
            
            //        Alamofire.request(url, method: .delete, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
}
