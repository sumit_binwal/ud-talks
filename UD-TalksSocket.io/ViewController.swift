//
//  ViewController.swift
//  UD-Talks Demo
//
//  Created by mac on 25/07/1940 Saka.
//  Copyright © 1940 ZiasyTechnology. All rights reserved.
//

import UIKit
import NVActivityIndicatorView



class ViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet var lblDiscription: UILabel!
    @IBOutlet var lblTitle: UILabel!
    
    var strPhoneNo = String()
    var nickname = String()
    var users = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhoneNo.delegate = self
        
        
        //hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btnLogin(_ sender: Any)
    {
        strPhoneNo = txtPhoneNo.text!
        if strPhoneNo == ""{
            
            let alert = UIAlertController(title: "Warning", message: "Please Enter your Phone Number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {

        }
        self.callWebServiceForLogin()
    }
    func callWebServiceForLogin() {
 //NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        let param =  ["name": txtPhoneNo.text ?? "",
                      "did": UserDefaults.getDeviceToken(),
                      "num"    : txtPhoneNo.text ?? ""]
        
        FireApi.shared().performMultiPartRequest(for: EndPoints.register.path, imageArray: [], headers: nil, parameters: param) { [weak self] result in
            
            UtilityClass.stopAnimating()
            
            guard let `self` = self else {return}
            
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    print(responseDict)
                    
                    if let resultArray = responseDict["result"] as? [[String:Any]], !resultArray.isEmpty {
                        let info = resultArray.first!
                        print(info)
                        UserDefaults.saveUserInformation(userInfo: info)
                        APPDELEGATE.screenRedirection()
                    }



                    return
                }
            }
        }
          
    }
}
//MARK: - UITextfiled Delegete
typealias loginView = ViewController

extension loginView : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text == "" && string == "" {
            return false
        }
        if textField == txtPhoneNo {
            
        } else  {
            
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
        return true
    }
}
