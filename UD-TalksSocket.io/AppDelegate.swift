//
//  AppDelegate.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 21/11/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import CoreData
import SocketIO
import InAppNotify
import IQKeyboardManagerSwift
import AWSS3
import AWSCore
import Pushy
import Fabric
import Crashlytics
import AWSCognito
import Mapbox
import FMDB

let scaleFactorX = UIScreen.main.bounds.size.width/375
let scaleFactorY = UIScreen.main.bounds.size.height/667
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
var userLattitude : Double = 0.0
var userLongtitude : Double = 0.0

/// variable for Notification Internet Connection
let NotificationInternetConnection = "internetConnection"

@UIApplicationMain



class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    /// cariable for internet Reachability
    var internetReachability : Reachability? = Reachability.networkReachabilityForInternetConnection()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityDidChange(_:)), name: NSNotification.Name(rawValue: ReachabilityDidChangeNotificationName), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(internetReachabilityChanged), name: NSNotification.Name(rawValue: NotificationInternetConnection), object: nil)

        
        _ = internetReachability?.startNotifier()

        
        DataBaseManager.sharedInstance()

        
        //Fabric
        Fabric.with([Crashlytics.self, AWSCognito.self])
        // TODO: Move this to where you establish a user session
        self.logUser()
        
      //  Crashlytics.sharedInstance().crash()
        
        // Initialize Pushy SDK
        let pushy = Pushy(UIApplication.shared)
        
        // Register the device for push notifications
        pushy.register({ (error, deviceToken) in
            // Handle registration errors
            if error != nil {
                return print ("Registration failed: \(error!)")
            }
            
            // Print device token to console
            print("Pushy device token: \(deviceToken)")
            
            // Persist the token locally and send it to your backend later
            UserDefaults.standard.set(deviceToken, forKey: "pushyToken")
        })
        
        // Handle push notifications
        pushy.setNotificationHandler({ (data, completionHandler) in
            // Print notification payload data
            print("Received notification: \(data)")
            
            // Fallback message containing data payload
            var message = "\(data)"
            
            // Attempt to extract "message" key from APNs payload
            if let aps = data["aps"] as? [AnyHashable : Any] {
                if let payloadMessage = aps["alert"] as? String {
                    message = payloadMessage
                }
            }
            
            // Display the notification as an alert
//            let alert = UIAlertController(title: "Incoming Notification", message: message, preferredStyle: UIAlertController.Style.alert)
//            
//            // Add an action button
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            
//            // Show the alert dialog
//            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
            // You must call this completion handler when you finish processing
            // the notification (after fetching background data, if applicable)
            completionHandler(UIBackgroundFetchResult.newData)
        })
        
        
        //AWSS3 Integration
        let endpoint = AWSEndpoint(urlString: "https://sgp1.digitaloceanspaces.com")
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey:"JEOWPS5PEY4E5X6J42F4", secretKey: "U4Se+JKztU5tz8Vjz/SMpUNJwd3Xz+Z2VBlPe8ANrAg")
        let defaultServiceConfiguration = AWSServiceConfiguration(region: AWSRegionType.USEast1, endpoint: endpoint, credentialsProvider: credentialsProvider)
        
        defaultServiceConfiguration?.maxRetryCount = 5
        defaultServiceConfiguration?.timeoutIntervalForRequest = 30
        AWSServiceManager.default().defaultServiceConfiguration = defaultServiceConfiguration
        
        if LocationManager.sharedInstance().isLocationAccessAllowed() {
            print("userLattitude = \(userLattitude)")
            print("userLongtitude = \(userLongtitude)")
        }
        else {
            UIAlertController.showAlertWith(title: String.MyApp.AppName, message: String.alertMessage.autoDetectLocation) {}
        }
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        UINavigationBar.defaultAppearance()

        screenRedirection()
        newMessageSocketLister()
        

        
        return true
        
    }
    
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail(UserDefaults.userID)
        Crashlytics.sharedInstance().setUserIdentifier(UserDefaults.mobileNumber)
        Crashlytics.sharedInstance().setUserName("Test User")
    }

    
    
    func newMessageSocketLister() {
        
        
        SocketIOManager.shared().reciveSenderMessageHandler = {[weak self] result in
            
            guard var infoDict = result else {return}
            print(infoDict)
            
            if let chatVC = self!.window!.visibleViewController() as? ChatViewController {
                
                if !infoDict.isEmpty {
                    
                    let senderID = infoDict["senderid"] as? String
                    if chatVC.reciverID == senderID || chatVC.isGroupChat
                    {
                        var isType = infoDict["type"] as? String
                        
                        if isType?.isEmpty ?? true
                        {
                            isType = infoDict["dtype"] as? String
                        }
                        
                        if isType == "video"
                        {
                            
                            let imageVideo = chatVC.getThumbnailFrom(path: URL.init(string: infoDict["message"] as! String)!)
                            infoDict["videoThumb"] = imageVideo
                        }
                        if chatVC.isGroupChat
                        {
                            if senderID != UserDefaults.userName
                            {
                                chatVC.chatMessages.append(infoDict)
                                chatVC.tblChat.reloadData()
                                chatVC.scrollToBottom()
                                
                            }
                        }
                        else
                        {
                            
                            chatVC.addMessageToDB(param: infoDict)
                               // chatVC.chatMessages.append(infoDict)
//                                chatVC.tblChat.reloadData()
//                                chatVC.scrollToBottom()
                            
                            
                        }
                        
                    }
                    else
                    {
                        self?.createInAppNotification(usingModel: infoDict)
                    }
                }
                
            }
            else
            {
                if !infoDict.isEmpty {
                    self?.createInAppNotification(usingModel: infoDict)
                }
            }
            
        }
        
        SocketIOManager.shared().sendChatMessageHandler = {[weak self] result in
            
            guard var infoDict = result else {return}
            print(infoDict)
            
            if let chatVC = self!.window!.visibleViewController() as? ChatViewController {
                
                chatVC.inputTextView.text = ""
                chatVC.textViewDidChange(chatVC.inputTextView)

                if !infoDict.isEmpty {
                    
                    print(infoDict)
                    
                        //If Media Type is Null
                        var isType = infoDict["type"] as? String
                        
                        if isType?.isEmpty ?? true
                        {
                            isType = infoDict["dtype"] as? String
                        }
                        
                        if isType == "video"
                        {
                            
                            let imageVideo = chatVC.getThumbnailFrom(path: URL.init(string: infoDict["message"] as! String)!)
                            infoDict["videoThumb"] = imageVideo
                        }

                    
                    var replaceIndex = Int()
                    var isMessageFound : Bool = false
                        for (dictIndex, chatDataDict) in chatVC.chatMessages.enumerated()
                        {
                            
                            
                            let uidData = chatDataDict["uid"] as! String
                            let uidServreID = infoDict["uid"] as! String
                            
                            print("UIDId Array Val \(uidData)")
                            print("recentSent Message Data \(uidServreID)")
                            if uidData == uidServreID
                            {
                                isMessageFound = true
                                replaceIndex = dictIndex
                            }

                        }

                    if isMessageFound
                    {
                    chatVC.chatMessages[replaceIndex] = infoDict
                    }
                    else
                    {
                    //chatVC.chatMessages.append(infoDict)
                        chatVC.addMessageToDB(param: infoDict)
                    }
        
                    
                    chatVC.tblChat.reloadData()
                    chatVC.scrollToBottom()

                    
                }
            }
            
        }
        
        SocketIOManager.shared().getHistoryHandler = {[weak self] result in
            
            guard let infoDict = result else {return}
            print(infoDict)
            
            if let chatVC = self!.window!.visibleViewController() as? ChatViewController {
                if let resultArray = infoDict["result"] as? [[String:Any]], !resultArray.isEmpty {
                    let info = resultArray.first!
                    print(info)
                    
                    var arrModifiedHistory = [[String:Any]]()
                    for var dictMedia in resultArray
                    {
                        //If Media Type is Null
                        var isType = dictMedia["type"] as? String
                        
                        if isType?.isEmpty ?? true
                        {
                            isType = dictMedia["dtype"] as? String
                        }
                        
                        if isType == "video"
                        {
                            
                            let imageVideo = chatVC.getThumbnailFrom(path: URL.init(string: dictMedia["message"] as! String)!)
                            dictMedia["videoThumb"] = imageVideo
                        }
                        arrModifiedHistory.append(dictMedia)
                        
                    }
                    
                    chatVC.chatMessages = arrModifiedHistory
                    chatVC.replaceDbMessageToServer(dataArry: arrModifiedHistory)

                    chatVC.tblChat.reloadData()
                    if chatVC.chatMessages.count > 0 {
                        chatVC.tblChat.scrollToRow(at: IndexPath (row: chatVC.chatMessages.count - 1, section: 0), at: .top, animated: false)
                    }
                }
            }
            
        }
        
        
        SocketIOManager.shared().chatTypingStatusHandler = {[weak self] result, isTypingStatus in
            
            guard let infoDict = result else {return}
            print(infoDict)
            
            if let chatVC = self!.window!.visibleViewController() as? ChatViewController {
                
                if !infoDict.isEmpty {
                    
                    let senderID = infoDict["senderid"] as? String
                    if chatVC.reciverID == senderID
                    {
                        if isTypingStatus
                        {
                            if !chatVC.isGroupChat
                            {
                        chatVC.labelOnlineStatus.text = "typing..."
                            }
 
                        }
                        else
                        {
                        chatVC.authSocketUserID()
                        }
                    }
                    else
                    {
                       // self?.createInAppNotification(usingModel: infoDict)
                    }
                }
                
            }
        }
        
        
        SocketIOManager.shared().chatOnlineOfflineStatusHandler = {[weak self] result, isOnlineStatus in
            
            guard let infoDict = result else {return}
            print(infoDict)
            
            let arrUsers = infoDict["myId"] as? [[String:Any]]
            
            guard let arrOnlineUsr = arrUsers else {return}
            

            if let chatVC = self!.window!.visibleViewController() as? ChatViewController {
                
                if !arrOnlineUsr.isEmpty {
                    
                    let isUserOnline = arrOnlineUsr.filter({ (dictVal) -> Bool in
                        
                        let strUserID = dictVal["userId"] as! String
                        
                        if chatVC.reciverID == strUserID
                        {
                            if !chatVC.isGroupChat
                            {
                                if isOnlineStatus
                                {
                                    chatVC.labelOnlineStatus.text = "Online"
                                    chatVC.getChatHistory()
                                }
                                else
                                {
                                    chatVC.labelOnlineStatus.text = ""
                                }
                            }
                            
                            return true

                        }
                        else
                            
                        {
                            return false
                        }
                        
                })
                    if !(isUserOnline.count > 0)
                    {
                        if !chatVC.isGroupChat
                        {
                        chatVC.labelOnlineStatus.text = ""
                        }
                        

                    }
                
            }
            
        }
        }
        SocketIOManager.shared().newMessageHandler = {[weak self] result in
            
            guard let infoDict = result else {return}
            print(infoDict)
            
            if let chatVC = self!.window!.visibleViewController() as? ChatListViewController {
                
                print(infoDict)
                
                // If same group chat is open...
                if let arryContact = infoDict["result"] as? [[String:Any]], !arryContact.isEmpty {
                    // Simply add new message model...
                    
                    chatVC.arrRes = arryContact as [[String : AnyObject]]
                   // chatVC.createUsersEntryInDB(dataArry: arryContact)
                    DataBaseManager.sharedInstance().insertValuesToUserListingTable(dataArry: arryContact)
                    chatVC.tblJSON.reloadData()
                    
                }
                else {
                    //self?.createInAppNotification(usingModel: model)
                }
            }
            else if let chatVC = self!.window!.visibleViewController() as? UserFarwardListingVC
            {
                print(infoDict)
                
                // If same group chat is open...
                if let arryContact = infoDict["result"] as? [[String:Any]], !arryContact.isEmpty {
                    // Simply add new message model...
                    
                    chatVC.arrRes = arryContact as [[String : AnyObject]]
                    chatVC.tablViewUsers.reloadData()
                    
                }
                else {
                    //self?.createInAppNotification(usingModel: model)
                }
            }
             else if let chatVC = self!.window!.visibleViewController() as? UserListingViewController
             {
                print(infoDict)
                
                // If same group chat is open...
                if let arryContact = infoDict["result"] as? [[String:Any]], !arryContact.isEmpty {
                    // Simply add new message model...
                    
                    chatVC.arrRes = arryContact as [[String : AnyObject]]
                    chatVC.tablViewUsers.reloadData()
                    
                }
                else {
                    //self?.createInAppNotification(usingModel: model)
                }
            }
            else if let chatVC = self!.window!.visibleViewController() as? GroupUserListingViewController
            {
                print(infoDict)
                
                // If same group chat is open...
                if let arryContact = infoDict["result"] as? [[String:Any]], !arryContact.isEmpty {
                    // Simply add new message model...
                    
                    chatVC.arrRes = arryContact as [[String : AnyObject]]

                    chatVC.tblVwUserListing.reloadData()
                    
                }
                else {
                    //self?.createInAppNotification(usingModel: model)
                }
            }
            
            
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //  SocketIOManager.sharedInstance.closeConnection()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //  SocketIOManager.sharedInstance.establishConnection()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "UD_TalksSocket_io")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    func screenRedirection () {
        
        if (UserDefaults.getUserInformation() != nil ) {
            
            let vwController = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: String.ControllerName.dashboard) as! HomeViewController
            
            
            let navigationController = UINavigationController.init(rootViewController: vwController)
            
            
            self.window?.rootViewController = navigationController
            
            if !SocketIOManager.shared().isSocketConnected() {
                SocketIOManager.shared().connectSocket()
                
                
                
            }
            
        }
        else {
            
            let vwController = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: String.ControllerName.loginVC) as! ViewController
            
            
            let navigationController = UINavigationController.init(rootViewController: vwController)
            
            self.window?.rootViewController = navigationController
        }
    }
    
//    // MARK:- InApp Notification Fire
    func createInAppNotification(usingModel model:[String:Any]) {

        
        let title =  model["sname"] as! String

        let msgType = model["dtype"] as! String
        var message = String()

        
        if msgType == "img"
        {
            message = "Sent a IMAGE"
            
        }
        else if msgType == "location"
        {
            message = "Sent a LOCATION"
            
        }
        else if msgType == "pdf"
        {
            message = "Sent a FILE"
            
        }
        else if msgType == "audio"
        {
            message = "Sent a AUDIO"
            
        }
        else if msgType == "share"
        {
            message = "Sent a CONTACT"
            
        }
        else if msgType == "video"
        {
            message = "Sent a VIDEO"
            
        }
        else if msgType == "emoji"
        {
            message = "Sent a EMOJI"
            
        }
        else
        
        {
            message = model["message"] as! String
        }


//        if model.messageType == "image" {
//            message = "Photo"
//        }
//
//        if model.messageType == "video" {
//            message = "Video"
//        }

        let announce = Announcement (title: title, subtitle: message, image: UIImage.init(named: "defaultProfilePic"), urlImage: nil, duration: 5, interactionType: .none, userInfo: model) { [weak self] (callBackType, str, announce) in

            guard let `self` = self else {return}
            let lgSideMenuVw = self.window?.visibleViewController()
            let rootNavigation = self.window?.rootViewController as! UINavigationController
print(rootNavigation.viewControllers)
            if callBackType == CallbackType.tap {

                print(announce.userInfo as? [String:Any] )

                if (announce.userInfo as? [String:Any]) != nil {

//                     If chat view is open...
                     //let chatVC1 =  as? ChatVC

                    if let chatVC = rootNavigation.visibleViewController as? ChatViewController {

                        let reciverID = model["senderid"] as! String
//                        If same group chat is open...
                        if reciverID == chatVC.reciverID {
                            print("Same Window")
                            // Do nothing
                        }
                        else {
                            // Redirect user

                            //chatVC.getChatHistory()
                            let chatVwCntroller = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: String.ControllerName.chatVC) as! ChatViewController

                            chatVwCntroller.reciverID = model["senderid"] as? String
                            chatVwCntroller.reciverName = model["sname"] as? String

                            rootNavigation.pushViewController(chatVwCntroller, animated: true)
                        }
                    }
                    else {
                   //      Redirect user
                        let chatVwCntroller = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: String.ControllerName.chatVC) as! ChatViewController
                        
                        chatVwCntroller.reciverID = model["senderid"] as? String
                        chatVwCntroller.reciverName = model["sname"] as? String
                        
                        rootNavigation.pushViewController(chatVwCntroller, animated: true)                    }
                }
            }
        }

        InAppNotify.theme.backgroundColor = UIColor.black
        InAppNotify .Show(announce, to: self.window!.visibleViewController() as! UIViewController)
    }
}


//MARK:- Extension PushNotification Methods



extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}
//MARK: - Reachability methods
extension AppDelegate
{
    //MARK:-
    
    /// function to check Reachability
    func checkReachability()
    {
        guard let r = internetReachability else { return }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationInternetConnection), object: r.isReachable)
    }
    
    //MARK:-
    
    /// function to check is Internet Available or not
    ///
    /// - Returns: <#return value description#>
    func isInternetAvailable() -> Bool
    {
        guard let r = internetReachability else { return false}
        return r.isReachable
    }
    
    //MARK:-
    
    /// function for reachability Did Change
    ///
    /// - Parameter notification: Notification reference
    @objc func reachabilityDidChange(_ notification: Notification)
    {
        checkReachability()
    }
}
