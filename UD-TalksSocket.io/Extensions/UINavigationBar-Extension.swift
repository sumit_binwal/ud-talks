//
//  UINavigationBar-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 16/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    static func defaultAppearance() {
        let appearance = UINavigationBar.appearance()
        //appearance.barTintColor = UIColor.clear
        //appearance.backgroundColor = UIColor.clear
        appearance.shadowImage = UIImage()
        //defaultNavBar
        
        let barImg = UIImage.init(named: "defaultNavBar")?.resizableImage(withCapInsets: .init(top: 0, left: 0, bottom: 0, right: 0   ), resizingMode: .stretch)
        
       // let barImg = UIImage.init(named: "navBar")?.resizableImage(withCapInsets: .init(top: 0, left: 0, bottom: 0, right: 0   ), resizingMode: .stretch)
//        UIImage *barImage = [[UIImage imageNamed:@"imageName"]
//            resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)
//            resizingMode:UIImageResizingModeStretch];
        
        appearance.setBackgroundImage(barImg, for: .default)
        appearance.isTranslucent = false
        appearance.tintColor = UIColor.white

        appearance.titleTextAttributes = [
            .foregroundColor : UIColor.white,
            .font : UIFont.setFontTypeBold(withSize: 20)
        ]
    }
    
    func transparentNavigationBar() {
        let appearance = UINavigationBar.appearance()
        appearance.barTintColor = UIColor.clear
        appearance.backgroundColor = UIColor.clear
        appearance.shadowImage = UIImage()
        appearance.setBackgroundImage(UIImage(), for: .default)

        //self.setBackgroundImage(UIImage(), for: .default)
        //self.shadowImage = UIImage()
        self.isTranslucent = true
        self.alpha = 0.0
    }
}
