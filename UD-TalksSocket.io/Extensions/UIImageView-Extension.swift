//
//  UIImageView-Extension.swift
//  Knackel
//
//  Created by Jitendra Singh on 27/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
    
    class func makeRound(imageView : UIImageView){
        
        imageView.layer.cornerRadius = imageView.frame.size.width/2 * scaleFactorX
        imageView.layer.masksToBounds = true
    }
    
    func setImageColor(_ color: UIColor) {
        
        let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
    class func setBorder(imageView: UIImageView, border: Float, color: UIColor){
        
        imageView.layer.borderWidth = CGFloat(border)
        imageView.layer.borderColor = color.cgColor
        imageView.layer.masksToBounds = true
    }
    
    class func setBorderOfImage(imageView: UIImageView, border: Float, color: UIColor, cornerRadious : Float){
        
        imageView.layer.borderWidth = CGFloat(border)
        imageView.layer.borderColor = color.cgColor
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = CGFloat(cornerRadious)
    }
}
