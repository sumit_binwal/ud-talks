//
//  NSAttributedString-Extension.swift
//  WaltzinUser
//
//  Created by Sumit Sharma on 13/09/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit

extension NSAttributedString {
    
    static func labelSetFontAndColorAttribute(withText firstString:String, font:UIFont, color:UIColor, secondString:String, strfont:UIFont, strColor:UIColor ) -> NSAttributedString {
        
        let attrs1 = [NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor : color]
        
        let attrs2 = [NSAttributedString.Key.font : strfont, NSAttributedString.Key.foregroundColor : strColor]
        
        let attributedString1 = NSMutableAttributedString(string:firstString, attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:secondString, attributes:attrs2)
        
        attributedString1.append(attributedString2)
        
        return attributedString1
    }
    
    static func labelSetFontAndColorUnderLineAttribute(withText firstString:String, font:UIFont, color:UIColor, secondString:String, strfont:UIFont, strColor:UIColor, underLineColor : UIColor) -> NSAttributedString {
        
        let attrs1 = [NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor : color]
        
        let attrUnderLine = [NSAttributedString.Key.font : strfont, NSAttributedString.Key.foregroundColor : strColor, NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.underlineColor : underLineColor] as [NSAttributedString.Key : Any]
        
        let attributedString1 = NSMutableAttributedString(string:firstString, attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:secondString, attributes:attrUnderLine)
        
        attributedString1.append(attributedString2)
        
        return attributedString1
    }
}
