//
//  UITableView-Extension.swift
//  Knackel
//
//  Created by Santosh on 05/06/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func add(rows: [Int]) {
        
        self.beginUpdates()
        var indexpaths = [IndexPath] ()
        
        for row in rows {
            let indexpath = IndexPath (row: row, section: 0)
            indexpaths.append(indexpath)
        }
        
        self.insertRows(at: indexpaths, with: .bottom)
        self.endUpdates()
    }
}
