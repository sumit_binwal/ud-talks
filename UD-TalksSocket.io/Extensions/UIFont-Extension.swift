//
//  UIFont-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 09/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
//["Roboto-Regular",
//    "Roboto-Black",
//    "Roboto-BoldItalic",
//    "Roboto-LightItalic",
//    "Roboto-Thin",
//    "Roboto-MediumItalic",
//    "Roboto-Medium",
//    "Roboto-Bold",
//    "Roboto-BlackItalic",
//    "Roboto-Italic",
//    "Roboto-ThinItalic"]
    
    private enum AppFontNameEnum: String {
    
        case fontNameRegular = "Roboto-Regular"
        case fontNameBlack = "Roboto-Black"
        case fontNameBoldItalic = "Roboto-BoldItalic"
        
        case fontNameLightItalic = "Roboto-LightItalic"
        case fontNameThin = "Roboto-Thin"
        case fontNameMediumItalic = "Roboto-MediumItalic"
        case fontNameMedium = "Roboto-Medium"
        case fontNameBold = "Roboto-Bold"
        case fontNameBlackItalic = "Roboto-BlackItalic"
        case fontNameItalic = "Roboto-Italic"
        case fontNameThinItalic = "Roboto-ThinItalic"
    }
    
    static func setFontTypeRegular(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameRegular.rawValue, size: size * scaleFactorX)!
        return font
    }
        
    static func setFontTypeBlack(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameBlack.rawValue, size: size * scaleFactorX)!
        return font
    }
        
    static func setFontTypeBoldItalic(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameBoldItalic.rawValue, size: size * scaleFactorX)!
        return font
    }
        
    static func setFontTypeLightItalic(withSize size: CGFloat) -> UIFont {
        
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameLightItalic.rawValue, size: size * scaleFactorX)!
        return font
    }
        
    static func setFontTypeThin(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameThin.rawValue, size: size * scaleFactorX)!
        return font
    }
    
    static func setFontTypeMediumItalic(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameMediumItalic.rawValue, size: size * scaleFactorX)!
        return font
    }
        
    static func setFontTypeMedium(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameMedium.rawValue, size: size * scaleFactorX)!
        return font
    }
        
    static func setFontTypeBold(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameBold.rawValue, size: size * scaleFactorX)!
        return font
    }
    
    static func setFontTypeBlackItalic(withSize size: CGFloat) -> UIFont {

        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameBlackItalic.rawValue, size: size * scaleFactorX)!
        return font
    }

    static func setFontTypeItalic(withSize size: CGFloat) -> UIFont {
    
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameItalic.rawValue, size: size * scaleFactorX)!
        return font
    }
    static func setFontTypeThinItalic(withSize size: CGFloat) -> UIFont {
        
        var font:UIFont
        font = UIFont (name: AppFontNameEnum.fontNameThinItalic.rawValue, size: size * scaleFactorX)!
        return font
    }
}
