//
//  UIStackVIew-Extension.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 06/01/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit
extension UIStackView {
    
    func addBackground(color: UIColor) {
        let subview = UIView(frame: bounds)
        subview.backgroundColor = color
        subview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subview, at: 0)
    }
    
}
