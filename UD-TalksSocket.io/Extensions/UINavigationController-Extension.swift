//
//  UINavigationController-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 05/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    func doesContain(_ viewControllerType: UIViewController.Type) -> UIViewController?
    {
        let controller = self.viewControllers.first { (controller) -> Bool in
            return controller.isKind(of: viewControllerType)
        }
        //        let typeContains = self.viewControllers.contains { (controller) -> Bool in
        //            return controller.isKind(of: viewControllerType)
        //        }
        guard let finalController = controller else {
            return nil
        }
        return finalController
    }
    
    func updateBar() {

//        self.isNavigationBarHidden = false
//        self.navigationBar.isHidden = false
//        
//        self.navigationBar.titleTextAttributes =
//            [
//                NSAttributedString.Key.font: UIFont.setFontTypeSemiBold(withSize: 21.0)
//        ]
//
//        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        
        // set navigation bar color
       // self.navigationBar.barTintColor = UIColor(red: 15/255.0, green: 15/255.0, blue: 15/255.0, alpha: 0.0)
        
        
        self.navigationBar.transparentNavigationBar()
        
    }
    
    func makeTranslucent()
    {

    }
}
