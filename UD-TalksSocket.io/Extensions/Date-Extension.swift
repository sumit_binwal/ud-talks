//
//  Date-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 18/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    func getStringForFormat(format : String? = "dd MMM yyyy") -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format!
        let dateString : String = dateFormatter.string(from: self)
        return dateString
    }
    
    static func getDate(fromString:String, usingFormat:String? = "MMM d, yyyy") -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = usingFormat!
        
        guard let date = dateFormatter.date(from: fromString) else
        {
            return Date()
        }
        
        return date
    }
    
    static func getISODate(fromString:String, usingFormat:String? = "MMM d, yyyy") -> Date
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = usingFormat!
        dateFormatter.calendar =  Calendar (identifier: .iso8601)
        guard let date = dateFormatter.date(from: fromString) else
        {
            return Date()
        }
        
        return date
    }
    
    static func getDate(withUtcOffset utcOffset:Int, fromDate date: Date) -> Date
    {
        let previousDate = Calendar.current.date(byAdding: .hour, value: utcOffset, to: date)
        return previousDate!
    }
    
    static func getString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: Date ())
    }
    
    
   static func calculateAge(birthday: String) -> Int {
    
        let birthdayDate = Date.getDate(fromString: birthday, usingFormat: "MMM d, yyyy")
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate, to: now, options: [])
        let age = calcAge.year
        return age!
    }
  
    
    static func getZodiac(fromDateString: String, usingFormat:String? = "MMM d, yyyy") -> String {
        
        if fromDateString.trimmedCount == 0 {
            return ""
        }
        
        var zodiacName: String = ""
        
        
        let date = Date.getDate(fromString: fromDateString, usingFormat: usingFormat)
        print(date)
        
        let calendar = Calendar (identifier: .gregorian)
        let dateComponents = calendar .dateComponents([.day,.month], from: date)
        
        let day = dateComponents.day!
        let month = dateComponents.month!
        
        
        switch (month)
        {
        case 1:
            //compare the dates
            if (day<=19)
            {
                zodiacName = "Capricorn"
            }
            else
            {
                zodiacName = "Aquarius"
            }
            
        case 2:
            if (day<=18)
            {
                zodiacName = "Aquarius"
            }
            else
            {
                zodiacName = "Pisces"
            }
            
        case 3:
            if (day<=20)
            {
                zodiacName = "Pisces"
            }
            else
            {
                zodiacName = "Aries"
            }
            
        case 4:
            if (day<=19)
            {
                zodiacName = "Aries"
            }
            else
            {
                zodiacName = "Taurus"
            }
            
        case 5:
            if (day<=20)
            {
                zodiacName = "Taurus"
            }
            else
            {
                zodiacName = "Gemini"
            }
            
        case 6:
            if (day<=20)
            {
                zodiacName = "Gemini"
            }
            else
            {
                zodiacName = "Cancer"
            }
            
        case 7:
            if (day<=22)
            {
                zodiacName = "Cancer"
            }
            else
            {
                zodiacName = "Leo"
            }
            
        case 8:
            if (day<=22)
            {
                zodiacName = "Leo"
            }
            else
            {
                zodiacName = "Virgo"
            }
            
        case 9:
            if (day<=22)
            {
                zodiacName = "Virgo"
            }
            else
            {
                zodiacName = "Libra"
            }
            
        case 10:
            if (day<=22)
            {
                zodiacName = "Libra"
            }
            else
            {
                zodiacName = "Scorpio"
            }
            
        case 11:
            if (day<=21)
            {
                zodiacName = "Scorpio"
            }
            else
            {
                zodiacName = "Sagitarius"
            }
            
        case 12:
            if (day<=21)
            {
                zodiacName = "Sagitarius"
            }
            else
            {
                zodiacName = "Capricorn"
            }
            
        default:
        break
        }
        
        return zodiacName
    }
    
    func timeAgoFrom(currentDate:Date) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(self)
        let latest = (earliest == now) ? self : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
                return "Last year"
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
                return "Last month"
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
                return "Last week"
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
                return "Yesterday"
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
                return "An hour ago"
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
                return "A minute ago"
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
    
    static func getDateBeforeYears(_ years:Int) -> Date
    {
        let previousDate = Calendar.current.date(byAdding: .year, value: -years, to: Date())
        return previousDate!
    }
}
