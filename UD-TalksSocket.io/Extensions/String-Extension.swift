//
//  String-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 10/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    var trimmedCount: Int {
        return trimmed.count
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func isValidEmailAddress() -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                returnValue = false
            }
        }
        catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    static var versionNumber: String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        return version
    }
    
    static var buildNumber: String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleVersion"] as! String
        return version
    }
    
    struct alertMessageForListing {
        static let searchRecord = "no search result found"
        static let notRecordFound = "no record found"
    }
    
    struct alertMessage {
        
        static let phoneNumber = "Please enter phone number"
        static let enterEmail = "Please enter email"
        static let enterPassword = "Please enter password"
        static let validEmail = "Please enter valid email"
        static let minimumPassword = "Please enter minimum 6 digit password"
        
        static let autoDetectLocation = "For Autodetect Location Please Allow Location On Settings."
    }
    
    struct alertSuggestCategory {
        static let selectCategory = "Please select Category."
        static let enterCategoryName = "Please enter Category Name."
        static let enterCategoryDescription = "Please enter Category Description."
    }
    
    struct LabelBidDetailText {
        static let firstMeetingDate = "First Meeting Date"
        static let timing = "Timing"
    }
    
    struct InterestedTaskersText {
        static let firstMeeting = "First Meeting"
        static let bidsReceived = "Bid Received"
        static let declineFirstMeeting = "Do you really want to Decline First Meeting with this Tasker?"
        static let declineAlert = "Do you really want to Decline this"
        static let confirm = "Confirm"
        static let decline = "Decline"
        static let rateTasker = "Rate the Tasker"
        static let reviewBid = "Review Bid"
        static let noSiteSurvey = "No site survey needed"
        static let bidDeadline = "Bids Deadline:"
    }

    struct labelTextCreateBidRequest {
        static let localTaskers = "Local Taskers"
        static let internationalTaskers = "International Taskers"
        static let hourly = "Hourly"
        static let fixed = "Fixed"
        static let upfront = "Upfront"
        static let noUpfront = "No Upfront"
        static let finalDelivery = "Final Delivery"
        static let projectType = "Project Type"
        static let cash = "Cash"
        static let card = "Card"
        static let sitRelax = "You can now sit relax."
        static let onTime = "will reach on time"
        static let provideRequest = "to porvide you the requested service."
        static let choose = "For choosing"
        static let service = "for your service."
        static let orderNo = "Order No."
        static let awaredLocationTrack = "You can track his location half an hour before the service time"
        static let happyServicing = "Happy Servicing!"
        static let providers = "Providers"
        static let locationService = "Location of Services"
        static let selectLocation = "Select Location"
        static let paymentMethod = "Payment Method"
        static let selectPaymentMethod = "Select Payment Method"
        static let deadline = "Deadline for Bids"
        static let selectDeadline = "Select Deadline"
        static let description = "Description"
        static let enterDescription = "Enter Description"
        static let selectProjectType = "Select Project Type"
        static let approximateBudget = "Add Approximate Budget"
        static let approximateHours = "Add Approximate Hours"
        static let budget = "Budget / Time"
        static let enterBudget = "Enter Budget"
        static let enterHours = "Enter Hours"
        static let preferences = "Your Preferences"
        static let selectPreferences = "Select Preferences"
        static let siteSurveyRequired = "Is your project requires site survey?"
        static let date = "Date"
        static let selectDate = "Select Date"
        static let startTime = "Start Time"
        static let selectStartTime = "Select Start Time"
        static let endTime = "End Time"
        static let selectEndTime = "Select End Time"
        static let images = "Your Images"
        static let additionalNotes = "Additional Note"
        static let enterAdditionalNotes = "Enter Additional Notes"
        
    }
 
    struct alertCreateBidRequest {
        static let selectLocation = "Please select service location."
        static let paymentMethod = "Please select Payment Method."
        static let deadline = "Please select Deadline for Bids."
        static let description = "Please enter description."
        static let projectType = "Please select Project Type."
        static let budget = "Please enter budget."
        static let additionalNotes = "Please enter Additional Notes."
        static let siteSurveyDate = "Please select Site Survey Date."
        static let siteSurveyStartTime = "Please select Site Survey Start Time."
        static let siteSurveyEndTime = "Please select Site Survey End Time."
        static let cantUploadMoreImage = "Can't Uplaod more than 5 Images"
        static let instructions = "Please enter some instructions."
        static let startTimeGreater = "Start Time can't be greater than End Time"
        static let endTimeLower = "End Time can't be lower than Start Time"
        static let siteSurveyLocation = "Please select Site Survey location."
        static let bidDeadline7Days = "Bid Deadline can't be set after 7 days"
        static let selectSiteSurvey = "Please select Site Survey Date First."
    }
    
    struct TaskerProfile {
        static let viewAll = "View All"
    }
    
    struct PageTitle {
        static let countryCodeTitle = "Country Code"
        static let selectLanguage = "Select Language"
        static let dashboardTitle = "Jobs"
        static let selectJobTypeTitle = "Select Job Request Type"
        static let chooseCategoryTitle = "Choose Your Category"
        static let suggestServiceTitle = "Suggest Service"
        static let serachCategoryTitle = "Choose Your Category"
        static let createBidJobTitle = "Create your Job"
        static let detailTitle = "Detail"
        static let bidsTitle = "Review Bids"
        static let rateTaskerTitle = "Rate the Tasker"
        static let reviewOfferTitle = "Review the Offer"
        static let reviewBidTitle = "Review the Bid"
        static let awardJobTitle = "Final Contract"
        static let awardedJobTitle = "Awarded"
        static let requestSiteSurveyTitle = "Request for site survey"
        static let taskerProfileTitle = "Tasker Profile"
        
        static let DirectRequestTitle = "Direct Request"
        static let BidRequestTitle = "Bid Request"
        static let PullToRefresh = "Pull to refresh"
        static let BidTitle = "Bid"
        static let OfferTitle = "Offer"
    }
    
    struct placeholderText {
        static let email = "Email"
        static let name = "Name"
        static let password = "Password"
        static let gender = "Gender"
        static let dob = "DOB"
        static let phone = "Phone"
        
        static let enterJobTitle = "Enter Job Title"
        static let searchCategory = "Search Category"
        
        static let rateTaskerFeedback = "Add your feedback here"
    }
    
    struct FilterText {
        static let title = "Filter"
        static let rating = "RATING"
        static let distance = "DISTANCE RANGE"
        static let price = "PRICE RANGE"
        static let locally = "LOCALLY"
        static let applyFilter = "Apply Filter"
        static let resetFilter = "Reset"
        static let alertResetFilter = "Do you really want to reset applied filters?"
    }
    
    struct jobCancelReason {
        static let notInterested = "Not Interested"
        static let foundLocalTaskers = "Found Local Tasker"
        static let jobNotNeeded = "Job is no longer needed"
        static let repostJob = "I will repost the Job"
        static let other = "Other"
    }
    
    struct LableBidRequestDetail {
        static let firstMeetingDate = "First Meeting Date"
        static let timing = "Timing"
        static let to = "to"
    }
    
    struct noRecordText {
        static let dashboardNoData = "Currently no active jobs available here"
        static let noAvailableTaskersData = "No Available Taskers data found"
        static let noFirstMeetingData = "Currently no First Meeting available here"
        static let noBidsData = "Once the site survey will be done you can see the bids posted by interested taskers."
    }
    
    struct ButtonTitle {
        static let signIn = "Sign In"
        static let forgotPassword = "Forgot Password?"
        static let submit = "Submit"
        static let signUp = "Sign Up"
        static let verify = "Verify"
        static let changeNumber = "Change Number ?"
        static let getStarted = "Get Started"
        static let goToDashboard = "Go to Dashboard"
        static let createNewJob = "Create New Job"
        static let next = "Next"
        static let done = "Done"
        static let save = "Save"
        static let cancel = "Cancel"
        static let viewJobPost = "View Job Post"
        static let chat = "Chat"
        static let yes = "Yes"
        static let no = "No"
        static let interestedTaskers = "Interested Taskers"
        static let accept = "Accept"
        static let decline = "Decline"
    }
    
    struct jobStatus {
        static let awarded = "Awarded"
        static let inProgress = "In Progress"
        static let open = "Open"
        static let cancelled = "Cancelled"
    }
    
    struct LabelText {
        
        // login
        static let dontHaveAccount = "Don’t have an account yet?"
        static let loginSignUp = " \("Sign Up" )"
        static let loginWelcome = "Welcome to WALTZiN"
        static let loginOr = "- \("or" ) -"
        static let male = "Male"
        static let female = "Female"
        
        // forgot password
        static let forgotyourPassword = "Forgot your password"
        static let passwordDescription = "Enter your email address to receive further instructions"
        
        // sign Up
        static let haveAccount = "Already have an account?"
        static let signupLogin = " \("Sign In" )"
        static let iAgreeWith = "  \("I agree with" ) "
        static let termsConditions = "Terms and Conditions"
        
        // phone number
        static let verifyNumber = "Verifying your number"
        static let numberDescription = "Enter your phone number with which you want to register your self on WALTZiN"
        
        // otp
        static let otpDescription = "\("A verification code has been sent to your Number" ) "
        static let dontReceived = "Didn't received ?"
        static let resend = " \("Resend" )"
        
        // thanku - share Experirnce
        static let thankYou = "Thank you"
        static let shareExperienceShare = "for sharing your expereince with us."
        static let shareExperienceInvite = "You can invite your friends to join us"
        
        // bid request - create thanku
        static let bidCreatePublished = "Your job has been published"
        static let bidCreateRelax = "You can now sit relax."
        static let bidCreateWait = "and wait for the taskers interest."
        static let bidCreateJobId = "\("Job Id." ) "
        
        // direct request - create thanku
        static let directCreateRequestSent = "Request Sent"
        static let directCreateSuccess = "Your booking request has been sent successfully."
        
        // selct job type
        static let selectJobMethod = "Select Job Post Method"
        static let selectJobDirect = "Direct Service Request"
        static let selectJobDirectDescription = "Chat with taskers first, tell them job details and select the final tasker"
        static let selectJobBidDescription = "Post the job details first and then select the final tasker"
        
        // request of site survey for direct request
        static let noNeedSiteSurvey = "No need of site survey"
    }
    
    struct sliderText {
        static let dashboard = "Dashboard"
        static let myJobs = "My Jobs"
        static let mySchedule = "My Schedule"
        static let myMessages = "My Messages"
        static let myAccount = "My Account"
        static let myReviews = "My Reviews"
        static let suggestService = "Suggest a Service"
        static let settings = "Settings"
        static let invite = "Invite"
        static let waltzerShop = "Waltzer Shop"
        static let logout = "Logout"
    }
    
    struct AWSFolder {
        static let UsersDir = "users/"
        static let JobsDir = "jobs/"
        static let ChatMedia = "chatmedia/"
    }
    
    struct chatStatusString {
        
        static let requestForSurvey = "Request For Site Survey"
        static let waitingFinalOffer = "Waiting For Final Offer"
        static let surveyUnderReview = "Survey Under Review"
        static let reviewOffer = "Review Offer"
        static let surveyConfirm = "Survey Confirmed"
        static let surveyDeclined = "Survey Declined"
        static let offerConfirm = "Confirm Offer"
        static let surveyDecline = "Decline Offer"
        static let awardJob = "Award Job"
        static let awarded = "Awarded"
    }
    
    struct MyApp {
        
        static let AppName = "UD-Talks"
        static let GoogleApiKey = "AIzaSyA_pnzvs_y4pVQOObeLuyjOSJBJtO5td2Y"
        //sumitkonstant@gmail.com/kipl123456
        static let defaultAlertMessage = "WALTZiN User"
        static let defaultErrorMessage = "Some error occured while performing the request. Please try again!"
        static let Alert_TermsAndCond = "You need to accept Terms and Condition before Signing Up."
        static let UserNameContactUS = "Sorry! This username is already in use. If you would like to have access to this username, please Contact Us."
        
        static let TermsAndConditionURL = "http://www.google.com"
        static let PrivacyAndPolicy = "http://www.google.com"
        static let Alert_categoties = "Please pick 3 or more categories to proceed ahead."
        static let UTC_DateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSSZ"
        static let UTC_DateFormatFlights = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        static let AWS_POOL_ID = "ap-south-1:7f166c81-e8a2-413f-8d57-d83616746c0e"
        static let AWS_BUCKET_NAME = "waltzin"
        static let AWS_BASE_URL = "https://s3.ap-south-1.amazonaws.com/waltzin/"
        
        static let Track_Flight_InformationNotFound = "Tracking information could not be found."
        static let SubscriptionPrice = "You can enjoy these perks every month for less than one grande latte >> ONLY $4.99/month!"
        static let SubscriptionDays = "Valid till 30 days"
        static let InAppPurchaseIdentifier = "com.kipl.knackel"
        
        static let ReportUserSuccess = "Reported successfully"
        
        static let ExitRoomTitle = "Exit Room"
        static let ExitRoomDescription = "Do you want to exit this room?"
        
        //StoryBord View Controller Name
        static let languageScreenMessage = "Please select language first."
    }
    
    struct signupAlertMessage
    {
        static let chooseImage = "Please choose profile image"
        static let enterName = "Please enter name"
        static let chooseGender = "Please choose gender"
        static let selectDOB = "Please select date of birth"
        static let chooseTnC = "Please check terms & services"

        //SelectJobType
        static let jobtype = "Please select job type"
        static let jobtitle = "Please enter job name"
        
        //Choose subCategory Alert
        static let subCateAlert = "Please select sub-category first"
    }
    
    struct ControllerName {
        static let dashboard = "dashboardVC"
        static let loginVC = "loginVC"
        static let chatListVC = "ChatListViewController"
        static let chatVC = "ChatViewController"
    }
    
    func capitalizingFirstLetter() -> String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
 
//    static func isSpaceBetweenString(string: String) -> Bool{
//        let whitespaceSet = CharacterSet.whitespaces
//        if !string.trimmingCharacters(in: whitespaceSet).isEmpty {
//            return true
//        }else
//        {
//            return false
//        }
//    }

    static func isValidNumber(textField: UITextField, range: NSRange, string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        if newLength > 10 {
            return false
        }
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        if textField.textInputMode?.primaryLanguage == nil || (textField.textInputMode?.primaryLanguage)! == "emoji" || string != numberFiltered  {
            return false;
        }
        return true
    }
    func isEmptyString() -> Bool
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if newString.isEmpty
        {
            return true
        }
        return false
    }
    
    func getTrimmedText() -> String
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString
    }
    
    
    func base64Encoded() -> String {
        
        var encodedString = ""
        let trimmedText = getTrimmedText()
        
        if let data = trimmedText.data(using: .utf8) {
            encodedString = data.base64EncodedString()
        }
        
        return encodedString
    }
    
    func base64Decoded() -> String {
        
        var decodedString = ""
        if let data = Data (base64Encoded: self) {
            decodedString = String (data: data, encoding: .utf8) ?? ""
        }
        
        return decodedString
    }
    
    static func randomString(length: Int = 20) -> String {
        let base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
