//
//  UIStoryBoardExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import UIKit

/// MARK: - UIStoryboard extension
extension UIStoryboard
{
    /// function to get Dashboard StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getMainStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Main", bundle: nil)
        return storyBoard
    }

    
    /// function to get Dashboard StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getNewScreenStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "NewScreens", bundle: nil)
        return storyBoard
    }
    
    /// function to get Tutorial StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getTutorialStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "TutorialView", bundle: nil)
        return storyBoard
    }
    
    /// function to get Language StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getLanguageStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Language", bundle: nil)
        return storyBoard
    }
    
    /// function to get Login StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getLoginStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Login", bundle: nil)
        return storyBoard
    }
    
    
    /// function to get Signup StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getSignUpStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "SignUp", bundle: nil)
        return storyBoard
    }
    
    
    
    /// function to get Direct Job Detail StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getDirectJobDetailStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "DirectJobDetail", bundle: nil)
        return storyBoard
    }
    
    /// function to get Select Job Type StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getSelectJobTypeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "SelectJobType", bundle: nil)
        return storyBoard
    }
    
    /// function to get Review Offer StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getReviewOfferStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "ReviewOffer", bundle: nil)
        return storyBoard
    }
    
    /// function to get Settings StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getSettingsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Settings", bundle: nil)
        return storyBoard
    }
    
    /// function to get My Jobs StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getMyJobsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "MyJobs", bundle: nil)
        return storyBoard
    }
    
    /// function to get My Reviews StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getMyReviewsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "MyReviews", bundle: nil)
        return storyBoard
    }
    
    /// function to get Available Taskers For Direct Job StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getAvailableTaskersDirectJobVCStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "AvailableTaskersDirectJob", bundle: nil)
        return storyBoard
    }
    
    /// function to get Active Chats for Direct Job StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getActiveChatsDirectJobVCStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "ActiveChatsDirectJob", bundle: nil)
        return storyBoard
    }
    
    /// function to get Category StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getCategoryVCStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Category", bundle: nil)
        return storyBoard
    }
    
    /// function to get My profile StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getMyProfileVCStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "MyProfile", bundle: nil)
        return storyBoard
    }
    
    /// function to get Award the Job StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getAwardJobVCStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "AwardJob", bundle: nil)
        return storyBoard
    }
    
    /// function to get Request For Site Survey StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getRequestSiteSurveyStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "RequestSiteSurvey", bundle: nil)
        return storyBoard
    }
    
    /// function to get Tasker Profile StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getTaskerProfileStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "TaskerProfile", bundle: nil)
        return storyBoard
    }
        /// function to get Request For Country Code StoryBoard
    ///
    /// - Returns: UIStoryboard value

    static func getCountryCodeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "CountryCodeList", bundle: nil)
        return storyBoard
    }
    
    /// function to get Bid request detail StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getBidRequestDetailStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "BidRequestDetail", bundle: nil)
        return storyBoard
    }
    
    /// function to get interested taskers StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getInterestedTaskersStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "InterestedTaskers", bundle: nil)
        return storyBoard
    }
    
    /// function to get Thank You StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getThankYouStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "ThankYou", bundle: nil)
        return storyBoard
    }
    
    /// function to get Chat StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getChatStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Chat", bundle: nil)
        return storyBoard
    }
    
    /// function to get Create Bid Request StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getCreateBidRequestStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "CreateBidRequest", bundle: nil)
        return storyBoard
    }
    
    /// function to get My Messages StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getMyMessagesStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "MyMessages", bundle: nil)
        return storyBoard
    }
    
    /// function to get Waltzer Shop StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getWaltzerShopStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "WaltzerShop", bundle: nil)
        return storyBoard
    }
    
    /// function to get My Schedules StoryBoard
    ///
    /// - Returns: UIStoryboard value
    static func getMySchedulesStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "MySchedules", bundle: nil)
        return storyBoard
    }
}
