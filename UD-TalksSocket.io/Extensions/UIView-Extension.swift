//
//  UIView-Extension.swift
//  WaltzinUser
//
//  Created by Sumit Sharma on 30/08/2018.
//  Copyright © 2018 sumit. All rights reserved.
//
import UIKit

extension UIView {
    
    //    func setViewSize(size:) {
    //        self.si
    //    }
    
    /// set border
    ///
    /// - Parameters:
    ///   - color: UIColor reference
    ///   - cornorRadius: CGFloat value
    ///   - borderWidth: CGFloat value
    func setBorderColor(_ color: UIColor, cornorRadius: CGFloat, borderWidth: CGFloat) {
        self.layer.cornerRadius = cornorRadius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth * scaleFactorX
        self.layer.masksToBounds = true
    }
}
