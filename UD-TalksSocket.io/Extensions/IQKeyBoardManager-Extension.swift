//
//  IQKeyBoardManager-Extension.swift
//  WaltzinUser
//
//  Created by Sumit Sharma on 04/09/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import IQKeyboardManagerSwift

class iqKeyboardmanager {
    
    func setupIQKeyBoardManager() {
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 50
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enable = true
    }
}
