//
//  UIWindow-Extension.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 17/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {
    
    //MARK:-
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController  = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }
    
    //MARK:-
    class func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        
        if vc.isKind(of: UINavigationController.self) {
            let navigationController = vc as! UINavigationController
            return UIWindow.getVisibleViewControllerFrom( vc: navigationController.visibleViewController!)
        }
        else if vc.isKind(of: UITabBarController.self) {
            let tabBarController = vc as! UITabBarController
            return UIWindow.getVisibleViewControllerFrom(vc: tabBarController.selectedViewController!)
        }
        else {
            if let presentedViewController = vc.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            }
            else {
                return vc
            }
        }
    }
}
