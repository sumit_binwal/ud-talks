//
//  UIColorExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import UIKit

/// MARK: - UIColor extension
extension UIColor
{
    struct WaltzinUser
    {
        /// value of dashboard un selected request color
        static let dashboardUnSelectedColor = UIColor (RED: 255, GREEN: 255, BLUE: 255, ALPHA: 0.4)
        
        /// value of TextField PlaceHolder Color
        static let textFieldPlaceHolder = UIColor (RED: 255, GREEN: 255, BLUE: 255, ALPHA: 0.4)
        
        /// value of dashboard selected direct request color
        static let dashboardSelectedDirectRequestColor = UIColor (RED: 0, GREEN: 255, BLUE: 253, ALPHA: 1.0)
        
        /// value of dashboard selected bid request color
        static let dashboardSelectedBidRequestColor = UIColor (RED: 255, GREEN: 0, BLUE: 149, ALPHA: 1.0)
        
        /// value of default cell bg color
        static let defaultCellBgColor = UIColor (RED: 37, GREEN: 37, BLUE: 37, ALPHA: 1.0)
    }
    
    /// class init method
    ///
    /// - Parameters:
    ///   - RED: Float value
    ///   - GREEN: Float value
    ///   - BLUE: Float value
    ///   - ALPHA: Float value
    convenience init(RED : Float, GREEN: Float, BLUE: Float, ALPHA: Float)
    {
        self.init(red: CGFloat(RED/255), green: CGFloat(GREEN/255), blue: CGFloat(BLUE/255), alpha: CGFloat(ALPHA))
    }
}
