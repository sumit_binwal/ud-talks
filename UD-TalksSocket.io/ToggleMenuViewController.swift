//
//  ToggleMenuViewController.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 01/12/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ToggleMenuViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var ArrOfImage = [#imageLiteral(resourceName: "Sell Post"), #imageLiteral(resourceName: "Buy Post"), #imageLiteral(resourceName: "Moments"), #imageLiteral(resourceName: "Images"), #imageLiteral(resourceName: "Take Photo"), #imageLiteral(resourceName: "Video"), #imageLiteral(resourceName: "Take Video"), #imageLiteral(resourceName: "Media Tools"), #imageLiteral(resourceName: "Send Location"), #imageLiteral(resourceName: "Chat Contact"), #imageLiteral(resourceName: "File Upload"), #imageLiteral(resourceName: "Audio File")]
    var ArrOflbl = ["Sell Post","Buy Post","Moments","Images","Take Photo","Video","Take Video","Media Tools","Send Location","Chat Contact","File Upload","Audio File"]
    
    @IBOutlet var ToggleCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        self.ToggleCollectionView.delegate = self
        self.ToggleCollectionView.dataSource = self
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArrOflbl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ToggleCollectionViewCell = ToggleCollectionView.dequeueReusableCell(withReuseIdentifier: "ToggleCell", for: indexPath) as! ToggleCollectionViewCell
        
        cell.ImagesOfToggle.image = ArrOfImage[indexPath.row]
        cell.lblName.text = ArrOflbl[indexPath.row]
        //data will show in[indexpath.row]
        return cell
    }
    //MARK:- For cell spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
