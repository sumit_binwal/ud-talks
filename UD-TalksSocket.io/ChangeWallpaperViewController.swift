//
//  ChangeWallpaperViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 15/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ChangeWallpaperViewController: UIViewController {

    @IBOutlet var collectionWallpaper: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }


}

extension ChangeWallpaperViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10 * scaleFactorX
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 177*scaleFactorX, height: 280*scaleFactorX)
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChatWallpaperCollectionCell.cellIdentifier, for: indexPath) as! ChatWallpaperCollectionCell
        
        let imageName = "wallpaper\(indexPath.row)"
        
        cell.wallpaperImgVw.image = UIImage.init(named: imageName)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let imageName = "wallpaper\(indexPath.row)"

        UserDefaults.saveChatWallpaper(wallpaper: imageName)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
