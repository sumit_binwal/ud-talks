//
//  ToggleCollectionViewCell.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 01/12/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ToggleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var ImagesOfToggle: UIImageView!
    
    @IBOutlet var lblName: UILabel!
    
}
