//
//  AddGroupVIewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 17/02/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class AddGroupVIewController: UIViewController {

    @IBOutlet weak var textFiledGroupName: UITextField!
    @IBOutlet weak var textFieldGroupDiscription: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonClicedAction(_ sender: UIButton) {
        
        if textFiledGroupName.text?.trimmedCount == 0
        {
            UIAlertController.showAlertWith(title: "", message: "Please enter group name first", dismissBloack: {})

        }
        else if textFieldGroupDiscription.text?.trimmedCount == 0
        {
            UIAlertController.showAlertWith(title: "", message: "Please enter group discription first", dismissBloack: {})
        }
        
        else
        {
            let userListingVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "GroupUserListingViewController") as! GroupUserListingViewController
            userListingVC.strGroupName = textFiledGroupName.text!
            userListingVC.strGroupDiscription = textFieldGroupDiscription.text!
            self.navigationController?.pushViewController(userListingVC, animated: true)
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
