//
//  FullImageVIewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 15/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class FullImageVIewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    var arrImageData = [[String:Any]]()
    var dictData = [String:Any]()
    var selectedIndex = Int()
    var imageIndex = Int()
    @IBOutlet weak var collectionVIewImgDetail: UICollectionView!
    override func viewDidLoad()
    {
        
        collectionVIewImgDetail.delegate = self
        collectionVIewImgDetail.dataSource = self
        
        
        collectionVIewImgDetail.layoutIfNeeded()
        collectionVIewImgDetail.selectItem(at: IndexPath.init(row: imageIndex, section: 0), animated: true, scrollPosition: .right)
    }

    
    //MARK:- Custome Action Buttons
    @IBAction func imageFullActionButton(_ sender: UIButton)
    {
        switch sender.tag {
        case 0: //Scan
            collectionVIewImgDetail.scrollToItem(at: IndexPath.init(row: 1, section: 0), at: .left, animated: true)
            break
        case 1 : //Farward
            
            break
        case 2 : //Save
            let cell = collectionVIewImgDetail.cellForItem(at: IndexPath.init(row: selectedIndex, section: 0)) as! ImageDetailCollectionCell
            UIImageWriteToSavedPhotosAlbum(cell.imageViewDetail.image!, nil, nil, nil);

            break
        case 3 : // Delete
 
            UIAlertController.showAlertWith(title: "UD Talk", message: "Do You Really Want to delete this image ?") {
                
                let strMsgID = self.dictData["uid"] as! String
                
                //        strMsgID.ap
                
                let params1 = ["id":UserDefaults.userID,"uid":",'" + strMsgID + "'"] as! [String:String]
                print(params1)
                SocketIOManager.shared().deleteMessage(parameters: params1)
                
                self.navigationController?.popViewController(animated: true)
            }

            break
        default:
            break
        }
    }
}

extension FullImageVIewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 375*scaleFactorX, height: 667*scaleFactorX)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageDetailCollectionCell.cellIdentifier, for: indexPath) as! ImageDetailCollectionCell
        selectedIndex = indexPath.row
        dictData = arrImageData[indexPath.row]
        print(dictData)
        if dictData["type"] as! String == "video"
        {
let imgVdio = arrImageData[indexPath.row]["videoThumb"] as! UIImage
            cell.imageViewDetail.image = imgVdio
            cell.buttonVideo.isHidden = false
            
            cell.buttonVideo.tag = indexPath.row
            cell.buttonVideo.addTarget(self, action: #selector(senderImageButtonClicked(sender:)), for: .touchUpInside)

        }
        else
        {
            let imgURl = arrImageData[indexPath.row]["message"] as! String
            cell.imageViewDetail.sd_setImage(with: URL.init(string: imgURl as! String), completed: nil)
            cell.buttonVideo.isHidden = true

        }


        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let imageName = "wallpaper\(indexPath.row)"
//        
//        UserDefaults.saveChatWallpaper(wallpaper: imageName)
//        
//        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func senderImageButtonClicked(sender:UIButton) {
        print(sender.tag)
        var dictData = arrImageData[sender.tag]
        //If Media Type is Null
        var isType = dictData["type"] as? String
        
        if isType?.isEmpty ?? true
        {
            isType = dictData["dtype"] as? String
        }
        if isType == "video"
        {
            let urlString = dictData["message"] as! String
            let videoURL = NSURL(string: urlString)
            let player = AVPlayer(url: videoURL! as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
                
            }
        }

        
        
    }
    
}
