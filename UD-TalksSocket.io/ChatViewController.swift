
//
//  ChatViewController.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 22/11/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AWSS3
import AWSCore
import SDWebImage
import OpalImagePicker
import Photos
import MobileCoreServices
import AVKit
import AVFoundation
import LocationPickerViewController
import ContactsUI
import MobileCoreServices
import MediaPlayer
import iOSPhotoEditor

/// variable for Max Chat Window Height
enum ChatMessageType : String
{
    case textMessage = "msg"
    case contactShare = "share"
    case image = "img"
    case video = "video"
    case audio = "audio"
    case files = "pdf"
    case location = "location"
    case emoji = "emoji"
    
}


private let MaxChatWindowHeight = 160 * scaleFactorX

class SearchBarView: UIView
{
    override var intrinsicContentSize: CGSize
    {
        return UIView.layoutFittingExpandedSize
    }
}

class ChatViewController: UIViewController, UIGestureRecognizerDelegate {
    var player:AVPlayer?
    
    @IBOutlet weak var tableVwTopConstraint: NSLayoutConstraint!
    @IBOutlet var chatWallpaperBG: UIImageView!
    @IBOutlet var buttonSetting: UIButton!
    @IBOutlet var buttonMenuOpen: UIButton!
    var mediapicker1: MPMediaPickerController!
    
    
    var previousCellIndex:Int = 0
    
    @IBOutlet var viewNavBar: SearchBarView!
    @IBOutlet var labelChatName: UILabel!
    @IBOutlet var labelOnlineStatus: UILabel!
    var selectedIndex = Int()
    
    @IBOutlet var viewSettingMenu: UIView!
    var isSettingMenuViewOpen = false
    
    var isGroupChat = false
    
    var dictGroupData = [String:Any]()
    
    var playerItem:AVPlayerItem?
    let group = DispatchGroup()
    var isMenuViewOpen = false
    
    @IBOutlet var viewFarword: UIView!
    let recordingView = SKRecordView(frame: CGRect(x: 300, y: 300 , width: 300 , height: 100))
    
    var arrFarwardSelectedItem = [[String:Any]]()
    
    var searchBarView = UISearchBar()
    var isSearchBarOpen = false
    var chatSearchMessages = [[String: Any]]()
    
    
    
    @IBOutlet var menuView: UIView!
    @IBOutlet weak var menuStackView: UIStackView!
    
    @IBOutlet var buttonDelete: UIButton!
    @IBOutlet var buttonFarward: UIButton!
    @IBOutlet var buttonCopy: UIButton!
    @IBOutlet var buttonHardDelete: UIButton!
    var longGesture = UILongPressGestureRecognizer()
    var chatMessages = [[String: Any]]()
    
    var nickname: String!
    
    let imagePicker = OpalImagePickerController()
    
    
    var bannerLabelTimer: Timer!
    
    var placeholderLabel : UILabel!
    
    
    /// variable for image picker
    let takeImage = UIImagePickerController()
    
    
    //var courses = [Course]()
    var reciverID:String!
    
    var reciverName:String!
    
    @IBOutlet weak var lblOtherUserActivityStatus: UILabel!
    @IBOutlet weak var inputTextView: KMPlaceholderTextView!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var lblNewsBanner: UILabel!
    @IBOutlet weak var conBottomEditor: NSLayoutConstraint!
    
    /// uiview variable for chat write box
    @IBOutlet weak var containerTextBox: UIView!
    
    /// uiview variable for chat write box background
    @IBOutlet weak var containerTextBoxBackground: UIView!
    
    /// variable for constraint Container Text Box Height
    @IBOutlet weak var constraintContainerTextBoxHeight: NSLayoutConstraint!
    
    /// variable for constraint Container Bottom
    @IBOutlet weak var constraintContainerBottom: NSLayoutConstraint!
    
    /// original Container Text Box Height
    var originalContainerTextBoxHeight : CGFloat = 0
    
    @IBOutlet weak var sentButton: UIButton!
    
    //MARK:- ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Change color of selection overlay to white
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        
        //Change color of image tint to black
        imagePicker.selectionImageTintColor = UIColor.black
        
        imagePicker.delegate = self
        
        //        //Change image to X rather than checkmark
        //        imagePicker.selectionImage = UIImage(named: "x_image")
        
        //Change status bar style
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        
        //Limit maximum allowed selections to 5
        imagePicker.maximumSelectionsAllowed = 5
        
        //Only allow image media type assets
        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        
        //Change default localized strings displayed to the user
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select that many images!", comment: "")
        imagePicker.configuration = configuration
        
        
        showNavigationBar()
        self.title = reciverName
        labelChatName.text = reciverName
        
        
        IQKeyboardManager.shared.enable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetReachabilityChanged), name: NSNotification.Name(rawValue: NotificationInternetConnection), object: nil)
        
        //Code for Wallpaper
        //tblChat.backgroundView = UIImageView(image: UIImage(named: "wallpaper"))
        // you can change the content mode:
        // tblChat.contentMode = UIImageView.ContentMode.scaleToFill
        // imageView.contentMode = .scaleAspectFit
        
        setUpView()
        self.authSocketUserID()
        
        
        if appDelegate.isInternetAvailable()
        {
            getChatHistory()
        }
//        else
//
//        {
        chatMessages = DataBaseManager.sharedInstance().fetchChatHistoryFromDB(userID: reciverID)
        scrollToBottom()
            tblChat.reloadData()
//        }
        
        
    }
    
    func replaceDbMessageToServer(dataArry:[[String:Any]])
    {
        DataBaseManager.sharedInstance().insertValuesToChatTable(dataArry: dataArry, reciverid:reciverID )

    }
    
    func getChatHistory()
    {
        if isGroupChat
        {
            let params1 = ["senderid":UserDefaults.userName,"reciverid":reciverID]
            print(params1)
            SocketIOManager.shared().loadGroupRecieverChat(parameters: params1)
        }
        else
        {
            let params1 = ["senderid":UserDefaults.userID,"reciverid":reciverID]
            print(params1)
            SocketIOManager.shared().loadRecieverChat(parameters: params1)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureTableView()
        
        if UserDefaults.getChatWallpaper().count > 0 
        {
            chatWallpaperBG.image = UIImage.init(named: UserDefaults.getChatWallpaper())
        }
        else
        {
            chatWallpaperBG.image = UIImage.init(named: "wallpaper0")
        }
        
        //configureOtherUserActivityLabel()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        player?.pause()
        //player = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        removeLongPressDilog()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupRecordView()
        
        longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        longGesture.delegate = self
        
        self.tblChat.addGestureRecognizer(longGesture)
        
        
        
    }
    
    
    func authSocketUserID()
    {
        if SocketIOManager.shared().isSocketConnected()
        {
            let param = ["id":UserDefaults.userID,"messageCount":"0","isChatEnable":"true","isDelivered":"true","isReaded":"true","isOnlineStatus":"true"] as! [String:Any]
            
            SocketIOManager.shared().authUserIDtoSocket(parameters: param)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    //MARK:- SetupView Custome Methods
    func setupRecordView()  {
        
        self.view.addSubview(recordingView)
        recordingView.delegate = self
        recordingView.recordingImages = [UIImage(named: "mic")!,UIImage(named: "mic")!,UIImage(named: "mic")!,UIImage(named: "mic")!,UIImage(named: "mic")!,UIImage(named: "mic")!]
        recordingView.normalImage = UIImage(named: "mic")!
        
        let vConsts = NSLayoutConstraint(item:self.recordingView , attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute: .bottom, multiplier: 1.0, constant: -8)
        
        let hConsts = NSLayoutConstraint(item: self.recordingView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: -18)
        
        view.addConstraints([hConsts])
        view.addConstraints([vConsts])
    }
    
    func sizePerMB(url: URL?) -> Double {
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return size.doubleValue / 1000000.0
            }
        } catch {
            print("Error: \(error)")
        }
        return 0.0
    }
    
    func covertToFileString(with size: UInt64) -> String {
        var convertedValue: Double = Double(size)
        var multiplyFactor = 0
        let tokens = ["bytes", "KB", "MB", "GB", "TB", "PB",  "EB",  "ZB", "YB"]
        while convertedValue > 1024 {
            convertedValue /= 1024
            multiplyFactor += 1
        }
        return String(format: "%4.2f %@", convertedValue, tokens[multiplyFactor])
    }
    
    func fetchContentLength(for url: URL, completionHandler: @escaping (_ contentLength: UInt64?) -> ()) {
        
        var request = URLRequest(url: url)
        request.httpMethod = "HEAD"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil, let response = response as? HTTPURLResponse, let contentLength = response.allHeaderFields["Content-Length"] as? String else {
                completionHandler(nil)
                return
            }
            completionHandler(UInt64(contentLength))
        }
        
        task.resume()
    }
    
    func setUpView() {
        
        tblChat.delegate = self
        tblChat.dataSource = self
        
        
        constraintContainerTextBoxHeight.constant = constraintContainerTextBoxHeight.constant * scaleFactorX
        originalContainerTextBoxHeight = constraintContainerTextBoxHeight.constant
        
        containerTextBoxBackground.layer.cornerRadius = 5 * scaleFactorX
        containerTextBoxBackground.layer.borderWidth = 1.0
        containerTextBoxBackground.layer.borderColor = UIColor.clear.cgColor
        containerTextBoxBackground.layer.masksToBounds = true
        
        inputTextView.placeholder = "Say Something..."
        inputTextView.placeholderColor = UIColor.black
        inputTextView.delegate = self
        inputTextView.layoutManager.delegate = self
        
        inputTextView.inputAccessoryView = UIView ()
        
        
        self.navigationItem.hidesBackButton = true
        
        viewNavBar.frame = CGRect (x: 0, y: 0, width: self.view.frame.size.width, height: UIDevice.isIphoneX ? 44 * scaleFactorY : 44)
        self.navigationItem.titleView = viewNavBar
        if isGroupChat
        {
            let strDisc = dictGroupData["description"] as? String
            print(strDisc)
            labelOnlineStatus.text = strDisc        }
        else
        {
            labelOnlineStatus.text = ""
        }
        
        
        
        // Keyobard will change frame notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        // keyboard text input mode change notification
        NotificationCenter.default.addObserver(self, selector: #selector(textInputModeChangedNotification), name: UITextInputMode.currentInputModeDidChangeNotification, object: nil)
        
    }
    
    func configureTableView() {
        tblChat.delegate = self
        tblChat.dataSource = self
        tblChat.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "idCellChat")
        tblChat.estimatedRowHeight = 90.0
        tblChat.rowHeight = UITableView.automaticDimension
        tblChat.tableFooterView = UIView(frame: CGRect.zero)
        
        //Register TableView Cell
        tblChat.register(UINib(nibName: SenderContactCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: SenderContactCell.cellIdentifier)
        tblChat.register(UINib(nibName: ReciverContactCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: ReciverContactCell.cellIdentifier)
        tblChat.register(UINib(nibName: FileSenderCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: FileSenderCell.cellIdentifier)
        tblChat.register(UINib(nibName: FileReciverCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: FileReciverCell.cellIdentifier)
        
        
    }
    
    
    func configureNewsBannerLabel() {
        lblNewsBanner.layer.cornerRadius = 15.0
        lblNewsBanner.clipsToBounds = true
        lblNewsBanner.alpha = 0.0
    }
    
    
    //MARK:- Notification Keyboard Frame Change
    
    /// function of keyboard Will Change Frame
    ///
    /// - Parameter notification: Notification reference
    @objc func keyboardWillChangeFrame(notification: Notification) {
        
        if let userInfo = notification.userInfo {
            
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.constraintContainerBottom.constant = 0.0
            }
            else {
                self.constraintContainerBottom.constant = endFrame?.size.height ?? 0.0
            }
            
            weak var weakSelf = self
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                weakSelf?.view.layoutIfNeeded()
                self.scrollToBottom()
                
            }, completion: nil)
        }
    }
    
    /// function to text Input Mode Changed Notification
    @objc func textInputModeChangedNotification() {
        print(UITextInputMode .activeInputModes)
        self.inputTextView.reloadInputViews()
    }
    
    
    //MARK:- ---> Custom Methods
    
    /// function fired when internet Reachability Changed
    ///
    /// - Parameter notification: Notification reference
    @objc func internetReachabilityChanged(notification: Notification)
    {
        if let isReachable = notification.object as? Bool {
            print("chatinternet ",isReachable)
            
            if isReachable
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                    if SocketIOManager.shared().isSocketConnected()
                    {
                        self.authSocketUserID()
                        
                        let unsentMsgArry = DataBaseManager.sharedInstance().fetchUnsendMessageChatFromDB()
                        
                        for unsentDict in unsentMsgArry
                        {
                            let strMsg = unsentDict["message"]  as! String
                            let msgType = unsentDict["type"] as! String
                            let msgUid = unsentDict["uid"]  as! String
                            
                            
                            if msgType == "msg"
                            {
                                self.sentMessageToSocket(type: .textMessage, message: strMsg , uid: msgUid )
                            }
                            else if msgType == "share"
                            {
                                self.sentMessageToSocket(type: .contactShare, message: strMsg , uid: msgUid)
                            }
                            
                        }
                    }                })
                
                
                
                
            }
        }
    }
    
    
    
    func sentMessageToSocket(type : ChatMessageType, message : String, uid : String)
    {
        var strUidString = String()
        if uid.isEmpty
        {
            strUidString = String.randomString()
        }
        
        if isGroupChat
        {
            //            let param = ["reciverid":self.reciverID,"message":message,"dtype":type.rawValue,"sname":UserDefaults.userName!,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
            
            let param = ["reciverid":self.reciverID,"message":message,"dtype":type.rawValue,"sname":UserDefaults.userName!,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userName!  ] as [String : Any]
            
            
            SocketIOManager.shared().sentGroupMessage(parameters: param)
            
        }
            
        else
            
        {
            // print(param)
            let param = ["reciverid":self.reciverID,"message":message,"dtype":type.rawValue,"sname":UserDefaults.userName!,"did":UserDefaults.getDeviceToken(),"uid":strUidString,"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID!  ] as [String : Any]
            
            if appDelegate.isInternetAvailable()
            {
                SocketIOManager.shared().sentMessage(parameters: param)
            }
            else
            {
                DataBaseManager.sharedInstance().insertDictToChatTable(dictVal: param,isMsgSent: "0")
                chatMessages = DataBaseManager.sharedInstance().fetchChatHistoryFromDB(userID: reciverID)
                inputTextView.text = ""
                tblChat.reloadData()
                scrollTableViewBottom()
                
            }
            
            
            switch type {
            case .location:
                chatMessages.append(param)
                tblChat.reloadData()
                scrollToBottom()
                break
            default:
                break
            }
        }
    }
    
    func addMessageToDB(param : [String:Any])
    {
        DataBaseManager.sharedInstance().insertDictToChatTable(dictVal: param,isMsgSent: "1")
        chatMessages = DataBaseManager.sharedInstance().fetchChatHistoryFromDB(userID: reciverID)
        inputTextView.text = ""
        tblChat.reloadData()
        scrollTableViewBottom()

    }
    
    func scrollToBottom() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { () -> Void in
            if !self.isSearchBarOpen
            {
                if self.chatMessages.count > 0 {
                    let lastRowIndexPath = NSIndexPath(row: self.chatMessages.count - 1, section: 0)
                    self.tblChat.scrollToRow(at: lastRowIndexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            }
            
        }
    }
    
    
    func showBannerLabelAnimated() {
        UIView.animate(withDuration: 0.75, animations: { () -> Void in
            self.lblNewsBanner.alpha = 1.0
            
        }) { (finished) -> Void in
            self.bannerLabelTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: Selector(("hideBannerLabel")), userInfo: nil, repeats: false)
        }
    }
    
    func closeSearchBar()
    {
        searchBarView.removeFromSuperview()
        isSearchBarOpen = false
        tableVwTopConstraint.constant = 0
        tblChat.reloadData()
    }
    
    func hideBannerLabel() {
        if bannerLabelTimer != nil {
            bannerLabelTimer.invalidate()
            bannerLabelTimer = nil
        }
        
        UIView.animate(withDuration: 0.75, animations: { () -> Void in
            self.lblNewsBanner.alpha = 0.0
            
        }) { (finished) -> Void in
        }
    }
    
    
    func handleConnectedUserUpdateNotification(notification: NSNotification) {
        let connectedUserInfo = notification.object as! [String: AnyObject]
        let connectedUserNickname = connectedUserInfo["nickname"] as? String
        lblNewsBanner.text = "User \(connectedUserNickname!.uppercased) was just connected."
        showBannerLabelAnimated()
    }
    
    
    func handleDisconnectedUserUpdateNotification(notification: NSNotification) {
        let disconnectedUserNickname = notification.object as! String
        lblNewsBanner.text = "User \(disconnectedUserNickname.uppercased) has left."
        showBannerLabelAnimated()
    }
    
    func handleUserTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String: AnyObject] {
            var names = ""
            var totalTypingUsers = 0
            for (typingUser, _) in typingUsersDictionary {
                if typingUser != nickname {
                    names = (names == "") ? typingUser : "\(names), \(typingUser)"
                    totalTypingUsers += 1
                }
            }
            
            if totalTypingUsers > 0 {
                let verb = (totalTypingUsers == 1) ? "is" : "are"
                
                lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
                lblOtherUserActivityStatus.isHidden = false
            }
            else {
                lblOtherUserActivityStatus.isHidden = true
            }
        }
        
    }
    
    
    
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        
        do {
            
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
            
        }
        
    }
    
    
    
    // MARK: UIGestureRecognizerDelegate Methods
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    
    func scrollTableViewBottom() {
        
        if !self.isSearchBarOpen
        {
            if chatMessages.count > 0 {
                tblChat.scrollToRow(at: IndexPath (row: chatMessages.count - 1, section: 0), at: .top, animated: false)
            }
        }
    }
}
/// MARK: - extension -> UITextViewDelegate, NSLayoutManagerDelegate -> textfield delegate methods
extension ChatViewController : UITextViewDelegate, NSLayoutManagerDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        //        JsonObject object = new JsonObject();
        //        object.addProperty("reciverid", rid);
        //        object.addProperty("senderid", sd.getKeyId());
        //        mSocket.emit("typing", object);
        
        let param = ["reciverid":self.reciverID,"senderid":UserDefaults.userID ] as [String : Any]
        SocketIOManager.shared().chatTypingStatus(parameters: param)
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        
        let param = ["reciverid":self.reciverID,"senderid":UserDefaults.userID ] as [String : Any]
        SocketIOManager.shared().stopChatTypingStatus(parameters: param)
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text!.count > 0
        {
            self.recordingView.removeFromSuperview()
        }
        else
        {
            self.setupRecordView()
        }
        self.inputTextView.isScrollEnabled = false
        
        let newHeight = textView.intrinsicContentSize.height + (constraintContainerTextBoxHeight.constant - textView.frame.size.height)
        
        constraintContainerTextBoxHeight.constant = max(originalContainerTextBoxHeight, min(newHeight, MaxChatWindowHeight))
        
        if constraintContainerTextBoxHeight.constant >= MaxChatWindowHeight {
            self.inputTextView.isScrollEnabled = true
            return
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveLinear, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 3
    }
}

//MARK: - extension -> UIImagePickerControllerDelegate, UINavigationControllerDelegate -> pick image from camera or galley
extension ChatViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard info[UIImagePickerController.InfoKey.mediaType] != nil else { return }
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
        
        switch mediaType {
        case kUTTypeImage:
            
            let imageName = UtilityClass.getCurrentTimeStamp() + ".jpg"
            
            
            if #available(iOS 11.0, *) {
                //imageViewProfile.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
                
                if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                    
                    print("image url = ", imgUrl)
                    let imgName = imgUrl.lastPathComponent
                    let imageURL = UtilityClass.getImageURL(imageURL: imgName, image: info[UIImagePickerController.InfoKey.editedImage] as! UIImage)
                    
                    uploadImage(usingImage: imageURL as NSURL, fileName: imageName, cellIndex: 0)
                }
                else {
                    print("image url")
                    
                    let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
                    
                    _ = UtilityClass.addFileToFolder("saved", fileName: "save.jpg", fileData: (editedImage?.jpegData(compressionQuality: 0.7))!)
                    
                    if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: "save.jpg") {
                        
                        self.uploadImage(usingImage: fileURL as NSURL, fileName:imageName, cellIndex: 0)
                    }
                }
            }
            else {
                // Fallback on earlier versions
            }
            
            break
        case kUTTypeMovie:
            
            let imageName = UtilityClass.getCurrentTimeStamp() + ".mov"
            
            
            let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as! NSURL
            let videoData = NSData(contentsOf: videoURL as URL)
            //let path = try! FileManager.default.url(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: false)
            //let newPath = path.appendingPathComponent("/videoFileName.mp4")
            //            do {
            //                try videoData?.write(to: newPath)
            //            } catch {
            //                print(error)
            //            }
            
            
            if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                
                print("image url = ", imgUrl)
                let imgName = imgUrl.lastPathComponent
                let imageURL = UtilityClass.getImageURL(imageURL: imgName, image: info[UIImagePickerController.InfoKey.editedImage] as! UIImage)
                
                uploadVideo(usingImage: imageURL as NSURL, fileName: imageName, cellIndex: 0)
            }
            else {
                print("image url")
                
                let editedImage = info[UIImagePickerController.InfoKey.mediaURL]
                
                _ = UtilityClass.addFileToFolder("saved", fileName: "save.mov", fileData: videoData! as Data)
                
                if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: "save.mov") {
                    
                    self.uploadVideo(usingImage: fileURL as NSURL, fileName:imageName, cellIndex: 0)
                }
            }
            
            break
        default:
            break
        }
        
        
        
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    //MARK: - Upload Media Functions
    func uploadImage(usingImage : NSURL, fileName: String, cellIndex:Int) {
        
        let url = usingImage
        
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        uploadRequest.bucket = "udtalks"
        uploadRequest.key = fileName
        uploadRequest.body = (url as NSURL) as URL
        uploadRequest.contentType = "image/jpeg"
        uploadRequest.acl = .publicReadWrite
        
        _ = "jpg"
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueOnSuccessWith { task -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                    switch code {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                    }
                } else {
                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                }
                return nil
            }
            //self.group.leave()
            let uploadOutput = task.result
            print("Upload complete for: \(String(describing: uploadRequest.key))")
            print("uploadOutput: \(String(describing: uploadOutput))")
            
            
            let imageStr = "https://udtalks.sgp1.digitaloceanspaces.com/" + fileName
            self.sentMessageToSocket(type: .image, message: imageStr, uid: "")
            
            return nil
        }
    }
    
    func uploadVideo(usingImage : NSURL, fileName: String, cellIndex:Int) {
        
        
        let url = usingImage
        
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        uploadRequest.bucket = "udtalks"
        uploadRequest.key = fileName
        uploadRequest.body = (url as NSURL) as URL
        uploadRequest.contentType = "movie/mov"
        uploadRequest.acl = .publicReadWrite
        
        _ = "mov"
        
        
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueOnSuccessWith { task -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                    switch code {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                    }
                } else {
                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                }
                return nil
            }
            //self.group.leave()
            let uploadOutput = task.result
            print("Upload complete for: \(String(describing: uploadRequest.key))")
            print("uploadOutput: \(String(describing: uploadOutput))")
            
            
            
            
            if self.isGroupChat
            {
                
                //                let param = ["reciverid":self.reciverID,"message":"https://udtalks.sgp1.digitaloceanspaces.com/" + fileName,"dtype":"video","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                //                SocketIOManager.shared().sentGroupMessage(parameters: param)
            }
            else
            {
                //                let param = ["reciverid":self.reciverID,"message":"https://udtalks.sgp1.digitaloceanspaces.com/" + fileName,"dtype":"video","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                //                SocketIOManager.shared().sentMessage(parameters: param)
            }
            
            
            //
            let fileNameStr = "https://udtalks.sgp1.digitaloceanspaces.com/" + fileName
            self.sentMessageToSocket(type: .video, message: fileNameStr, uid: "")
            
            
            return nil
        }
        // UtilityClass.startAnimating()
        
        _ = "jpg"
        
        //        let keyName = fileName //
        //        let url = usingImage
        //        let remoteName = keyName
        //        let S3BucketName = String.MyApp.AWS_BUCKET_NAME + "/" + String.AWSFolder.ChatMedia.replacingOccurrences(of: "/", with: "")
        //        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        //        uploadRequest.body = (url as NSURL) as URL
        //        uploadRequest.key = remoteName
        //        uploadRequest.bucket = S3BucketName
        //        uploadRequest.contentType = "image/jpeg"
        //        uploadRequest.acl = .publicReadWrite
        //
        //        let transferManager = AWSS3TransferManager.default()
        //        transferManager.upload(uploadRequest).continueWith(block: { (task: AWSTask) -> Any? in
        //
        //            DispatchQueue.main.async {
        //                //       UtilityClass.stopAnimating()
        //            }
        //
        //            if let error = task.error {
        //                print("Upload failed with error: (\(error.localizedDescription))")
        //            }
        //
        //            if let exception = task.error?.localizedDescription {
        //                print("Upload failed with exception (\(exception))")
        //            }
        //
        //            if task.result != nil {
        //
        //                let url = AWSS3.default().configuration.endpoint.url
        //                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
        //
        //                print("Uploaded to:\(String(describing: publicURL))")
        //
        //                self.sentMessageToServer(model: ModelSocketSendMessage(to: self.strReciverID, jobId: self.strJObID, message: keyName, messageType: ModelSocketSendMessage.MessageType.image), cellIndex: cellIndex)
        //            }
        //
        //            return nil
        //        })
    }
    
    
    func uploadAudio(usingImage : NSURL, fileName: String, cellIndex:Int) {
        
        
        let url = usingImage
        
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        uploadRequest.bucket = "udtalks"
        uploadRequest.key = fileName
        uploadRequest.body = (url as NSURL) as URL
        uploadRequest.contentType = "audio/x-caf"
        uploadRequest.acl = .publicReadWrite
        
        _ = "mov"
        
        
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueOnSuccessWith { task -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                    switch code {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                    }
                } else {
                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                }
                return nil
            }
            //self.group.leave()
            let uploadOutput = task.result
            print("Upload complete for: \(String(describing: uploadRequest.key))")
            print("uploadOutput: \(String(describing: uploadOutput))")
            
            
            
            if self.isGroupChat
            {
                
                //                let param = ["reciverid":self.reciverID,"message":"https://udtalks.sgp1.digitaloceanspaces.com/" + fileName,"dtype":"audio","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                //                SocketIOManager.shared().sentGroupMessage(parameters: param)
            }
            else
            {
                //                let param = ["reciverid":self.reciverID,"message":"https://udtalks.sgp1.digitaloceanspaces.com/" + fileName,"dtype":"audio","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                //                SocketIOManager.shared().sentMessage(parameters: param)
            }
            let strAudioUrl = "https://udtalks.sgp1.digitaloceanspaces.com/" + fileName
            self.sentMessageToSocket(type: .audio, message: strAudioUrl, uid: "")
            
            return nil
        }
        
    }
    
    func uploadFiles(usingImage : NSURL, fileName: String, cellIndex:Int) {
        
        
        let url = usingImage
        
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        uploadRequest.bucket = "udtalks"
        uploadRequest.key = fileName
        uploadRequest.body = (url as NSURL) as URL //kUTTypeCompositeContent
        uploadRequest.contentType = "application/pdf"
        uploadRequest.acl = .publicReadWrite
        
        _ = "mov"
        
        
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueOnSuccessWith { task -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                    switch code {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                    }
                } else {
                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                }
                return nil
            }
            //self.group.leave()
            let uploadOutput = task.result
            print("Upload complete for: \(String(describing: uploadRequest.key))")
            print("uploadOutput: \(String(describing: uploadOutput))")
            
            
            if self.isGroupChat
            {
                
                //                let param = ["reciverid":self.reciverID,"message":"https://udtalks.sgp1.digitaloceanspaces.com/" + fileName,"dtype":"pdf","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                //                SocketIOManager.shared().sentGroupMessage(parameters: param)
            }
            else
            {
                //                let param = ["reciverid":self.reciverID,"message":"https://udtalks.sgp1.digitaloceanspaces.com/" + fileName,"dtype":"pdf","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                //                SocketIOManager.shared().sentMessage(parameters: param)
            }
            
            
            let strFileUrl = "https://udtalks.sgp1.digitaloceanspaces.com/" + fileName
            self.sentMessageToSocket(type: .files, message: strFileUrl, uid: "")
            
            
            return nil
        }
        
    }
    
    
}
extension ChatViewController : OpalImagePickerControllerDelegate
{
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        print(assets)
        let mediaValue = assets[0]
        
        if mediaValue.mediaType == .image{
            //do anything for image asset
            var imageArray = [UIImage]()
            for (indexVal, assetsImg ) in  assets.enumerated()
            {
                let requestImageOption = PHImageRequestOptions()
                requestImageOption.deliveryMode = PHImageRequestOptionsDeliveryMode.fastFormat
                
                let manager = PHImageManager.default()
                
                manager.requestImage(for: assetsImg, targetSize: PHImageManagerMaximumSize, contentMode:PHImageContentMode.default, options: requestImageOption) { (image:UIImage!, _) in
                    //imageArray.append(image)
                    let editedImage = image// UIImage.init(data: imgData as Data) as! UIImage
                    
                    let tempImgName:String = String(indexVal)
                    
                    let imageName = UtilityClass.getCurrentTimeStamp() + tempImgName + ".jpg"
                    
                    
                    // let editedImage = imgActual as? UIImage
                    
                    _ = UtilityClass.addFileToFolder("saved", fileName: imageName, fileData: (editedImage?.jpegData(compressionQuality: 0.4))!)
                    
                    if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: imageName) {
                        
                        self.uploadImage(usingImage: fileURL as NSURL, fileName:imageName, cellIndex: 0)
                        
                    }
                }
            }
            
            
        }else if mediaValue.mediaType == .video{
            print("//do anything for video asset")
            
            mediaValue.requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (contentEditingInput, dictInfo) in
                
                
                if let urlString = (contentEditingInput!.avAsset as? AVURLAsset)?.url.absoluteString
                {
                    print("VIDEO URL: ", urlString)
                    let videoURL = URL.init(string: urlString)
                    let imageName = UtilityClass.getCurrentTimeStamp() + ".mov"
                    let videoData = NSData(contentsOf: videoURL as! URL)
                    
                    print("image url")
                    
                    let editedImage = videoURL
                    
                    _ = UtilityClass.addFileToFolder("saved", fileName: "save.mov", fileData: videoData! as Data)
                    
                    if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: "save.mov") {
                        
                        self.uploadVideo(usingImage: fileURL as NSURL, fileName:imageName, cellIndex: 0)
                    }
                    
                }
                
            })
            
            
            
        }else if mediaValue.mediaType == .audio{
            //do anything for audio asset
        }
        
        
        
        dismiss(animated: true, completion: nil)
        
        
    }
    //    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    //    {
    //        return
    //        print(images)
    //
    //        var compressedImg = [UIImage] ()
    //        for (indexVal, img ) in images.enumerated()  {
    //           // group.enter()
    ////            var imgData = NSData()
    ////            imgData = img.jpegData(compressionQuality: 0.6)! as NSData
    //            let editedImage = img// UIImage.init(data: imgData as Data) as! UIImage
    //
    //            let tempImgName:String = String(indexVal)
    //
    //            let imageName = UtilityClass.getCurrentTimeStamp() + tempImgName + ".jpg"
    //
    //
    //           // let editedImage = imgActual as? UIImage
    //
    //            _ = UtilityClass.addFileToFolder("saved", fileName: imageName, fileData: (editedImage.jpegData(compressionQuality: 0.1))!)
    //
    //            if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: imageName) {
    //
    //                self.uploadImage(usingImage: fileURL as NSURL, fileName:imageName, cellIndex: 0)
    //
    //        }
    //
    //
    //        }
    //        dismiss(animated: true, completion: nil)
    //
    //    }
    
    
    
}


extension ChatViewController : LatLongPickDelegate
{
    func didFinishPickLocation(_ latitude: String, longitute: String, name: String) {
        
        var param = [String:Any]()
        if self.isGroupChat
        {
            
            //             param = ["reciverid":self.reciverID,"message":latitude + "," + longitute + "," + name ,"dtype":"location","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
            //            print(param)
            //            SocketIOManager.shared().sentGroupMessage(parameters: param)
        }
        else
        {
            //             param = ["reciverid":self.reciverID,"message":latitude + "," + longitute + "," + name ,"dtype":"location","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
            //            print(param)
            //            SocketIOManager.shared().sentMessage(parameters: param)
        }
        
        let strLocation = latitude + "," + longitute + "," + name
        self.sentMessageToSocket(type: .location, message: strLocation, uid: "")
        
        
        
    }
    
}

extension ChatViewController: SKRecordViewDelegate {
    
    func SKRecordViewDidCancelRecord(_ sender: SKRecordView, button: UIView) {
        
        sender.state = .none
        sender.setupRecordButton(UIImage(named: "mic")!)
        recordingView.audioRecorder?.stop()
        recordingView.recordButton.imageView?.stopAnimating()
        
        containerTextBoxBackground.isHidden = false

        sentButton.isHidden = false
        print("Cancelled")
    }
    
    
    func SKRecordViewDidSelectRecord(_ sender: SKRecordView, button: UIView) {
        tblChat.reloadData()
        sender.state = .recording
        sender.setupRecordButton(UIImage(named: "rec-1")!)
        sender.audioRecorder?.record()
        //recordingView.recordButton.imageView?.startAnimating()
        print("Began ------ " + NSUUID().uuidString)
        containerTextBoxBackground.isHidden = true
        sentButton.isHidden = true
    }
    
    func SKRecordViewDidStopRecord(_ sender : SKRecordView, button: UIView) {
        recordingView.audioRecorder?.stop()
        sender.state = .none
        sender.setupRecordButton(UIImage(named: "mic")!)
        
        recordingView.recordButton.imageView?.stopAnimating()
        
        let fileName = NSUUID().uuidString + ".caf"
        
        let fileManager = FileManager.default
        let destinationUrl = DOCUMENTS_DIRECTORY_URL.appendingPathComponent(fileName)
        do {
            try fileManager.moveItem(at: recordingView.getFileURL(), to: destinationUrl)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        let time = NSDate().timeIntervalSince1970
        
        //        DispatchQueue.main.async {
        //            self.showLatestMessageOnView(msg: "", time: time, messageID: "564783658745647", deliveryStatus: "", msgType: "audio", locationUrl: "", fileName: fileName, replyMessageId: "")
        //        }
        
        //        DispatchQueue.global(qos: .background).async {
        //            UtilityClass.sendAudioToBitBucket(audioURL: destinationUrl, length: 0, type: "audio", audioName: fileName)
        
        //        }
        self.uploadAudio(usingImage: destinationUrl as NSURL, fileName: fileName, cellIndex: 0)
        containerTextBoxBackground.isHidden = false
        sentButton.isHidden = false
        print("-----Done")
    }
    
}

//MARK:- Extention -> TableView DataSource Delegate Methods

extension ChatViewController : UITableViewDataSource, UITableViewDelegate
{
    // MARK: UITableView Delegate and Datasource Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearchBarOpen
        {
            return chatSearchMessages.count
        }
        else
            
        {
            return chatMessages.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var dictData = [String:Any]()
        
        if isSearchBarOpen
        {
            dictData = chatSearchMessages[indexPath.row]
        }
        else
            
        {
            dictData = chatMessages[indexPath.row]
        }
        
        
        selectedIndex = indexPath.row
        let senderID = dictData["send_id"] as? String
        var senderID1 = String()
        if senderID?.isEmpty ?? true
        {
            senderID1 = dictData["senderid"] as! String
        }
        
        //If Media Type is Null
        var isType = dictData["type"] as? String
        
        if isType?.isEmpty ?? true
        {
            isType = dictData["dtype"] as? String
        }
        
        
        var isSingleChatType = !(reciverID  == senderID ?? senderID1)
        if isGroupChat
        {
            isSingleChatType = UserDefaults.mobileNumber == senderID ?? senderID1
        }
        
        if isSingleChatType{
            var cell : ChatSenderTableViewCell
            
            if (isType == "img" || isType == "video" || isType == "emoji")
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageCell", for: indexPath) as! ChatSenderTableViewCell
                //cell.selectionStyle = .none
                
                if isType == "video"
                {
                    
                    cell.senderImageButton.setImage(UIImage.init(named: "videoImg"), for: .normal)
                    
                    let urlString = dictData["message"] as! String
                    if let videoThmb = dictData["videoThumb"] as? UIImage
                    {
                        cell.mediaImageView.image = videoThmb
                    }
                    
                    
                }
                else if (isType == "emoji")
                {
                    cell.mediaImageView.image = UIImage.init(named: dictData["message"] as! String)
                    
                    cell.senderImageButton.setImage(nil, for: .normal)
                    
                }
                else
                {
                    cell.senderImageButton.setImage(nil, for: .normal)
                    cell.mediaImageView.sd_setImage(with: URL.init(string: dictData["message"] as? String ?? "s"), completed: nil)
                }
                
                let isReadMessage = dictData["isread"] as? String
                let isDeliver = dictData["deliver"] as? String
                if isReadMessage == "true" && isDeliver  == "true"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadnDeliver")
                }
                else if isReadMessage == "false" && isDeliver  == "false"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadDliverFalse")
                }
                else
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadFalseDeliverTrue")
                }
                
                if let strUnsent = dictData["isMsgSent"] as? String
                {
                    if strUnsent == "0"
                    {
                        cell.chatStatusImage.image = UIImage.init(named: "crossImg")
                    }
                }
                
                cell.senderImageButton.tag = indexPath.row
                cell.senderImageButton.addTarget(self, action: #selector(senderImageButtonClicked(sender:)), for: .touchUpInside)
                
            }
            else if isType == "share"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: SenderContactCell.cellIdentifier) as! SenderContactCell
                // //cell.selectionStyle = .none
                
                let strDate = dictData["datetime"] as! String
                cell.labelTime.text =                 UtilityClass.getDateStringFromStr(givenDateStr: strDate, andCurrentDateFormat: "yyyy-mm-dd hh:mm:ss", andResultingDateFormat: "hh:mm")
                let strMessage = dictData["message"] as! String
                let arrValues = strMessage.components(separatedBy: ",")
                
                cell.labelName.text = arrValues.first
                cell.buttonSenderCell.tag = indexPath.row
                
                //Chat Read Status
                let isReadMessage = dictData["isread"] as? String
                let isDeliver = dictData["deliver"] as? String
                if isReadMessage == "true" && isDeliver  == "true"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadnDeliver")
                }
                else if isReadMessage == "false" && isDeliver  == "false"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadDliverFalse")
                }
                else
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadFalseDeliverTrue")
                }
                if let strUnsent = dictData["isMsgSent"] as? String
                {
                    if strUnsent == "0"
                    {
                        cell.chatStatusImage.image = UIImage.init(named: "crossImg")
                    }
                }
                
                cell.buttonSenderCell.addTarget(self, action: #selector(contactCellClicked(sender:)), for: .touchUpInside)
                return cell
                
            }
            else if isType == "pdf"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: FileSenderCell.cellIdentifier) as! FileSenderCell
                //cell.selectionStyle = .none
                
                let strMessage = dictData["message"] as! String
                let arrValues = strMessage.components(separatedBy: ".")
                
                //CHatRead Status
                let isReadMessage = dictData["isread"] as? String
                let isDeliver = dictData["deliver"] as? String
                if isReadMessage == "true" && isDeliver  == "true"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadnDeliver")
                }
                else if isReadMessage == "false" && isDeliver  == "false"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadDliverFalse")
                }
                else
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadFalseDeliverTrue")
                }
                
                
                if arrValues.last == "xls" || arrValues.last == "xlsx"
                {
                    cell.imgVwFileType.image = #imageLiteral(resourceName: "xlsFileImage")
                    cell.labelFileType.text = "Excel File"
                }
                else if arrValues.last == "doc" || arrValues.last == "docx"
                {
                    cell.imgVwFileType.image = #imageLiteral(resourceName: "docFileImage")
                    cell.labelFileType.text = "Word File"
                    
                }
                else
                {
                    cell.imgVwFileType.image = #imageLiteral(resourceName: "pdfFileImg")
                    cell.labelFileType.text = "PDF File"
                    
                }
                cell.labelFileSize.text = "File Size"
                
                
                DispatchQueue.global(qos: .background).async {
                    
                    DispatchQueue.main.async {
                        let fileSize = self.fetchContentLength(for: NSURL.init(string: strMessage) as! URL) { (filesize1) in
                            
                            
                            print(filesize1)
                            //                            let filesize11 = self.covertToFileString(with: filesize1!)
                            let filesize11 = "8.34 KB"
                            cell.labelFileSize.text = "\(filesize11)"
                            
                        }
                        
                    }
                }
                
                cell.buttonFile.tag = indexPath.row
                
                cell.buttonFile.addTarget(self, action: #selector(fileCellClicked(sender:)), for: .touchUpInside)
                return cell
                
            }
            else if isType == "location"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: SenderLocationCell.cellIdentifier) as! SenderLocationCell
                //cell.selectionStyle = .none
                
                
                let isReadMessage = dictData["isread"] as? String
                let isDeliver = dictData["deliver"] as? String
                if isReadMessage == "true" && isDeliver  == "true"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadnDeliver")
                }
                else if isReadMessage == "false" && isDeliver  == "false"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadDliverFalse")
                }
                else
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadFalseDeliverTrue")
                }
                
                let strDate = dictData["datetime"] as! String
                cell.labelTime.text =                 UtilityClass.getDateStringFromStr(givenDateStr: strDate, andCurrentDateFormat: "yyyy-mm-dd hh:mm:ss", andResultingDateFormat: "hh:mm")
                cell.buttonLocationCell.tag = indexPath.row
                cell.buttonLocationCell.addTarget(self, action: #selector(locationButtonClicked(sender:)), for: .touchUpInside)
                return cell
            }
            else if isType == "audio"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: AudioSenderCell.cellIdentifier) as! AudioSenderCell
                //cell.selectionStyle = .none
                
                
                let isReadMessage = dictData["isread"] as? String
                let isDeliver = dictData["deliver"] as? String
                if isReadMessage == "true" && isDeliver  == "true"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadnDeliver")
                }
                else if isReadMessage == "false" && isDeliver  == "false"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadDliverFalse")
                }
                else
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadFalseDeliverTrue")
                }
                
                cell.buttonPlay.tag = indexPath.row
                let strDate = dictData["datetime"] as! String
                cell.labelTime.text =                 UtilityClass.getDateStringFromStr(givenDateStr: strDate, andCurrentDateFormat: "yyyy-mm-dd hh:mm:ss", andResultingDateFormat: "hh:mm")
                cell.buttonPlay.addTarget(self, action: #selector(playAudioAction(sender:)), for: UIControl.Event.touchUpInside)
                //   cell.addGestureRecognizer(longGesture)
                return cell
                
            }
            else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "ChatSenderTableViewCell", for: indexPath) as! ChatSenderTableViewCell
                cell.messageLabel.text = dictData["message"] as? String
                //cell.selectionStyle = .none
                
                let isReadMessage = dictData["isread"] as? String
                let isDeliver = dictData["deliver"] as? String
                if isReadMessage == "true" && isDeliver  == "true"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadnDeliver")
                }
                else if isReadMessage == "false" && isDeliver  == "false"
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadDliverFalse")
                }
                else
                {
                    cell.chatStatusImage.image = UIImage.init(named: "chatReadFalseDeliverTrue")
                }
                if let strUnsent = dictData["isMsgSent"] as? String
                {
                    if strUnsent == "0"
                    {
                        cell.chatStatusImage.image = UIImage.init(named: "crossImg")
                    }
                    
                    
                }
                
            }
            cell.senderImage.image = UIImage.init(named: "user")
            cell.addGestureRecognizer(longGesture)
            
            //  cell.senderImage.sd_setImage(with: URL.init(string:
            // arrayChatMessageListing[indexPath.row].userPicture), completed: nil)
            cell.timeLabel.text = dictData["datetime"] as! String
            return cell
        }
            
        else {
            var cell : ChatReceiverTableViewCell
            
            if (isType == "img" || isType == "video" || isType == "emoji") {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverImageCell", for: indexPath) as! ChatReceiverTableViewCell
                //cell.selectionStyle = .none
                
                if isType == "video"
                {
                    cell.receiverImageButton.setImage(UIImage.init(named: "videoImg"), for: .normal)
                    let urlString = dictData["message"] as! String
                    
                    DispatchQueue.global(qos: .background).async {
                        let videoImage = self.getThumbnailFrom(path: URL.init(string: urlString)!)
                        DispatchQueue.main.async {
                            cell.mediaImageView.image = videoImage
                        }
                    }
                }
                else if (isType == "emoji")
                {
                    cell.mediaImageView.image = UIImage.init(named: dictData["message"] as! String)
                    
                    cell.receiverImageButton.setImage(nil, for: .normal)
                    
                }
                else
                {
                    cell.receiverImageButton.setImage(nil, for: .normal)
                    cell.mediaImageView.sd_setImage(with: URL.init(string: dictData["message"] as? String ?? "2"), completed: nil)
                }
                
                cell.receiverImageButton.tag = indexPath.row
                cell.receiverImageButton.addTarget(self, action: #selector(senderImageButtonClicked(sender:)), for: .touchUpInside)
                
            }
            else if isType == "share"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: ReciverContactCell.cellIdentifier) as! ReciverContactCell
                //cell.selectionStyle = .none
                
                let strDate = dictData["datetime"] as! String
                cell.labelTime.text =                 UtilityClass.getDateStringFromStr(givenDateStr: strDate, andCurrentDateFormat: "yyyy-mm-dd hh:mm:ss", andResultingDateFormat: "hh:mm")

                let strMessage = dictData["message"] as! String
                let arrValues = strMessage.components(separatedBy: ",")
                
                cell.labelName.text = arrValues.first
                cell.buttonReciverCell.tag = indexPath.row
                
                cell.buttonReciverCell.addTarget(self, action: #selector(contactCellClicked(sender:)), for: .touchUpInside)
                return cell
                
            }
            else if isType == "pdf"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: FileReciverCell.cellIdentifier) as! FileReciverCell
                //cell.selectionStyle = .none
                
                let strMessage = dictData["message"] as! String
                let arrValues = strMessage.components(separatedBy: ".")
                
                if arrValues.last == "xls" || arrValues.last == "xlsx"
                {
                    cell.imgVwFileType.image = #imageLiteral(resourceName: "xlsFileImage")
                    cell.labelFileType.text = "Excel File"
                }
                else if arrValues.last == "doc" || arrValues.last == "docx"
                {
                    cell.imgVwFileType.image = #imageLiteral(resourceName: "docFileImage")
                    cell.labelFileType.text = "Word File"
                    
                }
                else
                {
                    cell.imgVwFileType.image = #imageLiteral(resourceName: "pdfFileImg")
                    cell.labelFileType.text = "PDF File"
                    
                }
                
                
                cell.labelFileSize.text = "File Size"
                
                DispatchQueue.global(qos: .background).async {
                    
                    DispatchQueue.main.async {
                        let fileSize = self.fetchContentLength(for: NSURL.init(string: strMessage) as! URL) { (filesize1) in
                            
                            
                            print(filesize1)
                            //  let filesize11 = self.covertToFileString(with: filesize1!)
                            let filesize11 = "8.34 KB"
                            
                            cell.labelFileSize.text = "\(filesize11)"
                            
                        }
                        
                    }
                }
                
                cell.buttonFile.tag = indexPath.row
                
                cell.buttonFile.addTarget(self, action: #selector(fileCellClicked(sender:)), for: .touchUpInside)
                return cell
                
            }
            else if isType == "location"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: ReciverLocationCell.cellIdentifier) as! ReciverLocationCell
                //cell.selectionStyle = .none
                
                let strDate = dictData["datetime"] as! String
                cell.labelTime.text =                 UtilityClass.getDateStringFromStr(givenDateStr: strDate, andCurrentDateFormat: "yyyy-mm-dd hh:mm:ss", andResultingDateFormat: "hh:mm")
                cell.buttonLocationCell.tag = indexPath.row
                cell.buttonLocationCell.addTarget(self, action: #selector(locationButtonClicked(sender:)), for: .touchUpInside)
                return cell
            }
            else if isType == "audio"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: AudioReciverCell.cellIdentifier) as! AudioReciverCell
                //cell.selectionStyle = .none
                
                //                cell.viewSender.isHidden = false
                //                cell.viewReceiver.isHidden = true
                //
                //                cell.labelSendTime.text = dictData["datetime"] as! String
                //
                //                cell.sliderSender.minimumValue = 0
                //               // cell.sliderSender.value = Float(chatModel.audioPlayProgress)
                //
                cell.buttonPlay.tag = indexPath.row
                let strDate = dictData["datetime"] as! String
                cell.labelTime.text =                 UtilityClass.getDateStringFromStr(givenDateStr: strDate, andCurrentDateFormat: "yyyy-mm-dd hh:mm:ss", andResultingDateFormat: "hh:mm")
                cell.buttonPlay.addTarget(self, action: #selector(playAudioAction(sender:)), for: UIControl.Event.touchUpInside)
                // cell.addGestureRecognizer(longGesture)
                return cell
                
            }
            else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "ChatReceiverTableViewCell", for: indexPath) as! ChatReceiverTableViewCell
                cell.messageLabel.text = dictData["message"] as? String
            }
            
            cell.receiverImage.image = UIImage.init(named: "user")
            //  cell.senderImage.sd_setImage(with: URL.init(string: arrayChatMessageListing[indexPath.row].userPicture), completed: nil)
            cell.timeLabel.text = dictData["datetime"] as! String
            //            cell.addGestureRecognizer(longGesture)
            
            return cell
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictData = chatMessages[indexPath.row]
        if tableView.allowsMultipleSelection
        {
            let strMessageID = dictData["id"] as? String ?? "0"
            if arrFarwardSelectedItem.contains(where: { (dictVal) -> Bool in
                if dictVal["id"] as! String == strMessageID
                {
                    return true
                }
                else
                {
                    return false
                    
                }
            })
            {
                for (index,dictVal) in arrFarwardSelectedItem.enumerated()
                {
                    let strMessage = dictData["id"] as? String ?? "0"
                    if strMessage == dictVal["id"]as? String ?? "0"
                    {
                        arrFarwardSelectedItem.remove(at: index)
                    }
                    
                }
            }
            else
            {
                print("array main isme nahin hai")
                
                arrFarwardSelectedItem.append(dictData)
            }
            print(arrFarwardSelectedItem.count)
            // print(arrFarwardSelectedItem)
        }
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let dictData = chatMessages[indexPath.row]
        if tableView.allowsMultipleSelection
        {
            let strMessageID = dictData["id"] as? String ?? "0"
            if arrFarwardSelectedItem.contains(where: { (dictVal) -> Bool in
                if dictVal["id"] as! String == strMessageID
                {
                    return true
                }
                else
                {
                    return false
                    
                }
            })
            {
                for (index,dictVal) in arrFarwardSelectedItem.enumerated()
                {
                    let strMessage = dictData["id"] as? String ?? "0"
                    if strMessage == dictVal["id"]as? String ?? "0"
                    {
                        arrFarwardSelectedItem.remove(at: index)
                    }
                    
                }
                
            }
            else
            {
                print("array main isme nahin hai")
                
                arrFarwardSelectedItem.append(dictData)
            }
            print(arrFarwardSelectedItem.count)
            // print(arrFarwardSelectedItem)
        }
    }
}


//MARK:- Extention -> Custome IBAction Methods
extension ChatViewController
{
    @IBAction func backButtonAction(sender: UIButton){
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func sendMessage(sender: AnyObject){
        
        if inputTextView.text.trimmedCount == 0 {
            return
        }
        
        if self.isGroupChat
        {
            
            //            let param = ["reciverid":reciverID,"message":inputTextView.text,"dtype":"msg","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userName  ] as [String : Any]
            //            print(param)
            //            SocketIOManager.shared().sentGroupMessage(parameters: param)
        }
        else
        {
            //            let param = ["reciverid":reciverID,"message":inputTextView.text,"dtype":"msg","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
            //            SocketIOManager.shared().sentMessage(parameters: param)
        }
        
        self.sentMessageToSocket(type: .textMessage, message: inputTextView.text, uid: "")
        
        
        // sender.isUserInteractionEnabled = false
        //self.sentMessageToServer(model:(ModelSocketSendMessage(to: strReciverID, jobId: strJObID, message: inputTextView.text, messageType: ModelSocketSendMessage.MessageType.text)), cellIndex: arrayChatMessageListing.count+1)
        
        //        if inputTextView.text.characters.count > 0 {
        //            SocketIOManager.sharedInstance.sendMessage(message: inputTextView.text!, withNickname: nickname ?? "KUKIII")
        //
        //            //chatMessages.append()
        //            //self.chatMessages.append(MessageEditor.text!) as [String : AnyObject]
        //            //tblChat.reloadData()
        //            //let indexPath = IndexPath(item: chatMessages.count, section: 0)
        //            //tblChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
        //            inputTextView.text = ""
        //            inputTextView.resignFirstResponder()
        //
        //            chatMessages.append(inputTextView?.text)
        //        }
    }
    
    @IBAction func btnEmojiList(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmojiViewController") as! EmojiViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        //self.navigationController?.pushViewController(EmojiViewController, animated: true)
    }
    
    @IBAction func pickImagesClickedAction(_ sender: UIButton) {
        switch sender.tag {
        case 0://Sell Post
            break
        case 1:// Buy Post
            break
        case 2:// Moments
            break
        case 3:// Images
            
            let imagePicker = OpalImagePickerController()
            imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
            //Change default localized strings displayed to the user
            let configuration = OpalImagePickerConfiguration()
            configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than five Image!", comment: "")
            imagePicker.configuration = configuration
            configuration.navigationTitle = "Photos"
            imagePicker.imagePickerDelegate = self as OpalImagePickerControllerDelegate
            self.present(imagePicker, animated: true, completion: nil)
            
            
        case 4:// Take Photos
            
            self.takeImage.sourceType     = .camera
            self.takeImage.allowsEditing  = false
            self.takeImage.delegate       = self
            self.present(self.takeImage, animated: true, completion: nil)
            break
            
        case 5:// Video
            let videoPicker = OpalImagePickerController()
            videoPicker.allowedMediaTypes = Set([PHAssetMediaType.video])
            videoPicker.maximumSelectionsAllowed = 1
            videoPicker.imagePickerDelegate = self as OpalImagePickerControllerDelegate
            
            //Change default localized strings displayed to the user
            let configuration = OpalImagePickerConfiguration()
            configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than one video!", comment: "")
            videoPicker.configuration = configuration
            configuration.navigationTitle = "Videos"
            self.present(videoPicker, animated: true, completion: nil)
            
            break
        case 6:// Take Video
            self.takeImage.sourceType     = .camera
            self.takeImage.allowsEditing  = false
            self.takeImage.delegate       = self
            self.takeImage.mediaTypes = [kUTTypeMovie] as [String]
            self.takeImage.videoQuality = UIImagePickerController.QualityType.typeMedium
            self.takeImage.videoMaximumDuration = 180
            
            self.present(self.takeImage, animated: true, completion: nil)
            break
        case 7 : //Media Tool
            let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
            
            //PhotoEditorDelegate
            photoEditor.photoEditorDelegate = self
            
            //The image to be edited
            let frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 1920, height: 1080))
            let cgImage = CIContext().createCGImage(CIImage(color: .black), from: frame)!
            let uiImage = UIImage(cgImage: cgImage)

            
            photoEditor.image = uiImage
            
            
            
            
            //Stickers that the user will choose from to add on the image
            // photoEditor.stickers.append(UIImage(named: "sticker" )!)
            
            //Optional: To hide controls - array of enum control
            photoEditor.hiddenControls = []
            
            //Optional: Colors for drawing and Text, If not set default values will be used
            photoEditor.colors = [.red,.blue,.green,.black,.gray,.yellow,.white]
            
            //Present the View Controller
            present(photoEditor, animated: true, completion: nil)
            
            
            break
        case 8 : //Send Location
            
            let customLocationPicker = CustomLocationPicker()
            customLocationPicker.latlongDelegate = self
            let navigationController = UINavigationController(rootViewController: customLocationPicker)
            present(navigationController, animated: true, completion: nil)
            
            break
        case 9 : //Chat Contact
            let userListVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "UserListingViewController") as! UserListingViewController
            userListVC.delegate = self
            self.navigationController?.pushViewController(userListVC, animated: true)
            
            break
        case 10 : //File
            let documentVw = UIDocumentPickerViewController.init(documentTypes: [ kUTTypeCompositeContent as String], in: .import)
            documentVw.delegate = self
            documentVw.allowsMultipleSelection = false
            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
            
            present(documentVw, animated: true, completion: {
                UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
            })
            
            
            break
        case 11 : //audio File
            
            let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.music)
            mediaPicker.allowsPickingMultipleItems = false
            mediapicker1 = mediaPicker
            mediaPicker.delegate = self 
            present(mediapicker1, animated: true, completion: nil)
            
            break
        default:
            break
        }
        
        closeMenuView()
    }
    
    
    func closeMenuView()
    {
        self.view.endEditing(true)
        if isMenuViewOpen
        {
            self.menuView.removeFromSuperview()
            self.isMenuViewOpen = false
            // buttonSetting.setImage(UIImage.init(named: "chatMenuIcon"), for: .normal)
            buttonMenuOpen.setImage(UIImage.init(named: "chatMenuIcon"), for: .normal)
            
        }
        if isSettingMenuViewOpen
        {
            buttonSetting.setImage(UIImage.init(named: "chatSettingIcon"), for: .normal)
            self.viewSettingMenu.removeFromSuperview()
            self.isSettingMenuViewOpen = false
            
        }
    }
    
    
    @IBAction func showMenuView(sender:UIButton)
    {
        self.view.endEditing(true)
        closeSearchBar()
        if !isMenuViewOpen {
            closeMenuView()
            
            sender.setImage(UIImage.init(named: "chatMenuIconSelected"), for: .normal)
            self.view.addSubview(menuView)
            menuView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.isMenuViewOpen = true
            
        }
        else {
            sender.setImage(UIImage.init(named: "chatMenuIcon"), for: .normal)
            self.menuView.removeFromSuperview()
            self.isMenuViewOpen = false
        }
        
        
    }
    
    @IBAction func settingButtonItemsAction(sender:UIButton)
    {
        
        switch sender.tag {
        case 0: //Add Address
            let newContact = CNMutableContact()
            newContact.phoneNumbers.append(CNLabeledValue(label: "home", value: CNPhoneNumber(stringValue: reciverName)))
            let contactVC = CNContactViewController.init(forNewContact: newContact)
            contactVC.contactStore = CNContactStore()
            contactVC.delegate = self
            contactVC.allowsActions = false
            let navController = UINavigationController(rootViewController: contactVC)
            //            navigationController.navigationBar.tintColor = UIColor.white
            //For presenting the vc you have to make it navigation controller otherwise it will not work, if you already have navigatiation controllerjust push it you dont have to make it a navigation controller
            self.present(navController, animated: true, completion: nil)
            
            
            break
        case 1: //Search
            
            searchBarView = UISearchBar.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
            searchBarView.showsCancelButton = true
            
            searchBarView.delegate = self
            
            self.view.addSubview(searchBarView)
            isSearchBarOpen = true
            tableVwTopConstraint.constant = 60
            break
        case 2: //Media
            var arrMedia = [[String:Any]]()
            
            for var dictMedia in chatMessages
            {
                //If Media Type is Null
                var isType = dictMedia["type"] as? String
                
                if isType?.isEmpty ?? true
                {
                    isType = dictMedia["dtype"] as? String
                }
                
                if isType == "img" || isType == "video" || isType == "audio" || isType == "location"
                {
                    
                    if isType == "video"
                    {
                        
                        let imageVideo = getThumbnailFrom(path: URL.init(string: dictMedia["message"] as! String)!)
                        dictMedia["videoThumb"] = imageVideo
                    }
                    arrMedia.append(dictMedia)
                    
                }
                
            }
            if arrMedia.count > 0
            {
                let mediaVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "ChatMediaViewController") as! ChatMediaViewController
                mediaVC.arrMedia = arrMedia
                
                self.navigationController?.pushViewController(mediaVC, animated: true)
            }
            
            break
        case 3: //Mute Notification
            break
        case 4: //Add Wallpaper
            
            let changWallVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "ChangeWallpaperViewController") as! ChangeWallpaperViewController
            self.navigationController?.pushViewController(changWallVC, animated: true)
            
            break
        case 5: //Clear Chat
            
            
            var arrChatID = [String]()
            for dictVal in chatMessages
            {
                let strChatID = "'\(dictVal["uid"]!)'"
                arrChatID.append(strChatID)
            }
            
            
            
            let strCommaValue = arrChatID.joined(separator: ",")
            let params1 = ["id":UserDefaults.userID,"uid":"," + strCommaValue] as! [String:String]
            print(params1)
            SocketIOManager.shared().deleteMessage(parameters: params1)
            
            chatMessages.removeAll()
            tblChat.reloadData()
            
            break
        default:
            break
        }
        
        closeMenuView()
    }
    
    
    @IBAction func buttonSettingAction(sender: UIButton)
    {
        self.view.endEditing(true)
        closeSearchBar()
        
        if !isSettingMenuViewOpen {
            closeMenuView()
            
            sender.setImage(UIImage.init(named: "chatSettingIconSelected"), for: .normal)
            self.view.addSubview(viewSettingMenu)
            viewSettingMenu.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.isSettingMenuViewOpen = true
            
        }
        else {
            sender.setImage(UIImage.init(named: "chatSettingIcon"), for: .normal)
            self.viewSettingMenu.removeFromSuperview()
            self.isSettingMenuViewOpen = false
        }
    }
    
    
    
    @objc func methodA()
        
    {
        performSegue(withIdentifier: "segA", sender: nil)
        
        let vcToggle = self.storyboard?.instantiateViewController(withIdentifier: "ToggleMenuViewController") as! ToggleMenuViewController
        self.navigationController?.pushViewController(vcToggle, animated: true)
    }
    
    @objc func methodB()
        
    {
        performSegue(withIdentifier: "segB", sender: nil)
        
        let vcSetting = self.storyboard?.instantiateViewController(withIdentifier: "SettingMenuViewController") as! SettingMenuViewController
        self.navigationController?.pushViewController(vcSetting, animated: true)
    }
    
    @IBAction func fileCellClicked(sender:UIButton)
    {
        var dictData = isSearchBarOpen ? chatSearchMessages[sender.tag] : chatMessages[sender.tag]
        
        
        
        let strURLLInk = dictData["message"] as! String
        
        let wbViewVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
        wbViewVC.pageURL = strURLLInk
        self.navigationController?.pushViewController(wbViewVC, animated: true)
        
        //        UtilityClass.openSafariController(usingLink: strURLLInk, onViewController: self)
    }
    
    @IBAction func contactCellClicked(sender:UIButton)
    {
        //var dictData = isSearchBarOpen ? chatSearchMessages[sender.tag] : chatMessages[sender.tag]
        
        let profileVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        profileVC.profileName = reciverName
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    @IBAction func locationButtonClicked(sender:UIButton)
    {
        var dictData = isSearchBarOpen ? chatSearchMessages[sender.tag] : chatMessages[sender.tag]
        let strLatLongData = dictData["message"] as! String
        let arrValues = strLatLongData.components(separatedBy: ",")
        
        print(arrValues)
        let locationVwCntroller = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: "LocationFullViewController") as! LocationFullViewController
        locationVwCntroller.latitude = arrValues[0]
        locationVwCntroller.longitude = arrValues[1]
        locationVwCntroller.strPlaceName = arrValues[2]
        self.navigationController?.pushViewController(locationVwCntroller, animated: true)
        
    }
    @IBAction func senderImageButtonClicked(sender:UIButton) {
        print(sender.tag)
        var dictData = isSearchBarOpen ? chatSearchMessages[sender.tag] : chatMessages[sender.tag]
        //If Media Type is Null
        var isType = dictData["type"] as? String
        
        if isType?.isEmpty ?? true
        {
            isType = dictData["dtype"] as? String
        }
        if isType == "video"
        {
            let urlString = dictData["message"] as! String
            let videoURL = NSURL(string: urlString)
            let player = AVPlayer(url: videoURL! as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
                
            }
        }
        else if isType == "img"
        {
            let fullImgVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "FullImageVIewController") as! FullImageVIewController
            
            
            
            fullImgVC.arrImageData = chatMessages.filter({ (dictVal) -> Bool in
                
                var isType = dictVal["type"] as? String
                
                if isType?.isEmpty ?? true
                {
                    isType = dictVal["dtype"] as? String
                }
                if isType  == "img" || isType == "video"
                {
                    return true
                }
                
                return false
            })
            print(dictData)
            let imgURL = dictData["message"] as! String
            for (indexVal, imgName) in fullImgVC.arrImageData.enumerated()
            {
                if imgName["message"] as! String == imgURL
                {
                    fullImgVC.imageIndex = indexVal
                }
            }
            
            
            print(fullImgVC.arrImageData)
            self.navigationController?.pushViewController(fullImgVC, animated: true)
        }
        
        
    }
    @objc func longTap(_ sender: UILongPressGestureRecognizer){
        print("Long tap")
        
        print(sender.view?.tag)
        
        //cell?.contentView.backgroundColor = UIColor.gray
        
        
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            
            let touchPoint = longGesture.location(in: self.tblChat)
            if let indexPath = tblChat.indexPathForRow(at: touchPoint) {
                print(indexPath)
                buttonDelete.tag = indexPath.row
                buttonFarward.tag = indexPath.row
                buttonCopy.tag = indexPath.row
                buttonHardDelete.tag = indexPath.row
                
            }
            
            
            viewFarword.frame = CGRect.init(x: 0, y: 0, width: 375 * scaleFactorX, height: 50 * scaleFactorX)
            self.view .addSubview(viewFarword)
            
            tblChat.allowsMultipleSelection = true
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            //Do Whatever You want on Began of Gesture
        }
    }
    
    @IBAction func playAudioAction(sender: UIButton) {
        
        
        
        var model = isSearchBarOpen ? chatSearchMessages[sender.tag] : chatMessages[sender.tag]
        
        //        if previousCellIndex != sender.tag
        //        {
        //            tblChat.reloadData()
        //        }
        
        
        let indexPathRow:Int = previousCellIndex
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        tblChat.reloadRows(at: [indexPosition], with: .none)
        
        
        let urlOfFile = model["message"] as! String
        print(urlOfFile)
        
        
        let senderID = model["send_id"] as? String
        var senderID1 = String()
        if senderID?.isEmpty ?? true
        {
            senderID1 = model["senderid"] as! String
        }
        
        player?.pause()
        player = nil

        if !(reciverID  == senderID ?? senderID1)
        {
            var cell : AudioSenderCell = tblChat.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! AudioSenderCell
            
            cell.seekbarSlider.minimumValue = 0.0
            
            if sender.tag != previousCellIndex
            {
                let url = URL(string: urlOfFile)
                let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
                player = AVPlayer(playerItem: playerItem)
                player!.play()
                cell.buttonPlay.setImage(UIImage.init(named: "pause"), for: .normal)
                
            }
            
            if self.player == nil
            {
                let url = URL(string: urlOfFile)
                let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
                player = AVPlayer(playerItem: playerItem)
                player!.play()
                cell.buttonPlay.setImage(UIImage.init(named: "pause"), for: .normal)
                
            }
            else if self.player!.timeControlStatus == .playing {
                
                cell.buttonPlay.setImage(UIImage.init(named: "Play"), for: .normal)
                self.player?.pause()//
            }
            else if self.player!.timeControlStatus == .paused {
                do {
                    cell.buttonPlay.setImage(UIImage.init(named: "pause"), for: .normal)
                    if player != nil
                    {
                        player!.seek(to: CMTime.init(seconds: Double(cell.seekbarSlider!.value)
                            , preferredTimescale: 1))
                    }
                    
                    player?.play()
                    
                }
            }
            
            
            
            
            let asset = player?.currentItem?.asset
            let audioDuration = asset?.duration
            let audioDurationSeconds = CMTimeGetSeconds(audioDuration!)
            
            let maximumduration = audioDurationSeconds
            cell.seekbarSlider.maximumValue = Float(maximumduration)
            
            if player != nil
            {
                self.player?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main, using: { (time) in
                    if self.player!.currentItem?.status == .readyToPlay {
                        
                        
                        let currentTime = CMTimeGetSeconds(self.player!.currentTime())
                        
                        let secs = Int(currentTime)
                        //self.timeLabel.text = NSString(format: "%02d:%02d", secs/60, secs%60) as String//"\(secs/60):\(secs%60)"
                        cell.seekbarSlider.setValue(Float(secs), animated: true)
                        if (audioDurationSeconds == currentTime)
                        {
                            cell.seekbarSlider.setValue(0, animated: true)
                        }
                        
                        let currentDuration = CMTimeGetSeconds((self.player!.currentItem?.duration)!)
                        
                        let secondDuration = Int(currentDuration)
                        
                        print(secondDuration)
                        print(secs)
                        
                        if secondDuration == secs
                        {
                            
                            let indexPathRow:Int = sender.tag
                            let indexPosition = IndexPath(row: indexPathRow, section: 0)
                            self.tblChat.reloadRows(at: [indexPosition], with: .none)
                        }
                        print(NSString(format: "%02d:%02d", secs/60, secs%60) as String)
                        
                    }
                })
            }

            
        }
        else
        {
            var cell : AudioReciverCell = tblChat.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! AudioReciverCell
            
            cell.seekbarSlider.minimumValue = 0.0
            
            if self.player == nil
            {
                let url = URL(string: urlOfFile)
                let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
                player = AVPlayer(playerItem: playerItem)
                player!.play()
                cell.buttonPlay.setImage(UIImage.init(named: "pause"), for: .normal)
                
            }
            else if self.player!.timeControlStatus == .playing {
                
                cell.buttonPlay.setImage(UIImage.init(named: "Play"), for: .normal)
                self.player?.pause()//
            }
            else if self.player!.timeControlStatus == .paused {
                do {
                    cell.buttonPlay.setImage(UIImage.init(named: "pause"), for: .normal)
                    if player != nil
                    {
                        player!.seek(to: CMTime.init(seconds: Double(cell.seekbarSlider!.value)
                            , preferredTimescale: 1))
                    }
                    
                    player?.play()
                    
                }
            }
            let asset = player?.currentItem?.asset
            let audioDuration = asset?.duration
            let audioDurationSeconds = CMTimeGetSeconds(audioDuration!)
            
            let maximumduration = audioDurationSeconds
            cell.seekbarSlider.maximumValue = Float(maximumduration)
            self.player?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main, using: { (time) in
                if self.player!.currentItem?.status == .readyToPlay {
                    let currentTime = CMTimeGetSeconds(self.player!.currentTime())
                    
                    let secs = Int(currentTime)
                    //self.timeLabel.text = NSString(format: "%02d:%02d", secs/60, secs%60) as String//"\(secs/60):\(secs%60)"
                    cell.seekbarSlider.setValue(Float(secs), animated: true)
                    if (audioDurationSeconds == currentTime)
                    {
                        cell.seekbarSlider.setValue(0, animated: true)
                    }
                    
                    let currentDuration = CMTimeGetSeconds((self.player!.currentItem?.duration)!)
                    
                    let secondDuration = Int(currentDuration)
                    
                    // print(time)
                    
                    if secondDuration == secs
                    {
                        
                        let indexPathRow:Int = sender.tag
                        let indexPosition = IndexPath(row: indexPathRow, section: 0)
                        self.tblChat.reloadRows(at: [indexPosition], with: .none)
                    }
                    
                    print(NSString(format: "%02d:%02d", secs/60, secs%60) as String)
                    
                }
            })
        }
        
        previousCellIndex = sender.tag
        
        
    }
    
    
    
    //MARK: - Farword Action Methods
    @IBAction func copyButtonClicked(sender: UIButton)
    {
        removeLongPressDilog()
        
        var dictData = isSearchBarOpen ? chatSearchMessages[sender.tag] : chatMessages[sender.tag]
        UIPasteboard.general.string = (dictData["message"] as! String)
    }
    @IBAction func deleteButtonClicked(sender: UIButton)
    {
        removeLongPressDilog()
        var dictData = isSearchBarOpen ? chatSearchMessages[sender.tag] : chatMessages[sender.tag]
        
        let strMsgID = dictData["uid"] as! String
        
        
        let params1 = ["id":UserDefaults.userID,"uid":",'" + strMsgID + "'"] as! [String:String]
        print(params1)
        SocketIOManager.shared().deleteMessage(parameters: params1)
        
        chatMessages.remove(at: sender.tag)
        tblChat.beginUpdates()
        tblChat.deleteRows(at: [NSIndexPath.init(row: sender.tag, section: 0) as IndexPath], with: .automatic)
        tblChat.endUpdates()
    }
    @IBAction func hardDeleteButtonClicked(sender: UIButton)
    {
        removeLongPressDilog()
        let dictData = chatMessages[sender.tag]
        
        let strMsgID = dictData["uid"] as! String
        let reciverID = dictData["rcv_id"] as! String
        let params1 = ["id":UserDefaults.userID! + "," + reciverID,"uid":",'" + strMsgID + "'"]
        SocketIOManager.shared().deleteMessage(parameters: params1)
        
        chatMessages.remove(at: sender.tag)
        tblChat.beginUpdates()
        tblChat.deleteRows(at: [NSIndexPath.init(row: sender.tag, section: 0) as IndexPath], with: .automatic)
        tblChat.endUpdates()
        
    }
    @IBAction func farwordButtonClicked(sender: UIButton)
    {
        removeLongPressDilog()
        
        if arrFarwardSelectedItem.count > 0
        {
            let farwardView = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "UserFarwardListingVC") as! UserFarwardListingVC
            farwardView.arrSelectedMessage = arrFarwardSelectedItem
            
            self.navigationController?.pushViewController(farwardView, animated: true)
        }
        else
            
        {
            UIAlertController.showAlertWith(title: "UDTalks", message: "Please Select one message First", dismissBloack: {})
        }
        
    }
    
    func removeLongPressDilog()  {
        tblChat.allowsMultipleSelection = false
        
        viewFarword.removeFromSuperview()
        tblChat.reloadData()
    }
}


//MARK: -
//MARK: - Contact PIcker Delegate
extension ChatViewController : UserListingDelegete
{
    func userListing(_ getSelectedUserData: [String : Any]) {
        print(getSelectedUserData)
        let strPhnNumber = getSelectedUserData["name"] as! String
        let strImgPath = getSelectedUserData["path"] as! String
        let strDeviceID = getSelectedUserData["deviceid"] as! String
        
        //let msgString = "\(strPhnNumber),\(strImgPath),\(UserDefaults.userID!),\(strDeviceID)"
        //strPhnNumber+","+strImgPath+","+UserDefaults.userID+","+strDeviceID
        
        if self.isGroupChat
        {
            
            //            let param = ["reciverid":reciverID,"message":"\(strPhnNumber),\(strImgPath),\(UserDefaults.userID!),\(strDeviceID)","dtype":"share","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
            //            print(param)
            //            SocketIOManager.shared().sentGroupMessage(parameters: param)
        }
        else
        {
            //            let param = ["reciverid":reciverID,"message":"\(strPhnNumber),\(strImgPath),\(UserDefaults.userID!),\(strDeviceID)","dtype":"share","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
            //
            //            //object.addProperty("message", "9713172282,imageURL,userID,DeviceId");
            //            SocketIOManager.shared().sentMessage(parameters: param)
        }
        let strContact = "\(strPhnNumber),\(strImgPath),\(UserDefaults.userID!),\(strDeviceID)"
        self.sentMessageToSocket(type: .contactShare, message: strContact, uid: "")
        
        
    }
    
    
}

extension ChatViewController : UIDocumentPickerDelegate
{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        
        let newUrls = urls.flatMap { (url: URL) -> URL? in
            // Create file URL to temporary folder
            var tempURL = URL(fileURLWithPath: NSTemporaryDirectory())
            // Apend filename (name+extension) to URL
            tempURL.appendPathComponent(url.lastPathComponent)
            do {
                // If file with same name exists remove it (replace file with new one)
                if FileManager.default.fileExists(atPath: tempURL.path) {
                    try FileManager.default.removeItem(atPath: tempURL.path)
                }
                // Move file from app_id-Inbox to tmp/filename
                try FileManager.default.moveItem(atPath: url.path, toPath: tempURL.path)
                print(tempURL)
                let fileName = UtilityClass.getCurrentTimeStamp() + "." + tempURL.pathExtension
                
                uploadFiles(usingImage: tempURL as NSURL, fileName: fileName, cellIndex: 0)
                
                return tempURL
            } catch {
                print(error.localizedDescription)
                return nil
            }
            print(url)
        }
        //
        //
        //        guard let selectedFileURL = urls.first else{
        //            return
        //        }
        //
        //        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        //        let sandboxFileURL = dir.appendingPathComponent(selectedFileURL.path)
        //
        //
        //
        //            if FileManager.default.fileExists(atPath: sandboxFileURL.path)
        //        {
        //            print("already Added")
        //            print(sandboxFileURL)
        //
        //        }
        //        else
        //        {
        //            do {
        //                try FileManager.default.copyItem(at: selectedFileURL, to: sandboxFileURL)
        //                print("COpy Files")
        //                print(sandboxFileURL.path)
        //                uploadFiles(usingImage: NSURL.init(string: sandboxFileURL.path)!, fileName: "fileupload1", cellIndex: 0)
        //            }
        //            catch
        //            {
        //                print("error : \(error)")
        //            }
        //        }
        
    }
    
    
}

extension ChatViewController : MPMediaPickerControllerDelegate
{
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        self.dismiss(animated: true, completion: nil)
        print("you picked: \(mediaItemCollection)")//This is the picked media item.
        
        for items in mediaItemCollection.items
        {
            print(items.value(forProperty: MPMediaItemPropertyTitle))
            print(items.value(forProperty: MPMediaItemPropertyAssetURL))
        }
        
        //  If you allow picking multiple media, then mediaItemCollection.items will return array of picked media items(MPMediaItem)
    }
    
}

//MARK:- <--- Extension Emoji View Delegate
extension ChatViewController: EmojiViewDelegate
{
    func emojiViewDidPickEmoji(_ emojiName: [String])
    {
        for name in emojiName
        {
            self.sentMessageToSocket(type: .emoji, message: name, uid: "")
        }
    }
}

extension ChatViewController : CNContactViewControllerDelegate
{
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) { self.dismiss(animated: true, completion: nil) }
}

//MARK: - <---- Extension UISearchBar Delegate Methods
extension ChatViewController : UISearchBarDelegate
{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.removeFromSuperview()
        searchBar.text = ""
        isSearchBarOpen = false
        tableVwTopConstraint.constant = 0
        
        tblChat.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        print(searchText)
        
        if searchText.count == 0
        {
            return
        }
        //        let foundItems = chatMessages.filter { $0.itemName.contains(filterItemName) }
        
        let arrVal:[[String:AnyObject]] = chatMessages.filter { (dictVal) -> Bool in
            
            //chatMessages.filter{($0["message"] as! String) == searchText} as [[String : AnyObject]]
            let strMessage = dictVal["message"] as! String
            if strMessage.contains(searchText)
            {
                return true
            }
            else
            {
                return false
            }
            } as [[String : AnyObject]]
        
        chatSearchMessages = arrVal
        tblChat.reloadData()
        print(arrVal)
        
    }
}

extension ChatViewController : PhotoEditorDelegate
{
    func doneEditing(image: UIImage) {
        // the edited image
        print(image)
        
        
        let imageName = UtilityClass.getCurrentTimeStamp() + ".jpg"
        
        
        // let editedImage = imgActual as? UIImage
        
        _ = UtilityClass.addFileToFolder("saved", fileName: imageName, fileData: (image.jpegData(compressionQuality: 0.1))!)
        
        if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: imageName) {
            
            self.uploadImage(usingImage: fileURL as NSURL, fileName:imageName, cellIndex: 0)
        }
    }
    
    func canceledEditing() {
        print("Canceled")
        
    }
}
