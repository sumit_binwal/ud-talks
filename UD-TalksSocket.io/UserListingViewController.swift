//
//  UserListingViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 07/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

protocol UserListingDelegete {
    func userListing(_ getSelectedUserData : [String:Any])
}

class UserListingViewController: UIViewController {

    @IBOutlet var tablViewUsers: UITableView!
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    var delegate:UserListingDelegete?
    var userDataDict = [String:Any]()
    var selectedPathIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Add Contact"

        _ = backButton
        
        tablViewUsers.delegate = self
        tablViewUsers.dataSource = self
        
        let doneBtn =  UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonPressed))
        
        

        self.navigationItem.rightBarButtonItem = doneBtn

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        
        if appDelegate.isInternetAvailable()
        {
            let params = ["senderid":UserDefaults.userID]
            SocketIOManager.shared().userListing(parameters: params as [String : Any])
        }
        else
            
        {
            arrRes = DataBaseManager.sharedInstance().fetchUserListingFromDB() as [[String : AnyObject]]
            tablViewUsers.reloadData()
        }

        

    }
    
    
    @objc func doneButtonPressed()
    {
        if userDataDict.count > 0
        {
            delegate?.userListing(userDataDict)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            UIAlertController.showAlertWith(title: "", message: "Please Select Contact First") {
                
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UserListingViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * scaleFactorX
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRes.count
    }
    
  
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "Cell") as! Kcell
       
        var dict = arrRes[indexPath.row]
        cell.lblName?.text = dict["name"] as? String
        cell.selectionStyle = .none
        if indexPath.row % 2 == 0
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 246, GREEN: 134, BLUE: 2, ALPHA: 1)
        }
        else
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 61, GREEN: 61, BLUE: 61, ALPHA: 1)
            
        }
            if selectedPathIndex == indexPath.row
            {
                cell.viewBGColor.backgroundColor = UIColor.gray
                cell.viewBGColor.alpha = 0.4
            }
            else
                
            {
                cell.viewBGColor.backgroundColor = UIColor.clear
                cell.viewBGColor.alpha = 0
            }

        
        cell.lblDiscription?.text = "Mobile, Mobile Covers, Batteries, Chargers"
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
     
        userDataDict = arrRes[indexPath.row]
        selectedPathIndex = indexPath.row
        tableView.reloadData()
    }
    
    
    
}
