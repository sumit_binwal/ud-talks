//
//  DataBaseManager.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 17/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import FMDB
import UIKit

//MARK:- Table Names
enum dbTableName : String {
    case usersTable = "users"
    case chatListTable = "chatList"
    case recentChatsTable = "recentChats"
}
//MARK: - Table Colums
enum tblRecentChatColumn : String {
    case chattype = "chattype"
    case datetime = "datetime"
    case deviceid = "deviceid"
    case dtype = "dtype"
    case id = "id"
    case message = "message"
    case name = "name"
    case photo = "photo"
    case userstatus = "userstatus"
    
    
    static let allValues = [chattype, datetime, deviceid, dtype, id, message, name, photo, userstatus]

    
    
}


class DataBaseManager:NSObject  {
    
    
    var database = FMDatabase()

    private static var instance : DataBaseManager =
    {
        let newInstance = DataBaseManager.init()
        
        return newInstance
    }()

    class func sharedInstance() -> DataBaseManager {
        return instance
    }

    private override init() {
        super.init()
        let fileURL = try! FileManager.default
            .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("udTalksDB.db")
        
        let fileExists = (try? fileURL.checkResourceIsReachable()) ?? false
        if !fileExists {
            let bundleURL = Bundle.main.url(forResource: "udTalksDB", withExtension: "db")!
            try! FileManager.default.copyItem(at: bundleURL, to: fileURL)
        }
        
        database = FMDatabase(url: fileURL)

        print(fileURL)
    }
    
    //MARK: - RecentChat DB Methods
        func insertValuesToRecentChatDB(dataArry : [[String:Any]])
        {
            database.open()
            do {
                try database.executeStatements("DELETE FROM recentChats")
    
                for dictVal in dataArry
                {
                    //
    let strAdmin = dictVal["admin"] as? String ?? ""
                    let strDescription = dictVal["description"] as? String ?? ""
                    let strPath = dictVal["path"] as? String ?? ""
                    
                    let strChattype = dictVal["\(tblRecentChatColumn.chattype)"] as! String
                    let strDateTime = dictVal["\(tblRecentChatColumn.datetime)"] as! String
                    let strDeviceID = dictVal["\(tblRecentChatColumn.deviceid)"] as! String
                    let strDtype = dictVal["\(tblRecentChatColumn.dtype)"] as! String
                    let strID = dictVal["\(tblRecentChatColumn.id)"] as! String
                    let strMsg = dictVal["\(tblRecentChatColumn.message)"] as! String
                    let strName = dictVal["\(tblRecentChatColumn.name)"] as! String
                    let strPhoto = dictVal["\(tblRecentChatColumn.photo)"] as! String
                    let strUsrStatus = dictVal["\(tblRecentChatColumn.userstatus)"] as! String
                    
                    
                    let strQueary = "(" + tblRecentChatColumn.chattype.rawValue + "," + tblRecentChatColumn.datetime.rawValue + "," + tblRecentChatColumn.deviceid.rawValue + "," + tblRecentChatColumn.dtype.rawValue + "," + tblRecentChatColumn.id.rawValue + "," + tblRecentChatColumn.message.rawValue + "," + tblRecentChatColumn.name.rawValue + "," + tblRecentChatColumn.photo.rawValue + "," + tblRecentChatColumn.userstatus.rawValue + ",admin,description,path)"
                    
                try database.executeUpdate("insert into recentChats \(strQueary) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [strChattype, strDateTime, strDeviceID, strDtype, strID, strMsg, strName, strPhoto, strUsrStatus,strAdmin,strDescription,strPath])
    
    

                }
            } catch {
                print("failed: \(error.localizedDescription)")
            }
            database.close()
    
        }
    
    func getValuesToRecentChatDB() -> [[String:Any]]
    {
        var arrResultValue = [[String:Any]]()

        database.open()
        do {
  
                //
//                let strAdmin = dictVal["admin"] as? String ?? ""
//                let strDescription = dictVal["description"] as? String ?? ""
//                let strPath = dictVal["path"] as? String ?? ""
//
//                let strChattype = dictVal["\(tblRecentChatColumn.chattype)"] as! String
//                let strDateTime = dictVal["\(tblRecentChatColumn.datetime)"] as! String
//                let strDeviceID = dictVal["\(tblRecentChatColumn.deviceid)"] as! String
//                let strDtype = dictVal["\(tblRecentChatColumn.dtype)"] as! String
//                let strID = dictVal["\(tblRecentChatColumn.id)"] as! String
//                let strMsg = dictVal["\(tblRecentChatColumn.message)"] as! String
//                let strName = dictVal["\(tblRecentChatColumn.name)"] as! String
//                let strPhoto = dictVal["\(tblRecentChatColumn.photo)"] as! String
//                let strUsrStatus = dictVal["\(tblRecentChatColumn.userstatus)"] as! String
            
                
            
//                try database.executeUpdate("insert into recentChats \(strQueary) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [strChattype, strDateTime, strDeviceID, strDtype, strID, strMsg, strName, strPhoto, strUsrStatus,strAdmin,strDescription,strPath])
            
                let rs = try database.executeQuery("select * from recentChats", values: nil)
                                    while rs.next() {
                
                
                if let deviceId = rs.string(forColumn: "deviceid"),let id = rs.string(forColumn: "id"),let name = rs.string(forColumn: "name"),let path = rs.string(forColumn: "path"),let photo = rs.string(forColumn: "photo"),let chatType = rs.string(forColumn: "chattype"),let datetime = rs.string(forColumn: "datetime"),let dtype = rs.string(forColumn: "dtype"),let message = rs.string(forColumn: "message"),let userstatus = rs.string(forColumn: "userstatus"),let admin = rs.string(forColumn: "admin"),let description = rs.string(forColumn: "description")
                {
                
                    let dbDict = ["deviceid":deviceId, "id":id, "name":name, "path":path, "photo":photo,"chattype":chatType,"datetime":datetime,"dtype":dtype,"message":message, "userstatus":userstatus, "admin":admin, "description":description]
                
                                            print(dbDict)
        arrResultValue.append(dbDict as [String : AnyObject])
                                           
                   }
                  }
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        return arrResultValue

    }
    
    
    //MARK: - USer Listing DB Methods
    func insertValuesToUserListingTable(dataArry : [[String:Any]])
    {
                database.open()
                do {
                    try database.executeStatements("DELETE FROM users")
        
                    for dictVal in dataArry
                    {
                        //
        
                        let strDeviceID = dictVal["deviceid"] as! String
                        let strID = dictVal["id"] as! String
                        let strName = dictVal["name"] as! String
                        let strPath = dictVal["path"] as! String
                        let strPhoto = dictVal["photo"] as! String
                    try database.executeUpdate("insert into users (deviceid,id,name,path,photo) values (?, ?, ?, ?, ?)", values: [strDeviceID, strID, strName, strPath, strPhoto])
        
                    }
                } catch {
                    print("failed: \(error.localizedDescription)")
                }
                database.close()
        
            }
    
    func fetchUserListingFromDB() -> [[String:Any]]
    {
        database.open()
        var userListingData = [[String:Any]]()
        do {
        let rs = try database.executeQuery("select deviceid, id, name, path, photo from users", values: nil)
                        while rs.next() {
        
        
                            if let deviceId = rs.string(forColumn: "deviceid"), let id = rs.string(forColumn: "id"), let name = rs.string(forColumn: "name"), let path = rs.string(forColumn: "path"), let photo = rs.string(forColumn: "photo")  {
        
                                let dbDict = ["deviceid":deviceId, "id":id, "name":name, "path":path, "photo":photo]
        
                                print(dbDict)
                                userListingData.append(dbDict as [String : AnyObject])
                                //tblJSON.reloadData()
                                //print("x = \(x); y = \(y); z = \(z)")
                            }
                        }
    }
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()

        return userListingData

}
    
    //MARK: - Chat History Listing DB Methods
    func insertValuesToChatTable(dataArry : [[String:Any]], reciverid:String)
    {
        database.open()
        do {
           
            try database.executeStatements("DELETE FROM chatList where rcv_id=\(reciverid)")

            for dictVal in dataArry
            {
                //
                
                let strDateTime = dictVal["\(tblRecentChatColumn.datetime)"] as! String
                let strDeliver = dictVal["deliver"] as! String
                let strID = dictVal["\(tblRecentChatColumn.id)"] as? String ?? "00"
                let strIsRead = dictVal["isread"] as! String
                let strMsg = dictVal["\(tblRecentChatColumn.message)"] as! String
                let strRecvieID = dictVal["rcv_id"] as? String ?? dictVal["reciverid"]
                let strSendID = dictVal["send_id"] as? String ?? dictVal["senderid"]
                let strType = dictVal["type"] as! String
                let strUid = dictVal["uid"] as! String
                
                
                if !checkIfMessageExistOrNotinDB(messageID: strID)
                {
                    try database.executeUpdate("insert into chatList (datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [strDateTime, strDeliver, strID, strIsRead, strMsg, strRecvieID, strSendID, strType, strUid, "1"])

                }
                
            }
        }
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }
    
    func insertDictToChatTable(dictVal : [String:Any],isMsgSent : String)
    {
        database.open()
        do {
            
           // try database.executeStatements("DELETE FROM chatList")
            
           
                //
                
                let strDateTime = dictVal["\(tblRecentChatColumn.datetime)"] as! String
                let strDeliver = dictVal["deliver"] as! String
                let strID = dictVal["\(tblRecentChatColumn.id)"] as? String ?? "00"
                let strIsRead = dictVal["isread"] as! String
                let strMsg = dictVal["\(tblRecentChatColumn.message)"] as! String
                let strRecvieID = dictVal["rcv_id"] as? String ?? dictVal["reciverid"]
                let strSendID = dictVal["send_id"] as? String ?? dictVal["senderid"]
                let strType = dictVal["type"] as? String ?? dictVal["dtype"]
                let strUid = dictVal["uid"] as! String
                
                
                
                try database.executeUpdate("insert into chatList (datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values: [strDateTime, strDeliver, strID, strIsRead, strMsg, strRecvieID, strSendID, strType, strUid, isMsgSent])
                
            
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
    }


    func fetchChatHistoryFromDB(userID : String) -> [[String:Any]]
    {
        database.open()
        var userListingData = [[String:Any]]()
        do {
            //SELECT * FROM contents WHERE id = 1
            let rs = try database.executeQuery("select * from chatList WHERE rcv_id = \(userID)", values: nil)
            while rs.next() {
                
                //datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent
                if let datetime = rs.string(forColumn: "datetime"), let id = rs.string(forColumn: "id"), let deliver = rs.string(forColumn:"deliver"), let isread = rs.string(forColumn: "isread"), let message = rs.string(forColumn: "message"), let rcv_id = rs.string(forColumn: "rcv_id"), let send_id = rs.string(forColumn: "send_id"), let type = rs.string(forColumn: "type"), let uid = rs.string(forColumn: "uid"), let isMsgSent = rs.string(forColumn: "isMsgSent")  {
                    
                    let dbDict = ["datetime":datetime, "id":id, "deliver":deliver, "isread":isread, "message":message, "rcv_id":rcv_id, "send_id":send_id, "type":type, "uid":uid, "isMsgSent":isMsgSent]
                    
                    print(dbDict)
                    userListingData.append(dbDict as [String : AnyObject])
                    //tblJSON.reloadData()
                    //print("x = \(x); y = \(y); z = \(z)")
                }
            }
        }
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
        return userListingData
        
    }
    
    func checkIfMessageExistOrNotinDB(messageID : String) -> Bool
    {
        database.open()
        var userListingData = [[String:Any]]()
        do {
            //SELECT * FROM contents WHERE id = 1
            let rs = try database.executeQuery("select COUNT(id) as cnt from chatList WHERE id = \(messageID)", values: nil)
            while rs.next() {
                
                let countVal = rs.int(forColumn: "cnt")
                if countVal == 1
                {
                    return true
                }
                else
                {
                    return false
                }
                print("Exist hai : \(countVal)")

                }
            }
        
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
return false
    }

    
    func fetchUnsendMessageChatFromDB() -> [[String:Any]]
    {
        database.open()
        var userListingData = [[String:Any]]()
        do {
            let rs = try database.executeQuery("select * from chatList where isMsgSent = 0", values: nil)
            while rs.next() {
                
                //datetime,deliver,id,isread,message,rcv_id,send_id,type,uid,isMsgSent
                if let datetime = rs.string(forColumn: "datetime"), let id = rs.string(forColumn: "id"), let deliver = rs.string(forColumn:"deliver"), let isread = rs.string(forColumn: "isread"), let message = rs.string(forColumn: "message"), let rcv_id = rs.string(forColumn: "rcv_id"), let send_id = rs.string(forColumn: "send_id"), let type = rs.string(forColumn: "type"), let uid = rs.string(forColumn: "uid"), let isMsgSent = rs.string(forColumn: "isMsgSent")  {
                    
                    let dbDict = ["datetime":datetime, "id":id, "deliver":deliver, "isread":isread, "message":message, "rcv_id":rcv_id, "send_id":send_id, "type":type, "uid":uid, "isMsgSent":isMsgSent]
                    
                    print(dbDict)
                    userListingData.append(dbDict as [String : AnyObject])
                    //tblJSON.reloadData()
                    //print("x = \(x); y = \(y); z = \(z)")
                }
            }
        }
        catch {
            print("failed: \(error.localizedDescription)")
        }
        database.close()
        
        return userListingData
        
    }


    
}
