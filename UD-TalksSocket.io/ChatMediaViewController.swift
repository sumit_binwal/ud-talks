//
//  ChatMediaViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 15/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ChatMediaViewController: UIViewController {

    var arrMedia = [[String:Any]]()
    var playerItem:AVPlayerItem?
    var audioPlayerUD : AVAudioPlayer?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func play(for resource: String, type: String) {
        // Prevent a crash in the event that the resource or type is invalid
        guard let path = Bundle.main.path(forResource: resource, ofType: type) else { return }
        // Convert path to URL for audio player
        let sound = URL(fileURLWithPath: path)
        do {
            audioPlayerUD = try AVAudioPlayer(contentsOf: sound)
            audioPlayerUD?.prepareToPlay()
            audioPlayerUD?.play()
        } catch {
            // Create an assertion crash in the event that the app fails to play the sound
            assert(false, error.localizedDescription)
        }
    }

    
    @IBAction func mediaItemsButtonClicked(sender:UIButton)
    {
        let dictData = arrMedia[sender.tag]
        
        var isType = dictData["type"] as? String


        if isType == "video"
        {
            
            let urlString = dictData["message"] as! String
            let videoURL = NSURL(string: urlString)
            let player = AVPlayer(url: videoURL! as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
        }
        }
        else if isType == "location"
        {
            let strLatLongData = dictData["message"] as! String
            let arrValues = strLatLongData.components(separatedBy: ",")
            
            print(arrValues)
            let locationVwCntroller = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: "LocationFullViewController") as! LocationFullViewController
            locationVwCntroller.latitude = arrValues[0]
            locationVwCntroller.longitude = arrValues[1]
            locationVwCntroller.strPlaceName = arrValues[2]
            self.navigationController?.pushViewController(locationVwCntroller, animated: true)
            
        }
        else if isType == "img"
        {
            let fullImgVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "FullImageVIewController") as! FullImageVIewController
            
            fullImgVC.arrImageData = arrMedia.filter({ (dictVal) -> Bool in
                
                var isType = dictVal["type"] as? String
                
                if isType?.isEmpty ?? true
                {
                    isType = dictVal["dtype"] as? String
                }
                if isType  == "img"
                {
                    return true
                }
                return false
            })
            
            let imgURL = dictData["message"] as! String
            for (indexVal, imgName) in fullImgVC.arrImageData.enumerated()
            {
                if imgName["message"] as! String == imgURL
                {
                    fullImgVC.imageIndex = indexVal
                }
            }
            
            
            self.navigationController?.pushViewController(fullImgVC, animated: true)
        }
        else if isType == "audio"
        {
            let strURLLInk = dictData["message"] as! String
            
            let wbViewVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            wbViewVC.pageURL = strURLLInk
            self.navigationController?.pushViewController(wbViewVC, animated: true)        }
        else if isType == "pdf"
        {
            
            let strURLLInk = dictData["message"] as! String
            
            let wbViewVC = UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            wbViewVC.pageURL = strURLLInk
            self.navigationController?.pushViewController(wbViewVC, animated: true)
        }
    }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


extension ChatMediaViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3 * scaleFactorX
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 117*scaleFactorX, height: 117*scaleFactorX)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChatMediaCollectionCell.cellIdentifier, for: indexPath) as! ChatMediaCollectionCell
        
        let dictMedia = arrMedia[indexPath.row]
        
        var isType = dictMedia["type"] as? String
        
        if isType?.isEmpty ?? true
        {
            isType = dictMedia["dtype"] as? String
        }
        cell.buttonMedia.setImage(nil, for: .normal)

        if isType == "img" || isType == "video" || isType == "audio" || isType == "location"
        {
            
            if isType == "video"
            {
                cell.labelMediaType.text = "Video"

                cell.imageMediaCover.image = dictMedia["videoThumb"] as! UIImage
                cell.buttonMedia.setImage(#imageLiteral(resourceName: "videoImg"), for: .normal)
                cell.buttonMedia.tintColor = UIColor.white
            }
            else if isType == "location"
            {
                cell.labelMediaType.text = "Location"
                cell.imageMediaCover.image = #imageLiteral(resourceName: "locationLogo")

            }
            else if isType == "img"
            {
                cell.labelMediaType.text = "Image"
                cell.imageMediaCover.sd_setImage(with: URL.init(string: dictMedia["message"] as! String), completed: nil)
            }
            else if isType == "audio"
            {
                cell.labelMediaType.text = "Audio"
            cell.imageMediaCover.image = #imageLiteral(resourceName: "menu12")
            }
            
            cell.buttonMedia.tag = indexPath.row
            cell.buttonMedia.addTarget(self, action: #selector(mediaItemsButtonClicked(sender:)), for: .touchUpInside)

            
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
    }
    
}
