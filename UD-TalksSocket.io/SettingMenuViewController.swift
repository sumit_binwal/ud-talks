//
//  SettingMenuViewController.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 02/12/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class SettingMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var ArrOfSettingLabel = ["Add to phone contacts", "Search", "Media", "Mute Notification", "Add wallpaper", "Clear chat"]
    var ArrOfSettingImage = ["Add Contacts", "Search", "Media", "Notification", "WallPapers", "Clean history"]
    
    @IBOutlet weak var tblSettingMenu: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrOfSettingLabel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblSettingMenu.dequeueReusableCell(withIdentifier: "SettingMenuCell") as! SettingMenuTVCell
        cell.imgSettingMenu.image = UIImage(named: ArrOfSettingImage[indexPath.row])
        cell.lblSettingMenu.text = ArrOfSettingLabel[indexPath.row]
        
        if indexPath.row == 3
        {
            
        //here is programatically switch make to the table view
        let switchView = UISwitch(frame: .zero)
        switchView.setOn(false, animated: true)
        switchView.tag = indexPath.row // for detect which row switch Changed
        switchView.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        cell.accessoryView = switchView
        
        }
        
        return cell
    }
    
    @objc func switchChanged(_ sender : UISwitch!){
        
        print("table row switch Changed \(sender.tag)")
        print("The switch is \(sender.isOn ? "ON" : "OFF")")
    }
}
