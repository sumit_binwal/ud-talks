//
//  ChatListViewController.swift
//  UD-Talks Demo
//
//  Created by mac on 25/07/1940 Saka.
//  Copyright © 1940 ZiasyTechnology. All rights reserved.
//

import UIKit
import SwipeCellKit

class ChatListViewController: UIViewController,SwipeTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate

{
    
    @IBOutlet var tblJSON: UITableView!
    
    @IBOutlet weak var textFieldSearch: UITextField!
    var fromId :String = ""
    
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    
    var isSearchBarOpen = false
    var chatSearchMessages = [[String: Any]]()

    
    override func viewDidLoad() {
        super.viewDidLoad()


        showNavigationBar()
        self.title = "Add Contact"
        _ = backBarButton
        

      if appDelegate.isInternetAvailable()
      {
        let params = ["senderid":UserDefaults.userID]
        SocketIOManager.shared().userListing(parameters: params as [String : Any])
        }
        else
        
      {
        arrRes = DataBaseManager.sharedInstance().fetchUserListingFromDB() as [[String : AnyObject]]
        tblJSON.reloadData()
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        tblJSON.reloadData()
    }
    

    
    @IBAction func crossButtonClicked(_ sender: Any)
    {
        textFieldSearch.text = ""
        isSearchBarOpen = false
        tblJSON.reloadData()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK:- Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let numberRow = isSearchBarOpen ? chatSearchMessages.count : arrRes.count
        return numberRow
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .left else { return nil }
        
        let dictValue = self.isSearchBarOpen ? self.chatSearchMessages[indexPath.row] : self.arrRes[indexPath.row]

        let viewButton = SwipeAction(style: .default, title: "") { action, indexPath in
            // handle action by updating model with deletion
            let profileVC =  UIStoryboard.getNewScreenStoryBoard().instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            profileVC.profileName = dictValue["name"] as! String
            self.navigationController?.pushViewController(profileVC, animated: true)

            
        }
        
        // customize the action appearance
        viewButton.image = UIImage(named: "swipeVIewHome")
        
        
        
        let callButton = SwipeAction(style: .default, title: "") { action, indexPath in
            // handle action by updating model with deletion
            
            var dict = [String:Any]()
            
            
            dict = dictValue
            
            let chatVC = UIStoryboard.getMainStoryBoard().instantiateViewController(withIdentifier: String.ControllerName.chatVC) as! ChatViewController
            
            let a = dict["id"] as! String
            let b = dict["name"] as! String
            
            print(a)
            print(b)
            chatVC.reciverID = a
            chatVC.reciverName = b
            
            self.isSearchBarOpen = false
            self.view.endEditing(true)
            self.textFieldSearch.text = ""
            
            self.navigationController?.pushViewController(chatVC, animated: true)
            

        }
        
        // customize the action appearance
        callButton.image = UIImage(named: "swipeAddHome")
        
        
        
        return [viewButton,callButton]
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tblJSON.dequeueReusableCell(withIdentifier: "Cell") as! Kcell
        
        cell.delegate = self
        
        let dictValue = isSearchBarOpen ? chatSearchMessages[indexPath.row] : arrRes[indexPath.row]

        
        var dict = dictValue
        cell.lblName?.text = dict["name"] as? String
        cell.selectionStyle = .none
        if indexPath.row % 2 == 0
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 246, GREEN: 134, BLUE: 2, ALPHA: 1)
        }
        else
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 61, GREEN: 61, BLUE: 61, ALPHA: 1)        }
        
       // cell.imgUser.circleView(_color:.gray, borderWidth: 0.5)
        //cell.imgUser?.clipsToBounds = true
        cell.lblDiscription?.text = "Mobile, Mobile Covers, Batteries, Chargers"
        
        //configure left buttons
//        cell.rightButtons = [MGSwipeButton(title: "View", icon: UIImage(named:"Agent"), backgroundColor: .green),
//                            MGSwipeButton(title: "Delete", icon: UIImage(named:"Agent"), backgroundColor: .blue)]
//
//        cell.rightSwipeSettings.transition = .clipCenter
        
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        
        
//        let imagUser = ""
//       print("dict")
//        let rootViewController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let selectedViewController = mainStoryboard.instantiateViewController(withIdentifier: "navigationChat") as! ChatViewController
//        selectedViewController.fromUserId = fromId
//        selectedViewController.senderId = ""
//        PredefinedConstants.appDelegate.moveToChatScreen(otherUserId: Config.fromId, otherUserName: "", otherUserImage: imagUser, inViewController: self, isCustomer: false)
//        rootViewController.pushViewController(selectedViewController, animated: true)
     
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * scaleFactorX
    }
}
//extension ChatListViewController : MGSwipeTableCellDelegate
//{
//    func swipeTableCellWillEndSwiping(_ cell: MGSwipeTableCell) {
//        
//    }
//    
//    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection, from point: CGPoint) -> Bool {
//        return true
//    }
//    
//    func swipeTableCell(_ cell: MGSwipeTableCell, didChange state: MGSwipeState, gestureIsActive: Bool) {
//        
//    }
//    
//    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
//        
//        print(direction)
//        print(index)
//        print(fromExpansion)
//        return true
//    }
//    
//    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
//        
//        let view : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 40 ))
//        view.backgroundColor = UIColor.black
//        return [view]
//    }
////    -(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
////    /**
////     * Delegate method invoked when the current swipe state changes
////     @param state the current Swipe State
////     @param gestureIsActive YES if the user swipe gesture is active. No if the uses has already ended the gesture
////     **/
//   // -(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState) state gestureIsActive:(BOOL) gestureIsActive;
//    /**
//     * Called when the user clicks a swipe button or when a expandable button is automatically triggered
//     * @return YES to autohide the current swipe buttons
//     **/
//   // -(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion;
//    /**
//     * Delegate method to setup the swipe buttons and swipe/expansion settings
//     * Buttons can be any kind of UIView but it's recommended to use the convenience MGSwipeButton class
//     * Setting up buttons with this delegate instead of using cell properties improves memory usage because buttons are only created in demand
//     * @param swipeTableCell the UITableViewCell to configure. You can get the indexPath using [tableView indexPathForCell:cell]
//     * @param direction The swipe direction (left to right or right to left)
//     * @param swipeSettings instance to configure the swipe transition and setting (optional)
//     * @param expansionSettings instance to configure button expansions (optional)
//     * @return Buttons array
//     **/
//  //  -(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
//  //  swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
//
//}

extension ChatListViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var newString : String?
        
        if string.count == 0 {
            if textField.text?.count == 0 {
                newString = textField.text
            }
            else {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else {
            var newStr = textField.text! as NSString
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            newString = newStr as String
        }
        
        let newString1 = newString as! String
        
        if newString1.count > 0
        {
            let arrVal:[[String:AnyObject]] = arrRes.filter { (dictVal) -> Bool in
                
                //chatMessages.filter{($0["message"] as! String) == searchText} as [[String : AnyObject]]
                let strMessage = dictVal["name"] as! String
                if strMessage.contains(newString1)
                {
                    return true
                }
                else
                {
                    return false
                }
                } as [[String : AnyObject]]
            
            isSearchBarOpen = true
            chatSearchMessages = arrVal
            tblJSON.reloadData()
        }
        
        
        
        return true
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        isSearchBarOpen = false
//        tblJSON.reloadData()
//    }
}
