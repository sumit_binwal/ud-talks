//
//  EmojiViewController.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 30/11/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

protocol EmojiViewDelegate {
    func emojiViewDidPickEmoji(_ emojiName:[String])

}

class EmojiViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    var Arrimg = ["emoji", "emoji1", "emoji2", "emoji3", "emoji4", "emoji5", "emoji6", "emoji7", "emoji8", "emoji9", "emoji10", "emoji11", "emoji12", "emoji13", "emoji14", "emoji15", "emoji16", "emoji17", "emoji18", "emoji19", "emoji20", "emoji21", "emoji22", "emoji23", "emoji24", "emoji25", "emoji26", "emoji27", "emoji28", "emoji29", "emoji30", "emoji31", "emoji32", "emoji33", "emoji34", "emoji35", "emoji36", "emoji37", "emoji38", "emoji39",  "emoji40", "emoji41", "emoji42", "emoji43", "emoji44", "emoji45", "emoji46", "emoji47", "emoji48", "emoji49",  "emoji50", "emoji51", "emoji52", "emoji53", "emoji54", "emoji55", "emoji56", "emoji57", "emoji58", "emoji59",  "emoji60", "emoji61", "emoji62", "emoji63", "emoji64", "emoji65", "emoji66", "emoji67", "emoji68", "emoji69", "emoji70", "emoji71", "emoji72", "emoji73", "emoji74", "emoji75", "emoji76", "emoji77", "emoji78", "emoji79"]

     @IBOutlet var collectionView: UICollectionView!
    var arrSelectedEmoji = [String] ()
    var delegate:EmojiViewDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
       // _ = backButton
        
        let doneBtn =  UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(btnSendEmoji(_:)))
        
        
        
        self.navigationItem.rightBarButtonItem = doneBtn

        

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return Arrimg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        let size = CGRect(x: 0, y: 0, width: 100, height: 100)
        
//        let imageview: UIImageView = UIImageView(frame: size)
  
        cell.img.image = UIImage.init(named: Arrimg[indexPath.row])!
        
//        let image: UIImage = UIImage(named: Arrimg[indexPath.row])!
//        imageview.image = image
//        imageview.tag = indexPath.row
//        cell.contentView.addSubview(imageview)
        
        
        for emojiName in arrSelectedEmoji
        {
            if emojiName == Arrimg[indexPath.row]
            {
                cell.backgroundColor = UIColor.green
            }
            else
            
            {
                cell.backgroundColor = UIColor.white
            }
        }
        //cell.img.image? = Arrimg[indexPath.row]
        //set images
        //cell.img.image = UIImage(named: Arrimg[indexPath.row])
        
         return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        
        var cell = collectionView.cellForItem(at: indexPath)
        
        let strEmojiName = Arrimg[indexPath.row] as! String
        
        if arrSelectedEmoji.contains(strEmojiName)
        {
            
            if let index = arrSelectedEmoji.index(of: strEmojiName) {
                arrSelectedEmoji.remove(at: index)
                
            }
            cell?.contentView.backgroundColor = UIColor.white
        
        }
        else
        {
            arrSelectedEmoji.append(strEmojiName)
            cell?.contentView.backgroundColor = UIColor.lightGray
        }
        print(arrSelectedEmoji)
    }
    
    @IBAction func btnSendEmoji(_ sender: Any)
    {
        if arrSelectedEmoji.count > 0
        {
        delegate?.emojiViewDidPickEmoji(arrSelectedEmoji)
       self.navigationController?.popViewController(animated: true)

        }
        else
        {
            UIAlertController.showAlertWith(title: "", message: "Please choose at least one emoji", dismissBloack: {})
        }
        
    }
    
}
