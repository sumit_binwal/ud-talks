//
//  Config.swift
//  AskMeWorld
//
//  Created by Jyoti on 02/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import SocketIO
import UIKit
//import SQLite

//MARK :- Constant
let App_Name = "KChat"
let const_appDelegate = UIApplication.shared.delegate as! AppDelegate
let App_Global_Msg = "This is an alert"
let userDefault = UserDefaults.standard
let ALERT_TITLE = "KChat"
//let scaleFactorX = UIScreen.main.bounds.size.width/375
//let scaleFactorY = UIScreen.main.bounds.size.height/667
let ApplicationName = "KChat"
let App_Global_Error_Msg = "We encountered some error, please try again later"

//let socketUrl = "http://192.168.0.68"
//let socketPort = "3331"

let socketUrl = "http://202.157.76.22"
let socketPort = "3331"

let socket_manager = SocketManager(socketURL: URL(string: socketUrl+":"+socketPort)!)
let socket = socket_manager.defaultSocket
var isSocketConnected = false
let logout_message = "Are you sure you want to logout from KChat?"
let AWS_BUCKET = "https://s3-ap-south-1.amazonaws.com/kchat-dev/"
//// Common fields --------------------------------------------------------------
//let const_id = Expression<Int64>("id")
//// ----------------------------------------------------------------------------
//
//// Contacts table fields --------------------------------------------------------------
//let const_name = Expression<String?>("name")
//let const_phone = Expression<String?>("phone")
//let const_app_user = Expression<Bool?>("app_user")
//let const_image = Expression<String?>("image")
//let const_contact_user_id = Expression<String?>("contact_user_id")
//let const_formatted_number = Expression<String?>("formatted_number")
//let const_country_code = Expression<String?>("country_code")
//let const_on_device_contact = Expression<Bool?>("on_device_contact")
//let const_is_in_chat = Expression<Bool?>("is_in_chat")
//let const_profile_pic_privacy = Expression<String?>("profile_pic_privacy")

// ------------------------------------------------------------------------------------

// Chats table fields --------------------------------------------------------------
//let const_message_type = Expression<String?>("message_type")
//let const_location = Expression<String?>("location")
//let const_message = Expression<String?>("message")
//let const_status = Expression<String?>("status")
//let const_other_user_id = Expression<String?>("other_user_id")
//let const_map_image = Expression<String?>("map_image")
//let const_msg_read_status = Expression<Int?>("read_status")
//let const_files = Expression<String?>("files")
//let const_msg_time = Expression<Double?>("const_msg_time")
//let const_msg_id = Expression<String?>("const_msg_id")
//let const_msg_delivery_status = Expression<String?>("delivery_status")
//let const_msg_is_starred = Expression<Bool?>("is_starred")
//let const_msg_is_deleted = Expression<Bool?>("is_deleted")
//let const_reply_message_id = Expression<String?>("reply_message_id")

// ---------------------------------------------------------------------------------


//let const_sender_id = Expression<String?>("sender_id")
//let const_sender_name = Expression<String?>("sender_name")
//let const_sender_phone = Expression<String?>("sender_phone")
//let const_receiver_id = Expression<String?>("receiver_id")
//let const_receiver_name = Expression<String?>("receiver_name")
//
//let const_message_id = Expression<String?>("message_id")
//
//let const_server_thread_id = Expression<String?>("server_thread_id")


//For Groups
//let const_group_id = Expression<String?>("group_id")
//let const_group_members_id = Expression<String?>("members_id")
//let const_group_members_name = Expression<String?>("members_name")
//let const_group_members_phone = Expression<String?>("members_phone")
//
//let const_group_name = Expression<String?>("group_name")
//let const_group_owner_id = Expression<String?>("group_owner_id")
//let const_group_image = Expression<String?>("group_image")
//let const_group_created_time = Expression<String?>("const_group_created_time")

let countriesDict = ["AF":"93",
                     "AL":"355",
                     "DZ":"213",
                     "AS":"1",
                     "AD":"376",
                     "AO":"244",
                     "AI":"1",
                     "AG":"1",
                     "AR":"54",
                     "AM":"374",
                     "AW":"297",
                     "AU":"61",
                     "AT":"43",
                     "AZ":"994",
                     "BS":"1",
                     "BH":"973",
                     "BD":"880",
                     "BB":"1",
                     "BY":"375",
                     "BE":"32",
                     "BZ":"501",
                     "BJ":"229",
                     "BM":"1",
                     "BT":"975",
                     "BA":"387",
                     "BW":"267",
                     "BR":"55",
                     "IO":"246",
                     "BG":"359",
                     "BF":"226",
                     "BI":"257",
                     "KH":"855",
                     "CM":"237",
                     "CA":"1",
                     "CV":"238",
                     "KY":"345",
                     "CF":"236",
                     "TD":"235",
                     "CL":"56",
                     "CN":"86",
                     "CX":"61",
                     "CO":"57",
                     "KM":"269",
                     "CG":"242",
                     "CK":"682",
                     "CR":"506",
                     "HR":"385",
                     "CU":"53",
                     "CY":"537",
                     "CZ":"420",
                     "DK":"45",
                     "DJ":"253",
                     "DM":"1",
                     "DO":"1",
                     "EC":"593",
                     "EG":"20",
                     "SV":"503",
                     "GQ":"240",
                     "ER":"291",
                     "EE":"372",
                     "ET":"251",
                     "FO":"298",
                     "FJ":"679",
                     "FI":"358",
                     "FR":"33",
                     "GF":"594",
                     "PF":"689",
                     "GA":"241",
                     "GM":"220",
                     "GE":"995",
                     "DE":"49",
                     "GH":"233",
                     "GI":"350",
                     "GR":"30",
                     "GL":"299",
                     "GD":"1",
                     "GP":"590",
                     "GU":"1",
                     "GT":"502",
                     "GN":"224",
                     "GW":"245",
                     "GY":"595",
                     "HT":"509",
                     "HN":"504",
                     "HU":"36",
                     "IS":"354",
                     "in":"91",
                     "ID":"62",
                     "IQ":"964",
                     "IE":"353",
                     "IL":"972",
                     "IT":"39",
                     "JM":"1",
                     "JP":"81",
                     "JO":"962",
                     "KZ":"77",
                     "KE":"254",
                     "KI":"686",
                     "KW":"965",
                     "KG":"996",
                     "LV":"371",
                     "LB":"961",
                     "LS":"266",
                     "LR":"231",
                     "LI":"423",
                     "LT":"370",
                     "LU":"352",
                     "MG":"261",
                     "MW":"265",
                     "MY":"60",
                     "MV":"960",
                     "ML":"223",
                     "MT":"356",
                     "MH":"692",
                     "MQ":"596",
                     "MR":"222",
                     "MU":"230",
                     "YT":"262",
                     "MX":"52",
                     "MC":"377",
                     "MN":"976",
                     "ME":"382",
                     "MS":"1",
                     "MA":"212",
                     "MM":"95",
                     "NA":"264",
                     "NR":"674",
                     "NP":"977",
                     "NL":"31",
                     "AN":"599",
                     "NC":"687",
                     "NZ":"64",
                     "NI":"505",
                     "NE":"227",
                     "NG":"234",
                     "NU":"683",
                     "NF":"672",
                     "MP":"1",
                     "NO":"47",
                     "OM":"968",
                     "PK":"92",
                     "PW":"680",
                     "PA":"507",
                     "PG":"675",
                     "PY":"595",
                     "PE":"51",
                     "PH":"63",
                     "PL":"48",
                     "PT":"351",
                     "PR":"1",
                     "QA":"974",
                     "RO":"40",
                     "RW":"250",
                     "WS":"685",
                     "SM":"378",
                     "SA":"966",
                     "SN":"221",
                     "RS":"381",
                     "SC":"248",
                     "SL":"232",
                     "SG":"65",
                     "SK":"421",
                     "SI":"386",
                     "SB":"677",
                     "ZA":"27",
                     "GS":"500",
                     "ES":"34",
                     "LK":"94",
                     "SD":"249",
                     "SR":"597",
                     "SZ":"268",
                     "SE":"46",
                     "CH":"41",
                     "TJ":"992",
                     "TH":"66",
                     "TG":"228",
                     "TK":"690",
                     "TO":"676",
                     "TT":"1",
                     "TN":"216",
                     "TR":"90",
                     "TM":"993",
                     "TC":"1",
                     "TV":"688",
                     "UG":"256",
                     "UA":"380",
                     "AE":"971",
                     "GB":"44",
                     "US":"1",
                     "UY":"598",
                     "UZ":"998",
                     "VU":"678",
                     "WF":"681",
                     "YE":"967",
                     "ZM":"260",
                     "ZW":"263",
                     "BO":"591",
                     "BN":"673",
                     "CC":"61",
                     "CD":"243",
                     "CI":"225",
                     "FK":"500",
                     "GG":"44",
                     "VA":"379",
                     "HK":"852",
                     "IR":"98",
                     "IM":"44",
                     "JE":"44",
                     "KP":"850",
                     "KR":"82",
                     "LA":"856",
                     "LY":"218",
                     "MO":"853",
                     "MK":"389",
                     "FM":"691",
                     "MD":"373",
                     "MZ":"258",
                     "PS":"970",
                     "PN":"872",
                     "RE":"262",
                     "RU":"7",
                     "BL":"590",
                     "SH":"290",
                     "KN":"1",
                     "LC":"1",
                     "MF":"590",
                     "PM":"508",
                     "VC":"1",
                     "ST":"239",
                     "SO":"252",
                     "SJ":"47",
                     "SY":"963",
                     "TW":"886",
                     "TZ":"255",
                     "TL":"670",
                     "VE":"58",
                     "VN":"84",
                     "VG":"284",
                     "VI":"340",
                     "IN" : "91"]
//ASS#


let DOCUMENTS_DIRECTORY_URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

public func print(items: Any..., separator: String = " ", terminator: String = "\n") {
    
    #if DEBUG
    
    var idx = items.startIndex
    let endIdx = items.endIndex
    
    repeat {
        Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
        idx += 1
    }
        while idx < endIdx
    
    #endif
}
