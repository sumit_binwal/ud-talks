//
//  SocketManager.swift
//  Knackel
//
//  Created by Santosh on 18/05/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation
import SocketIO

class SocketIOManager {
    
    enum SocketEmitEvents: String {
        case newMessage
        case chatHistory
        case chatList
        case joinTravelChat
        case leaveChatWindow
        case presenceStatus
        case userid
        case employeeList
        case sendMessage
        case deleteMessage
        case loadGroupChat
        case loadRecieverChat
        case togglewindowstatus
        case ChatemployeeListDetail
        case typing
        case stopTyping
        case sendMessageGroup
        

    }
    
    enum SocketListenerEvents: String {
        case newMessage
        case roomMessage
        case presenceStatus
        case employeeListDetail
        case mySendMessage
        case recieveMessage
        case userMessage
        case chatEmployeeList
        case onTyping
        case onStopTyping
        case userOnline
        case userOffline
        case userMessageGroup
    }
    
    private var socketClientManager: SocketManager!
    private var socketClient: SocketIOClient!
    
    private var _token = ""
    
    typealias HandlerSocketConnection = () -> ()
    
    var onSocketConnection: HandlerSocketConnection?
    var onSocketDisConnection: HandlerSocketConnection?
    
    //On New Message Recive handler
    var newMessageHandler: Optional<([String:Any]?) -> ()> = nil {
        didSet {
            if newMessageHandler == nil {
                removeNewMessageListener ()
            }
            else {
                addNewMessageListener()
            }
        }
    }
    
    
    //On Recive New Message Recive handler
    var reciveSenderMessageHandler: Optional<([String:Any]?) -> ()> = nil {
        didSet {
            if reciveSenderMessageHandler == nil {
                removeNewMessageListener ()
            }
            else {
                addNewMessageListener()
            }
        }
    }
    //Get Chat History Handler
    var getHistoryHandler: Optional<([String:Any]?) -> ()> = nil {
        didSet {
            if getHistoryHandler == nil {
                removeNewMessageListener ()
            }
            else {
                addNewMessageListener()
            }
        }
    }
    
   
    //Get Sent Message  Handler
    var sendChatMessageHandler: Optional<([String:Any]?) -> ()> = nil {
        didSet {
            if sendChatMessageHandler == nil {
                removeNewMessageListener ()
            }
            else {
                addNewMessageListener()
            }
        }
    }
    
    //online Offline Status  Handler
    var chatOnlineOfflineStatusHandler: Optional<([String:Any]?, Bool) -> ()> = nil {
        didSet {
            if chatOnlineOfflineStatusHandler == nil {
                removeNewMessageListener ()
            }
            else {
                addNewMessageListener()
            }
        }
    }
    
    //Typing Status  Handler
    var chatTypingStatusHandler: Optional<([String:Any]?, Bool) -> ()> = nil {
        didSet {
            if chatTypingStatusHandler == nil {
                removeNewMessageListener ()
            }
            else {
                addNewMessageListener()
            }
        }
    }
    
    //Recent Chat List Handler
    var recentChatMessageHandler: Optional<([String:Any]?) -> ()> = nil {
        didSet {
            if recentChatMessageHandler == nil {
                removeNewMessageListener ()
            }
            else {
                addNewMessageListener()
            }
        }
    }
    
    
    
    
    
    // Shared variable to return single instance of this class SocketManager
    private static var sharedManager: SocketIOManager = {
        let socketInstance = SocketIOManager ()
        return socketInstance
    }()
    
    // init method, initializes the SocketManager instance
    private init() {
        print("init called")
        
        
        //let paramDict = ["ssid":UserDefaults.userSSID!,"access_token":"12345678"] as [String:Any]

        
        socketClientManager = SocketManager (socketURL: URL (string: socketBaseURL)!, config: SocketIOClientConfiguration (arrayLiteral: .compress, .log(true), .reconnectWait(4), .reconnects(true)))
        
        socketClient = socketClientManager.defaultSocket
        
        handleListeners()
    }
    
    // Shared function accesible to users for accessing the methods and variables
    class func shared() -> SocketIOManager {
        return sharedManager
    }
    
//    class func removeReference() {
//        sharedManager = nil
//    }
    
    //MARK:* User Token
    /// User token GET-SET for authentication. Should only be called before connectSocket()
    var token: String {
        get {
            return _token
        }
        set {
            switch socketClientManager.status {
            case .disconnected, .notConnected:
                _token = newValue
                var config = socketClientManager.config
                config.insert(.connectParams(["ssid":newValue]))
                socketClientManager.config = config
            default:
                break
            }
        }
    }
    
    //MARK:: Connect Socket
    /// Connect Socket
    func connectSocket() {
        socketClientManager.connect()
    }
    
    //MARK:: Disconnect Socket
    /// Disconnect Socket
    func disconnectSocket() {
        if isSocketConnected() {
            socketClientManager.disconnect()
        }
    }
    
    //MARK:: Socket Connect Status
    /// isSocketConnected
    func isSocketConnected() -> Bool {
        return socketClientManager.status == .connected
    }
    
    //MARK:: Listeners
    /// Handling the Listners
    private func handleListeners() {
        socketClient.on(clientEvent: .connect) { (dataArray, ack) in
            print(dataArray)
            guard let closure = self.onSocketConnection else {return}
            closure ()
        }
        
        socketClient.on(clientEvent: .disconnect) { (dataArray, ack) in
            print(dataArray)
            guard let closure = self.onSocketDisConnection else {return}
            closure ()
        }
    }
    
}

//MARK:-
//MARK:- Socket Emits
extension SocketIOManager {
    
    typealias SocketCompletionHandler = ([String:Any]?) -> ()
    typealias SocketHistoryCompletionHandler = ([[String:Any]]?) -> ()
    typealias SocketChatHistoryCompletionHandler = ([[String:Any]]?,_ chatDetail:ModelChatInfo) -> ()
    
    //MARK:: Send Message
    func sendMessage(_ model: ModelSocketSendMessage, onCompletion: @escaping SocketCompletionHandler) {
        let params = model.getDictionary()
        print(params)
        

        socketClient.emitWithAck(SocketEmitEvents.newMessage.rawValue, with: [params]).timingOut(after: 0) { (result) in
            print(result)
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                onCompletion (nil)
                return
            }
            if infoDict.isEmpty {
                onCompletion (nil)
                return
            }
            onCompletion (infoDict)
        }
    }
    
    //MARK:: Get Chat History With User
    func getChatHistory(_ model: ModelSocketGetChatHistory, onCompletion: @escaping SocketChatHistoryCompletionHandler) {
        
        let params = model.getDictionary()
        print(params)
        
        socketClient.emitWithAck(SocketEmitEvents.chatHistory.rawValue, with: [params]).timingOut(after: 0) { (result) in
            print(result)
            
            var modelChatDetail = ModelChatInfo()

            guard result.count > 0, let infoArray = result[0] as? [String:Any] else {
                onCompletion (nil, modelChatDetail)
                return
            }
            modelChatDetail.updateModel(usingDictionary: infoArray)

            let statusCode = infoArray["status"] as! Int
            let messageDict = infoArray["data"] as? [String:Any]
            
            
            
            guard messageDict != nil else
            {
                return
            }
            
            if statusCode == 200
            {
                let arryMessagesDict = messageDict!["messages"] as? [[String:Any]]
                onCompletion(arryMessagesDict, modelChatDetail)
            }
            else
            {
                onCompletion ([infoArray], modelChatDetail)
            }
        }
    }
    
    //MARK:: Get Chat History With User
    func getChatListing(_ onCompletion: @escaping SocketHistoryCompletionHandler) {
        
        socketClient.emitWithAck(SocketEmitEvents.chatList.rawValue, with: []).timingOut(after: 0) { (result) in
            print(result)
            
            guard result.count > 0, let infoArray = result[0] as? [[String:Any]] else {
                onCompletion (nil)
                return
            }
            print(infoArray)
            onCompletion (infoArray)
        }
    }
    
    //MARK:: Get Chat History With User
    func joinTravelChat(parameters: [String:Any], onCompletion: @escaping SocketCompletionHandler) {
        
        socketClient.emitWithAck(SocketEmitEvents.joinTravelChat.rawValue, with: [parameters]).timingOut(after: 0) { (result) in
            print(result)
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                onCompletion (nil)
                return
            }
            print(infoDict)
            onCompletion(infoDict)
        }
    }
    
    //MARK:: Leaving chat window
    func leaveChatWindow(parameters: [String:Any]) {
        
        socketClient.emitWithAck(SocketEmitEvents.leaveChatWindow.rawValue, with: [parameters]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: Authenticate User ID
    func authUserIDtoSocket(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)

        socketClient.emitWithAck(SocketEmitEvents.userid.rawValue, with: [jsonString]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: Chat EmployeDetail
    func chatEmployeDetailSocket(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.ChatemployeeListDetail.rawValue, with: [jsonString]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    
    //MARK:: Authenticate User ID
    func toggleWindowtoSocket(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.togglewindowstatus.rawValue, with: [jsonString]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: User Listing Detail
    func userListing(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.employeeList.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
   
    //MARK:: Chat Typing
    func chatTypingStatus(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.typing.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: Chat Typing
    func stopChatTypingStatus(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.stopTyping.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    
    //MARK:: Get History Detail
    func loadRecieverChat(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.loadRecieverChat.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: Get History Detail
    func loadGroupRecieverChat(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.loadGroupChat.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: Sent Message Detail
    func sentMessage(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.sendMessage.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: Sent Message Detail
    func sentGroupMessage(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.sendMessageGroup.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    
    //MARK:: Delete Message Detail
    func deleteMessage(parameters: [String:Any]) {
        
        
        let jsonString = JSONStringEncoder().encode(parameters)
        
        socketClient.emitWithAck(SocketEmitEvents.deleteMessage.rawValue, with: [jsonString!]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    
    //MARK:: Presence status
    func setPresenceStatus(parameters: [String:Any]) {
        socketClient.emitWithAck(SocketEmitEvents.presenceStatus.rawValue, with: [parameters]).timingOut(after: 0) { (result) in
            print(result)
        }
    }
    
    //MARK:: Exit Chat Room
//    func exitChatRoom(parameters: [String:Any], onCompletion: @escaping SocketCompletionHandler) {
//
//        socketClient.emitWithAck(SocketEmitEvents.exitRoom.rawValue, with: [parameters]).timingOut(after: 0) { (result) in
//            print(result)
//
//            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
//                onCompletion (nil)
//                return
//            }
//            print(infoDict)
//            onCompletion(infoDict)
//        }
//    }
}

//MARK:-
//MARK:- Socket Listeners
private extension SocketIOManager {
    
    //MARK:: Add message listener
    func addNewMessageListener() {
        removeNewMessageListener()
        socketClient.on(SocketListenerEvents.newMessage.rawValue) {[weak self] (result, ack) in
            print(result)
            
            guard let `self` = self else {return}
            guard let handler = self.newMessageHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        
        
        socketClient.on(SocketListenerEvents.roomMessage.rawValue) {[weak self] (result, ack) in
            print(result)
            
            guard let `self` = self else {return}
            guard let handler = self.newMessageHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        socketClient.on(SocketListenerEvents.presenceStatus.rawValue) { (result, ack) in
            print(result)
        }
        
        socketClient.on(SocketListenerEvents.recieveMessage.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.reciveSenderMessageHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        socketClient.on(SocketListenerEvents.chatEmployeeList.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.recentChatMessageHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        
        socketClient.on(SocketListenerEvents.userMessage.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.getHistoryHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        
        socketClient.on(SocketListenerEvents.userMessageGroup.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.getHistoryHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        //mySendMessage
        
        socketClient.on(SocketListenerEvents.mySendMessage.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.sendChatMessageHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        //myTypingMessage
        
        socketClient.on(SocketListenerEvents.onTyping.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.chatTypingStatusHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil, true)
                return
            }
            if infoDict.isEmpty {
                handler (nil, true)
                return
            }
            
            handler (infoDict, true)
        }
        
        //onStopMyTypingMessage
        socketClient.on(SocketListenerEvents.onStopTyping.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.chatTypingStatusHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil, false)
                return
            }
            if infoDict.isEmpty {
                handler (nil, false)
                return
            }
            
            handler (infoDict, false)
        }
        
        //onOnlineMessage
        socketClient.on(SocketListenerEvents.userOnline.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.chatOnlineOfflineStatusHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil, true)
                return
            }
            if infoDict.isEmpty {
                handler (nil, true)
                return
            }
            
            handler (infoDict, true)
        }
        
        //onOfflineMessage
        socketClient.on(SocketListenerEvents.userOffline.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.chatOnlineOfflineStatusHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil, false)
                return
            }
            if infoDict.isEmpty {
                handler (nil, false)
                return
            }
            
            handler (infoDict, false)
        }
        
        
        socketClient.on(SocketListenerEvents.employeeListDetail.rawValue) {[weak self] (result, ack) in
            print(result)
            guard let `self` = self else {return}
            guard let handler = self.newMessageHandler else {return}
            
            guard result.count > 0, let infoDict = result[0] as? [String:Any] else {
                handler (nil)
                return
            }
            if infoDict.isEmpty {
                handler (nil)
                return
            }
            
            handler (infoDict)
        }
        
        
    }
    
    //MARK:: Remove message listener
    func removeNewMessageListener() {
        socketClient.off(SocketListenerEvents.newMessage.rawValue)
        socketClient.off(SocketListenerEvents.roomMessage.rawValue)
        socketClient.off(SocketListenerEvents.presenceStatus.rawValue)
        socketClient.off(SocketListenerEvents.employeeListDetail.rawValue)
        socketClient.off(SocketListenerEvents.mySendMessage.rawValue)
        socketClient.off(SocketListenerEvents.recieveMessage.rawValue)
        socketClient.off(SocketListenerEvents.userMessage.rawValue)
        socketClient.off(SocketListenerEvents.chatEmployeeList.rawValue)
        socketClient.off(SocketListenerEvents.onTyping.rawValue)
        socketClient.off(SocketListenerEvents.onStopTyping.rawValue)
        socketClient.off(SocketListenerEvents.userOnline.rawValue)
        socketClient.off(SocketListenerEvents.userOffline.rawValue)
        socketClient.off(SocketListenerEvents.userMessageGroup.rawValue)
        socketClient.off(SocketListenerEvents.mySendMessage.rawValue)
        socketClient.off(SocketListenerEvents.newMessage.rawValue)
        

    }
}

struct JSONStringEncoder {
    /**
     Encodes a dictionary into a JSON string.
     - parameter dictionary: Dictionary to use to encode JSON string.
     - returns: A JSON string. `nil`, when encoding failed.
     */
    func encode(_ dictionary: [String: Any]) -> String? {
        guard JSONSerialization.isValidJSONObject(dictionary) else {
            assertionFailure("Invalid json object received.")
            return nil
        }
        
        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        let jsonData: Data
        
        dictionary.forEach { (arg) in
            jsonObject.setValue(arg.value, forKey: arg.key)
        }
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        } catch {
            assertionFailure("JSON data creation failed with error: \(error).")
            return nil
        }
        
        guard let jsonString = String.init(data: jsonData, encoding: String.Encoding.utf8) else {
            assertionFailure("JSON string creation failed.")
            return nil
        }
        
        print("JSON string: \(jsonString)")
        return jsonString
    }
}

