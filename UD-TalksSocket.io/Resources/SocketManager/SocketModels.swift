//
//  SocketModels.swift
//  Knackel
//
//  Created by Santosh on 24/05/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation

struct ModelSocketSendMessage {
    
    enum MessageType: String {
        case text = "0"
        case image = "1"
    }
    //jobId, receiver, message . isMedia
    var to: String
    var jobId: String?
    var message: String
    var messageType: MessageType
    
    func getDictionary() -> [String:Any] {
        return [
            "receiver": to,
            "message": message,
            "isMedia": messageType.rawValue,
            "jobId": jobId ?? ""
        ]
    }
}

struct ModelSocketGetChatHistory {
    
    var reciverID: String
    var jobID: String = ""
    
    func getDictionary() -> [String:Any] {
        return [
            "receiver": reciverID,
            "jobId": jobID
        ]
    }
}
