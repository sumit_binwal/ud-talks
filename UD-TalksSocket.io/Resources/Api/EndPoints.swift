//
//  EndPoints.swift
//  Knackel
//
//  Created by Vaibhav Khatri on 18/04/18.
//  Copyright © 2018 Konstant InfoSolutions Pvt. Ltd. All rights reserved.
//

import Foundation

enum StaticEndpoints {
    
    case developmentURL
    case localURL
    case stagingURL
    case socketBaseURL
    case termsAndCondition
    case privacyAndPolicy
    case other(String)
    
    var url: URL {
        
        switch self {
            
        case .termsAndCondition:
            return URL (string: String.MyApp.TermsAndConditionURL)!
            
        case .privacyAndPolicy:
            return URL (string: String.MyApp.PrivacyAndPolicy)!
            
        case .localURL:
            return URL (string: "http://")!
            
        case .stagingURL:
            return URL (string: "http://128.199.222.145/")!
            
        case .developmentURL:
            // http://192.168.0.131:9235/app/
            return URL (string: "http://")!
            
        case .socketBaseURL:
            return URL (string: "http://128.199.222.145:8000/")!
            
        case .other(let otherURLString):
            return URL (string: otherURLString)!
        }
    }
}

let baseURL: String = StaticEndpoints.stagingURL.url.absoluteString

let socketBaseURL : String = StaticEndpoints.socketBaseURL.url.absoluteString

enum EndPoints {
    
    case register
    case createGroup
    
    var path: URL {
        
        switch self {
            // Sign up endpoint
        case .register:
            return URL (string: baseURL + "reg/reg.php")!
        case .createGroup:
            return URL (string: baseURL + "reg/greg.php")!

       
        }
    }
}

//MARK:- Enum Endpoint Final Path
enum LinksEnum : String {
    case terms = "http://202.157.76.19:9230/app/staticPage/terms"
}
