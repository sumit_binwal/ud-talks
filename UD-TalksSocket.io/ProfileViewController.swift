//
//  ProfileViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 07/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet var tableViewProfile: UITableView!
    var profileName = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewProfile.delegate = self
        tableViewProfile.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProfileViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 360
        }
        else if indexPath.row == 1
        {
            return 101
        }
        else if indexPath.row == 2
        {
            return 101
        }
        else //indexPath.row == 3
        {
            return 137
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell  = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderCell.cellIdentifier) as! ProfileHeaderCell
            
            cell.profileHeadername?.text = profileName
            return cell

        }
        else if indexPath.row == 1
        {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "profileCellHeader1") as! UITableViewCell
            
            return cell
        }
        else if indexPath.row == 2
        {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "profileCellHeader2") as! UITableViewCell
            
            return cell
        }
         else if indexPath.row == 3 //indexPath.row == 3
        {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "profileCellHeader3") as! UITableViewCell
            
            return cell
        }
        else //indexPath.row == 4
        {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "profileCellHeader4") as! UITableViewCell
            
            return cell
        }
    }

    
    
    
}
