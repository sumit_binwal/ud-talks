//
//  CollectionViewCell.swift
//  UD-TalksSocket.io
//
//  Created by Mac on 30/11/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var img: UIImageView!
}
