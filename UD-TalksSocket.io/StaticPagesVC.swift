//
//  StaticPagesVC.swift
//  WaltzinUser
//
//  Created by Ratina on 10/2/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit

/// UIViewController class to show static pages content
class StaticPagesVC: UIViewController {

    /// variable to indicate which static page is -> 1 - About Us, 2 - FAQ, 3 - Terms & Conditions, 4 - Privacy Policy and 5 - Help
    var pageType = ""
    
    var pageURL = String()
    /// variable for static content data
    @IBOutlet weak var staticWebView: UIWebView!
    
    /// UIViewController class life cycles method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    /// UIViewController class life cycles method
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        showNavigationBar()
        
        
        
        _ = backBarButton
        
        loadWebView()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    /// function to load static content view
    func loadWebView() {
        
        UtilityClass.startAnimating()
        self.staticWebView.loadRequest(URLRequest.init(url: URL.init(string: pageURL)!))
    }
}

/// MARK: - extension for UIWebViewDelegate methods
extension StaticPagesVC : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UtilityClass.stopAnimating()
    }
}
