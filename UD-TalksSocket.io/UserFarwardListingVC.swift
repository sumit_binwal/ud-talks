//
//  UserFarwardListingVC.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 26/02/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class UserFarwardListingVC: UIViewController {

    
        @IBOutlet var tablViewUsers: UITableView!
        var arrRes = [[String:AnyObject]]() //Array of dictionary
        var userDataDict = [String:Any]()
    var arrSelectedMessage = [[String:Any]]()
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.title = "Farword Message"
            
            //_ = backButton
            
            tablViewUsers.delegate = self
            tablViewUsers.dataSource = self
            
           // let doneBtn =  UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneButtonPressed))
            
            
            
            //self.navigationItem.rightBarButtonItem = doneBtn
            
            
            // Do any additional setup after loading the view.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            
            
            let params = ["senderid":UserDefaults.userID]
            SocketIOManager.shared().userListing(parameters: params as [String : Any])
            
        }
        
        
        @objc func doneButtonPressed()
        {
            if userDataDict.count > 0
            {
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                UIAlertController.showAlertWith(title: "", message: "Please Select Contact First") {
                    
                }
            }
            
        }
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
         }
         */
        
    }
    extension UserFarwardListingVC : UITableViewDelegate, UITableViewDataSource
    {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 80 * scaleFactorX
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrRes.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "Cell") as! Kcell
            var dict = arrRes[indexPath.row]
            cell.lblName?.text = dict["name"] as? String
            
            if indexPath.row % 2 == 0
            {
                cell.vwStatus.backgroundColor = UIColor.init(RED: 246, GREEN: 134, BLUE: 2, ALPHA: 1)
            }
            else
            {
                cell.vwStatus.backgroundColor = UIColor.init(RED: 61, GREEN: 61, BLUE: 61, ALPHA: 1)        }
            
            cell.lblDiscription?.text = "Mobile, Mobile Covers, Batteries, Chargers"
            
            
            return cell
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            
            userDataDict = arrRes[indexPath.row]
            
            for dictVal in arrSelectedMessage
            {
            
                var isType = dictVal["type"] as? String
                
                if isType?.isEmpty ?? true
                {
                    isType = dictVal["dtype"] as? String
                }
                
                let reciverID = userDataDict["id"] as! String
                
                let strMsg = dictVal["message"] as! String
                if isType == "msg"
                {
                    let param = ["reciverid":reciverID,"message":strMsg,"dtype":"msg","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
                else  if isType == "share"
                {
                    let param = ["reciverid":reciverID,"message":strMsg,"dtype":"share","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    
                    //object.addProperty("message", "9713172282,imageURL,userID,DeviceId");
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
                else if isType == "emoji"

                {
                    let param = ["reciverid":reciverID,"message":strMsg,"dtype":"emoji","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
                else if isType == "pdf"
                {
                    let param = ["reciverid":reciverID,"message":strMsg,"dtype":"pdf","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
                else if isType == "location"
                {
                   let param = ["reciverid":reciverID,"message":strMsg ,"dtype":"location","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
else if isType == "img"
                {
                    let param = ["reciverid":reciverID,"message":strMsg,"dtype":"img","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
                else if isType == "video"
                {
                    let param = ["reciverid":reciverID,"message":strMsg,"dtype":"video","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
                else if isType == "audio"
                {
                    let param = ["reciverid":reciverID,"message":strMsg,"dtype":"audio","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userID  ] as [String : Any]
                    SocketIOManager.shared().sentMessage(parameters: param)

                }
                
            }
            self.navigationController?.popViewController(animated: true)
            
        }
        
        
        
    }

    
