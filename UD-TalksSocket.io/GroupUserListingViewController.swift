//
//  GroupUserListingViewController.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 17/02/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class GroupUserListingViewController: UIViewController {

    @IBOutlet weak var textFldSearch: UITextField!
    @IBOutlet weak var labelSelectedCount: UILabel!
    @IBOutlet weak var tblVwUserListing: UITableView!
    var strGroupName = String()
    var strGroupDiscription = String()
    
    var arrSelectedUser = [String]() //Array of dictionary
    var arrRes = [[String:Any]]() //Array of dictionary
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblVwUserListing.delegate = self
        tblVwUserListing.dataSource = self
        
        labelSelectedCount.text = "\(arrSelectedUser.count) Members Selected"

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
    hideNavigationBar()
        
        let params = ["senderid":UserDefaults.userID]
        SocketIOManager.shared().userListing(parameters: params as [String : Any])

    }
    override func viewWillDisappear(_ animated: Bool) {
        showNavigationBar()
    }
    
    @IBAction func createButtonClicked(_ sender: Any)
    {
        if arrSelectedUser.count > 1
        {
            arrSelectedUser.append(UserDefaults.mobileNumber!)
            performCreateGroup()
        }
        else
        {
            UIAlertController.showAlertWith(title: "", message: "Please select atleast 2 member in group", dismissBloack: {})
        }
        
    }
    @IBAction func backButtonClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func crossBtnClicked(_ sender: Any)
    {
        textFldSearch.text = ""
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func performCreateGroup() {
        //NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
//        params.put("num", "9797979797,9879879879,9999999,6969696"); // Number of user
//        params.put("photo", GroupIcon); // put a image url

        let params =  ["name": strGroupName,
                      "uid": strGroupDiscription,
                    "admin": UserDefaults.userName!,
                        "dt": Date.getString(),
                      "did": UserDefaults.getDeviceToken(),
                      "num"    : arrSelectedUser.joined(separator: ","),
                      "photo":"No Image Found"]
        print(params)
        
        FireApi.shared().performMultiPartRequest(for: EndPoints.createGroup.path, imageArray: [], headers: nil, parameters: params) { [weak self] result in
            
            UtilityClass.stopAnimating()
            
            guard let `self` = self else {return}
            
            switch result {
                
            case .error(let error):
                UIAlertController .showAlertWith(title: String.MyApp.AppName, message: error.localizedDescription, dismissBloack: {})
                
            case .success(let responseDict, let statusCode):
                
                print(responseDict)
                print(statusCode)
                
                // Error
                if statusCode == FireApi.ErrorCodes.code203.rawValue {
                    
                    if let msg = responseDict["message"] as? String {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: msg, dismissBloack: {})
                    }
                    else {
                        UIAlertController .showAlertWith(title: String.MyApp.AppName, message: String.MyApp.defaultErrorMessage, dismissBloack: {})
                    }
                    
                    return
                }
                
                // Success
                if statusCode == FireApi.ErrorCodes.code200.rawValue {
                    
                    print(responseDict)
                    
                    if let resultArray = responseDict["result"] as? [[String:Any]], !resultArray.isEmpty {
                        let info = resultArray.first!
                        print(info)
                        let strNumber = info["number"] as! String
                        let strGroupID = "\(info["g_id"]!)" 
                        
                        var arrNumbers = strNumber.components(separatedBy: ",")
                        print(arrNumbers)
                        arrNumbers.reverse()
                        for strValue in arrNumbers
                        {
                            let param = ["reciverid":strGroupID,"message":strValue,"dtype":"added","sname":UserDefaults.userName,"did":UserDefaults.getDeviceToken(),"uid":String.randomString(),"datetime": Date.getString(),"isread":"false","deliver":"false","senderid":UserDefaults.userName  ] as [String : Any]
                            print(param)
                            SocketIOManager.shared().sentGroupMessage(parameters: param)

                        }
                        
                        UIAlertController.showAlertWithTitle(title: "UDTalks", message: "Group Sucessfully Create", onViewController: self, dismissHandler: { (indexVal) in
                            
                            
                            self.showNavigationBar()
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        })

//                        UserDefaults.saveUserInformation(userInfo: info)
//                        APPDELEGATE.screenRedirection()
                    }
                    
                    
                    
                    return
                }
            }
        }
        
    }

}
extension GroupUserListingViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "Cell") as! Kcell
        var dict = arrRes[indexPath.row]
        cell.lblName?.text = dict["name"] as? String
        let phoneNmbr = dict["name"] as! String

        if arrSelectedUser.contains(phoneNmbr)
        {
            cell.viewBGColor.backgroundColor = UIColor.gray
            cell.viewBGColor.alpha = 0.3
        }
        else
        {
            cell.viewBGColor.backgroundColor = UIColor.clear
            cell.viewBGColor.alpha = 0
        }
        
        if indexPath.row % 2 == 0
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 246, GREEN: 134, BLUE: 2, ALPHA: 1)
        }
        else
        {
            cell.vwStatus.backgroundColor = UIColor.init(RED: 61, GREEN: 61, BLUE: 61, ALPHA: 1)        }
        
        cell.lblDiscription?.text = "Mobile, Mobile Covers, Batteries, Chargers"
        

        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        var cell = tableView.cellForRow(at: indexPath)
  
        
        let dictData = arrRes[indexPath.row] as! [String:Any]
        
        let phoneNmbr = dictData["name"] as! String
        
        
        if arrSelectedUser.contains(phoneNmbr)
        {
            
            if let index = arrSelectedUser.index(of: phoneNmbr) {
                arrSelectedUser.remove(at: index)
                
            }
           // cell?.contentView.backgroundColor = UIColor.white
            
        }
        else
        {
            arrSelectedUser.append(phoneNmbr)
           // cell?.contentView.backgroundColor = UIColor.lightGray
        }
        print(arrSelectedUser)
        
        
        labelSelectedCount.text = "\(arrSelectedUser.count) Members Selected"
        tblVwUserListing.reloadData()
        print(arrSelectedUser)
    }
}
