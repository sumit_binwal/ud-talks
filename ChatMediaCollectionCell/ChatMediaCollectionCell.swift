//
//  ChatMediaCollectionCell.swift
//  UD-TalksSocket.io
//
//  Created by Sumit Sharma on 15/02/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ChatMediaCollectionCell: UICollectionViewCell {
    @IBOutlet var labelMediaType: UILabel!
    static let cellIdentifier = "ChatMediaCollectionCell"
    
    @IBOutlet var buttonMedia: UIButton!
    @IBOutlet var imageMediaCover: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
